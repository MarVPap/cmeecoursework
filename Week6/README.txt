
Welcome to the 6th week of the CMEE master students! 

 __________________________________
   Genomics and Bioinformatics   
 __________________________________
                  

---- Code: 1 file

1. pop_gen_prac.R :

An R script to introduce ourselves to some basic population genomics 
to analyse dense genomic SNP data. This script calculates
genotype frequencies, allele frequencies, expected genotype
frequencies, tests for Hardy Weinberg equilibrium, and identifies
SNPs with large departures from Hardy Weinberg equilibrium.


---- Results: 1 file 

1. gen_plots.pdf:

A .pdf file consisting the 8 graphs produced form the population genomics 
analysis of the pop_gen_prac.R script.


---- Data: 1 file

1. H938_chr15.geno :
 
Dense genomic SNP data used from the pop_gen_prac.R script. Real data 
consisting of SNPs from chromosome 15 sampled from 52 global human populations. 
The data are derived from the Human Genome Diversity Project and were genotyped
using the Illumina 650k SNP genotyping chip array.


----Sandbox: empty folder



____________________________________________________________________
Created by Marina Papadopoulou marina.papadopoulou16@imperial.ac.uk




