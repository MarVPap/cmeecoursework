
----------------------------------------------------------------------------------------
____________________ The Cultural Evolution of Evolution _______________________________
----------------------------------------------------------------------------------------
This is the directory of my main project, supervised by Dr. James Rosindell, Prof. Armand Marie Leroi and Prof. Timothy Barraclough.

----Code:
..........................................................................................
The main files that correspond to the majority of the results of my report are:
1) ImportTextData.py: first step to import raw data. Reads through folders of xml papers summary files and
extracts usefull information. Then transforms the abstracts to the form needed for 
the topic analysis. Saves the corpus and dictionary on disk. Also exports as csv
all information from the xml files (authors, journal, abstracts etc).
2) ReportAnalysis.py : the full pipeline from importing the text data till extraction of topic analysis results and more
3) ReportPlots.R: analysis and plotting the topic analysis results.

The code for the rates of evolution and the scientific revolutions was provided by my supervisor
Armand Marie Leroi, and can not be shared publicly yet, given that is in the proccess of becoming a package.
..........................................................................................
The rest of the fules created during the project are:

1. compile_latex.sh : compiles tex files and remove junk files
2.jstor_data_cleaning.sh: unix to take xml and 1 gram files out of nested downloaded format
and add to separate folders to go through in analysis
3. run_dtm.sh: script to run in terminal the C implementation for the dynamic topic model
4. main: the compiled C implementation for the dynamic topic model (also dtm-master folder)
5. clean_dtm.py: prepares data and runs dynamic topic model **(not included in final report)
6. Diversity.py: imports resulting distributions from dtm modelling. 
Turns distributions into counts (community matrices) and calculates
diversity metrics. Exports all output in csv files. **(not included in final report)
7. DynTopM.py: Full dynamic topic modelling file. Runs dtm (like clean_dtm.py)
and then combine results with journals and paper info. Produces csv of distributions,
create wordclouds and plots(pca?,tsne, topic evolution). **(not included in final report)
8. load_files.py: Imports basic packages and files needed for all analysis.
Run at the beggining of each session.
9. ImportJSTORData.py: equivalent with ImportTextData but for ngrams data from jstor.
Goes through xml files, collects info, runs through its equivalents 1grams files,
prepares data for topic analysis. Outputs dictionary and corpus.
10. kmeans_phylo.py: plots kmeans/silhouette/trees
11. basic_functions.py: some functions commonly used to load at the beginning of the session
12. general_datawrang.py: script from first two months, data manipulation after lda and visualization
13. pairwise.py: combines lda results with journals info, create list JTDistribution, 
averages probability for each journal, create matrixjt, calculate different distances and plot trees. Outputs tree files, distance matrices, journal topic distribution file
14. TopicAnal.py: imports dictionary/corpus, runs a lot of lda models, calculates coherence measures. Outputs models and some words from topics csvs.
15. Visualize: ldavis and other visualization code

16. Data_manip.R: for abstract data (scopus) wrangling before keeping them for analysis 
17. DTMChangeModel.R: to build model of change through time
18. DTMPlotting.R: plot topic evolution through time from dtm
19. heatmap_phylo.R: plot heatmaps and dendograms
20. jstordata.R: see jstor data summary
21. PlotR_Old.R: to merge all the above and plot everything


----Data: An example of an .xml file with summary data from a paper that was used as my raw data.

----Results: The main file with the topic distributions over the documents of the dataset.

