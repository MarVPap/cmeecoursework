#!/usr/bin/env python

__author__  = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


# Import packages
print '\n'
print 'Importing packages..'

from pprint import pprint  # pretty-printer
from collections import defaultdict  # remove words that appear only once
from gensim import corpora, models, similarities
from nltk import word_tokenize
from nltk.corpus import stopwords
from sklearn import feature_extraction
from nltk.stem.snowball import SnowballStemmer # for stemming

#from lxml import etree # to deal with xml file format

import re 			# for use of regular expressions
import os 
import sys 
import csv 			# to import and export csvs
import nltk 		# natural language toolkit
import numpy as np  # for gensim package
import codecs
import string
import pandas as pd # for dataset manipulation
import unicodedata  # to deal with xml file format
#import urllib2
 


# INSERT ABSTRACTS

def import_data(my_xml_directory, my_ngrams_directory):

	def read_xmls(xml_directory): # '../Data/Downloads'
		''' A function that uses a directory and goes through every
		xml file in this directory. It uses regular expressions to keep
		the following information: name and surname of first author, name and surname
		of all authors, papers ID, number of times the paper has been cited till
		the day of download, the date it was publiced, the first author's ID, and 
		the number of references. It is then uses the gender_guesser package
		to guess the gender of all authors of each paper. Finally it exports
		all these information in a list of list (each sublist contains every papers
		information).'''
		
		# create objects
		MyCSV = [] 		# general list to store all the data
		x = 0 			# to count the papers info added
		y = 0 			# to count the errors occured
		
		for xmlFile in os.listdir(xml_directory): 	# for every xml name in the input directory
			xmlFile2  = xml_directory +'/'+ xmlFile 	# create path to the file
			f        = open(xmlFile2, 'rt') 					# open the file to read
			textFile = f.read()
			f.close()
			
			## Change files format to make it ready for regex	
			textFile = textFile.replace('\n', ' ').replace('\t', ' ') 	# replace all newlines and tabs with spaces
			textFile = textFile.decode('ascii', 'ignore')				# change format
			textFile = str((textFile)) 									# convert everything to string
			textFile = re.sub(' +',' ', textFile) 						# replace all + with spaces
			
			#######################################################
			## Use pf regular expressions (specifically look
			## behind and look infront commands) to extract elements
			
			#ScopusID = re.search(r'(?<=SCOPUS_ID:)\d+', textFile)				 # for the scopus ID
			#Abstract = re.search(r'(?<=<ce:para> ).*?(?=</ce:para> )', textFile) # Journal name
			Journal  = re.search(r'(?<=<journal-title>).*?(?=</journal-title>)', textFile)
			if Journal == None:
				Journal  = re.search(r'(?<=<journal-title content-type="full">).*?(?=</journal-title>)', textFile)

			Date     = re.search(r'(?<=<year>)\d{4}', textFile) # the date published (format: year-month-day)

		
			########################################
			# EXTRACT INFORMATION
			#print Journal.group()
			#print Date.group()
			try:			
				## Combine everything found into a tuple:
				Info = (xmlFile.replace('.xml', ''), Journal.group(), Date.group())
				# My_abstracts = Abstract.group()
				MyCSV.append(Info) 	# Add each papers tuple with information to the general results list (of tuples)
				# MyCSV.update({Info:My_abstracts})
				x = x+1 					# count paper added
				if x%10000 == 0:
					print '%i files imported!'%x
				
			except AttributeError:  		# In case one of the variables is missing in a specific xml file (e.g. some papers without references or number of them)
				y=y+1 						# count error
				print "Something missing- Attribute Error skipped for the file:"
				print xmlFile 				# name of the file for check 
				
				continue
			
		print "There were added %s elements in my results list for this part." %x
		print "There were %s errors or files with missing information." %y
		
		return MyCSV


	## Call the function for all the folders with our files (all parts)
	# compile documents into a list

	print 'Reading XML files....'

	xmlInfo= read_xmls(my_xml_directory) # Take info from all xml files in each folder

	export_csv(xmlInfo, '../Results/JSTOR/xmlInfo.csv', 'nlist')

	german_journals = ['Annalen des Naturhistorischen Museums in Wien', 'Zeitschrift fr Morphologie und kologie der Tiere']
	file_names = [x[0]+'-NGRAMS1.txt' for x in xmlInfo if x[1] not in german_journals] # create ngrams1 names to search in folder


	def read_ngrams(my_ngrams_directory, file_names): #'../Data/JSTOR_1GRAMS/'
				# create objects
		NGrams1 = [] 	# general list to dictionary
		n = 0 			# to count the papers info added
		y = 0 			# to count the errors occured
		
		for k in file_names:
			drctry = my_ngrams_directory + k

			try:
				with open(drctry) as f: # open 1 ngram1 file
					ngram = f.read()
					f.close()

			except IOError:
				print drctry
				print'Wrong directory'
				y = y+1

				continue

			ngram = ngram.split('\n') 
			ngram = [x.split('\t') for x in ngram] # convert text to list of tuples

			nngram = []
			for i in range(0, len(ngram)):
				if len(ngram[i]) == 1:
					continue
				nngram.append((ngram[i][0], int(ngram[i][1])))

			NGrams1.append(nngram)
			n = n+1	
			if n%500 == 0:
				print '%i files imported!'%n

		print "Ngrams files imported"
		print n
		print "Directory errors"
		print y

		return NGrams1


	print 'Reading N-grams...'
	NGrams1 = read_ngrams(my_ngrams_directory, file_names)
	#NGrams1 = read_ngrams('../Data/JSTOR_1GRAMS/', file_names)

	print '\n'
	print "Hoorey! Your Directories List is ready!"

	return NGrams1

NGrams1 = import_data('../Data/JSTOR_FULL/JSTOR_XML', '../Data/JSTOR_FULL/JSTOR_1GRAMS/')
my_stopwords= open('../Data/stopwords.txt', 'rb').read()
my_stopwords.split('\n')

def prepare_ngrams_data(my_ngrams, save_dict_directory, save_corp_directory):

	''' Takes list of 1grams dictionaries and prepares them for topic analysis '''

	print 'Excluding special characters...'
	specialchar = '[!#$%&()*+,-1234567890./:;\'<=>?@^"_{|}~]'

		# remove common words and special characters 
	print 'Excluding common/stop words...'
	#stop = stopwords.words('english')

	## Stemming 
	print 'Stemming corpus...'
	stemmer = SnowballStemmer('english')

	stem_corpus = []
	to_remove=[]
	for papers in my_ngrams:
		to_remove=[]
		for tupl in papers: # remove stop and characters
			tupl1 = (tupl[0].strip(specialchar) , tupl[1])
			if len(tupl1[0]) < len(tupl[0]):
				to_remove.append(tupl)
			elif tupl1[0] in my_stopwords:
				to_remove.append(tupl)

			elif len(tupl1[0]) <3:
				to_remove.append(tupl)

		for i in to_remove:
			papers.remove(i)

		# stem corpus
		newstem = [(stemmer.stem(word),c) for word,c in papers]
		dic = defaultdict(int)
		for tup in newstem:
			dic[tup[0]] += tup[1]

		stem_corpus.append(dic.items())


	## Export vocabulary for stemmed words..
	all_words = [[x[0] for x in tups] for tups in my_ngrams]
	all_words = list(set([y for t in all_words for y in t]))
	stemmed_words =  [(word, stemmer.stem(word)) for word in all_words]
	export_csv(stemmed_words, '../Data/JSTORstemmed_words2.csv', 'nlist')

	## Remove words that appear only once
	print 'Removing words that appear only once or everywhere(0.7)...'
	corpfreq = defaultdict(int)

	stem_clean_corpus = []
	l = 0
	for text in stem_corpus:
		# to_remove=[]
	#    for token in text:
	#        if token[1] == 1:
	#        	to_remove.append(token)

		newtext = [token for token in text if token[1] != 1]
		stem_clean_corpus.append(newtext)
		lnew = len(text)-len(newtext)
		l += lnew

		newset = [t[0] for t in newtext]
		for tok in newset:
			corpfreq[tok] += 1


	print ' %i tuples of frequency 1 has been removed!' %l

	
	stem_clean_corpus = [[token for token in abstr if corpfreq[token[0]] > len(stem_clean_corpus)*0.005 and corpfreq[token[0]]<len(stem_clean_corpus)*0.8] for abstr in stem_clean_corpus]
	
	#export_csv(frequency, '../Data/new_frequency_table.csv', 'dictionary')
	#export_csv(corpfreq, '../Data/corpus_word_frequency.csv', 'dictionary')


	print 'Saving stemmed as dictionary..'

	j_dictionary = corpora.Dictionary([[w[0] for w in ab] for ab in stem_clean_corpus])
	j_dictionary.save(save_dict_directory)
	#j_dictionary.save('../Data/Dictionaries/jstor_dictionary.dict')
	
	print 'Dictionary saved info:', j_dictionary

	print 'Tokenizing: converting strings to vectors...'
	## Matches every word in the dictionary to a number and then creates
	## a list of tuples for each document with the first element the word 
	## number and second the times it appears in the document

	## convert dictionary to a bag of words corpus for reference 

	# inverse dictionary to match words in numbers
	dictionary_info = defaultdict(int)
	for k,v in j_dictionary.iteritems():
		dictionary_info[v] = k

	final_corpus = [[(dictionary_info[w],f) for w,f in ab] for ab in stem_clean_corpus]


	# abs_corpus = [my_dictionary.doc2bow(text) for text in abs_corpus]

	# print 'Calculating words frequency (as wordsfreq)..'
	# wordsfreq = my_dictionary.token2id
	# print(my_dictionary.token2id)

	print 'Saving as bag of words corpus..'
	corpora.MmCorpus.serialize(save_corp_directory, final_corpus)
	#corpora.MmCorpus.serialize('../Data/Dictionaries/jstor_corpus.mm', final_corpus)


	print 'Preparation of data done!!'
	print '2 files have been created in memory:'
	print '1.'+ save_dict_directory + ': dictionary of all words in the main corpus'
	print '2.'+ save_corp_directory + ': tokenized vectors for words in main corpus'

	return stem_clean_corpus


stem_clean_corpus = prepare_ngrams_data(NGrams1, '../Data/Dictionaries/jstor_dictionary.dict', '../Data/Dictionaries/jstor_corpus.mm')


#######################################################
# Create document-term matrix
# doc_term_matrix = 

#######################################################
# Create Dataset 
all_data = import_data('../Data/XML', 0 , 5)

print 'Saving Abstracts in file..'
# Add header
CSVHead = ('ScopusID','Journal','Date', 'CitedBy', 'Abstract')
all_data.insert(0,CSVHead) # Insert header as first element of main list      

print 'Exporting...'
with open('../Data/all_data_NEW.csv', 'w') as tosave:
	writer = csv.writer(tosave, dialect ='excel')
	writer.writerows(all_data)
#	for i in test_corpus:
#		tosave.write('%s\n' % i)

#######################################################
# Import Clean dataframe (same in TopicAnal.py)

with open('../Data/all_data_top_300.csv', 'r') as newcsv:
	reader = csv.reader(newcsv)
	clean_data = [tuple(line) for line in reader]

# Remove header
clean_data.pop(0)

# Keep only abstracts 
print 'Keeping only abstracts..'
my_abstracts = [paper[5] for paper in clean_data]


#####################################################
# Create test corpus
print '\n'
print '**************************'
print 'Creating corpus..'
print '\n'
the_abstracts = import_data('../Data/XML', 0 , 5)
print '\n'
nostem_corpus = prepare_data(the_abstracts, '../Data/Dictionaries/my_corpus_full.dict', '../Data/Dictionaries/my_corpus_full.mm' )
print '\n'
print '**************************'

######################################################


