####################
# Visualisation

import pyLDAvis
import pyLDAvis.gensim
from IPython.core.display import display, HTML

# Prepare pyLDAvis
print 'Preparing pyLDAvis...'
text_vis_data = pyLDAvis.gensim.prepare(win_lda, scopus_corpus, scopus_dict)
print 'One done'
pyLDAvis.prepared_data_to_html(text_vis_data)
print 'Saving...'
pyLDAvis.save_html(text_vis_data, '../Results/ScopusAnalysis/lda140.html')



pyLDAvis.show(text_vis_data500)

display(text_vis_data)

pyLDAvis.save_html(text_vis_data500, '../Results/lda500.html')


pyLDAvis.enable_notebook()

text_vis_data500 = pyLDAvis.gensim.prepare(lda500, i_corpus, i_dictionary)

###########################################
# Add zeros for tSNE

c = 0
filled_corpus = []
for abstr in i_corpus:
	new_abstr = abstr
	absflat = [word for word,freq in new_abstr]

	for i in range(0,len(i_dictionary)):
		if i not in absflat:
			new_abstr.append((i,0.0))
	filled_corpus.append(sorted(new_abstr, key=lambda x: x[0]))
	c = c+1 

	if c%5000 == 0:
		print c, 'abstracts filled with zeros so far..'

print 'Wubba, lubba, dub, dub!'


dt = np.dtype('ushort, short')

my_array = np.zeros([82630,130525], dtype= short)

#np.asarray(i_corpus, dtype =dt)

##################################################################################
## Create matrix for document plotting in topic space, grouped by journal color


coloring= list(set())

##########################################
## Loading and curating the data
X = np.array(filled_data)
matrix_data = np.array(MyMatrix)
#y = np.array(coloring)
y = []
for i in range(0,len(coloring)):
	y.append(i)
y = np.array(y)

# Run tsne

def TSNE_PLOT(X, coloring):

	RS=20150101
	MyTSNE = TSNE(random_state=RS).fit_transform(X)

	df_tsne = pd.DataFrame()
	df_tsne['x-tsne'] = MyTSNE[:,0]
	df_tsne['y-tsne'] = MyTSNE[:,1]
	df_tsne['label'] = coloring

	sns.palplot(sns.hls_palette(8, l=.3, s=.8))

	chartTSNE = ggplot( df_tsne, aes(x='x-tsne', y='y-tsne', color='label') ) \
		+ geom_point(size=70,alpha=1) \
		+ ggtitle("tSNE dimensions colored by journals")
	chartTSNE


TSNE_PLOT(X, target_data)

# again for all documents data
df_tsne = pd.DataFrame()
df_tsne['label'] = target_data

# try pca 
X_pca = decomposition.TruncatedSVD(n_components=2).fit_transform(X)
df_pca = pd.DataFrame()
df_pca['x-pca'] = X_pca[:,0]
df_pca['y-pca'] = X_pca[:,1]
df_pca['label'] = target_data

chartPCA = ggplot( df_pca, aes(x='x-pca', y='y-pca', color='label') ) \
        + geom_point(size=70,alpha=1) \
        + ggtitle("PCA dimensions colored by journals")
chartPCA
