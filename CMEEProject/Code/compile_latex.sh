#!/bin/bash
#Author: Marina marina.papadopoulou16@imperial.ac.uk
#Script: 
#Date: Feb 2017

## Compile pdf
pdflatex $s.tex
bibtex $s
pdflatex $s.tex
pdflatex $s.tex
evince $s.pdf & 

## Cleanup
rm *~
rm *.aux
rm *.dvi
rm *.log
rm *.nav
rm *.out
rm *.snm
rm *.toc
rm *.log
rm *.blg
rm *.bbl

echo 'Done!'

