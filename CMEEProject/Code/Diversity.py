#!/usr/bin/env python
 
# Import csv from data
DocDistr = import_csv('../Results/dtm100_DocTopYearDistr.csv', 'list')
JYShIn = import_csv('../Results/JournalYearlyDiversity.csv', 'list')

# Goes through its row of the dataframe
# Keeps the year and the journal to a new list
# the goes through all topics probabilities, if its > 0.1, stores
# it as 1 to the new list, if not adds a 0 to this topics column
CountDistr = []
c = 0
for i in DocDistr:
	CountDistr.append([i[0], i[1]])

	for k in range(2, len(i)):
		if float(i[k]) > 0.1:
			CountDistr[c].append(1)
		else:
			CountDistr[c].append(0)
	c +=1

export_csv(CountDistr, '../Results/dtm100_CountDocTopDistr.csv', 'nlist')
CountDistr = import_csv('../Results/dtm100_CountDocTopDistr.csv', 'list')

# CountDistr: a list of lists with year and journal as first 2 columns
# and all 100 topics as rest of the columns with 1 and 0 (presence >0.1)

############################################################
# Yearly count distribution
YTopDistr = []

for i in journals:
	thisjournal = []
	for k in CountDistr:
		if k[0] == i:
			#thisjournal.append(k[2:len(k)])
			thisjournal.append(k)

	for y in years:
		thisyear = []
		for x in thisjournal:
			if int(x[1]) == y:
				thisyear.append(x)

		if len(thisyear) != 0:
			sms = [sum(x) for x in zip(*[t[2:len(thisyear[0])] for t in thisyear])]
			sms.insert(0, i)
			sms.insert(0, y)
			YTopDistr.append(sms) # COMMUNITY MATRIX TO EXPORT

export_csv(YTopDistr, '../Results/dtm100_CountYJDistr.csv', 'nlist')


############################################################
# Calculate Simpson diversity metrics for each year/journal

# Make matrix into dictionary and calculate simpson
journals = list(set([i[0] for i in CountDistr])) 
years = range(2000,2016)
#clustyears= [range(2000,2004), range(2004,2008),range(2008,2012), range(2012, 2016)]
#JYSimD = []
FullDvrst = []
JComMatrix = []

for i in journals:
	thisjournal = []
	for k in CountDistr:
		if k[0] == i:
			#thisjournal.append(k[2:len(k)])
			thisjournal.append(k)
	sms = [sum(x) for x in zip(*[t[2:len(thisjournal[0])] for t in thisjournal])]
	sms.insert(0,i)
	JComMatrix.append(sms) # COMMUNITY MATRIX TO EXPORT
	for y in years:
		thisyear = []
		for x in thisjournal:
			if int(x[1]) == y:
				thisyear.append(x[2:len(x)])

		if len(thisyear) != 0:
			# Shanon index
			SI = Shanon_index(thisyear)
			#evenness
			evness = SI/(np.log(100))

			newdict = defaultdict(int)
			c = 0
			for f in range(0,100):
				newdict[f] = sum([top[f] for top in thisyear])
				if newdict[f] != 0:
					c += 1
			# Menhinick index diveristy
			Mdiv = c/np.sqrt(sum([sum(k) for k in thisyear]))
			# Simpsons diversity
			SD = Simpson_di(newdict)
			# Inverse species diversity
			SDinv = float(1)/SD


			FullDvrst.append((i,y,SI, Mdiv, evness, SD, SDinv))


#export_csv(JYSimD, '../Results/JYSimpDvrsty.csv', 'nlist')
header = ['Journal', 'Year' , 'ShanonIndex', 'MenhinickRichness','PelousEvenness', 'SimpsonDiverity', 'EffNumSp' ]
export_csv(FullDvrst, '../Results/FullDiversity.csv', 'nlist', header)
export_csv(JComMatrix, '../Results/JCommunityMatrix.csv', 'nlist')
#############################################################
# Evenness

def Evenness(data,N):
	'''Pielou's index 
	data: as shannon index
	N: total number of topics (N)'''

	return Shanon_index(data)/np.log(N)

FullDvrst = []
for i in JYShIn:
	#evenness
	evness = float(i[2])/np.log(100)
	# Menhinick index diveristy
	Mdiv = 100/
	FullDvrst.append((i[0], i[1], float(i[2]), evness))



#############################################################

def Simpson_di(data):

    """ Given a hash { 'species': count } , returns the Simpson Diversity Index
    
    >>> simpson_di({'a': 10, 'b': 20, 'c': 30,})
    0.3888888888888889
    """

    def p(n, N):
        """ Relative abundance """
        if n is  0:
            return 0
        else:
            return float(n)/N

    N = sum(data.values())
    
    return sum(p(n, N)**2 for n in data.values() if n is not 0)


###############################################
# Calculate Shanon index for each year/journal
# Year

years = range(2000,2016)
YShIn = []
for i in years:
	thisyear = []
	thisjournal = []
	for k in CountDistr:
		if int(k[1]) == i:
			thisyear.append(k[2:len(k)])

	SI = Shanon_index(thisyear)
	YShIn.append((i,SI))


	print 'Year %s done!'%i
#save
export_csv(YShIn, '../Results/YearlyDiversity.csv', 'nlist' )


# Journals 

journals = list(set([i[0] for i in CountDistr])) 
JShIn = []
JYShIn = []

for i in journals:
	thisjournal = []
	for k in CountDistr:
		if k[0] == i:
			#thisjournal.append(k[2:len(k)])
			thisjournal.append(k)

	for y in years:
		thisyear = []
		for x in thisjournal:
			if int(x[1]) == y:
				thisyear.append(x[2:len(x)])

		if len(thisyear) != 0:
			SI = Shanon_index(thisyear)
			JYShIn.append((i,y,SI))



	#SI = Shanon_index(thisjournal)
	
	#JShIn.append((i,SI))

	print 'Journal %s done!'%i

export_csv(JShIn, '../Results/JournalDiversity.csv', 'nlist' )
export_csv(JYShIn, '../Results/JournalYearlyDiversity.csv', 'nlist' )

