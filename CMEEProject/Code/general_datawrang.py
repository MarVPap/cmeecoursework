#!/usr/bin/env python

import itertools
from operator import itemgetter
import collections

from time import time
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.metrics import silhouette_samples, silhouette_score, pairwise
#from __future__ import print_function


####################################################
# A) DATA WRANGLING 

####################################################
# Make documents distribution over words 

c = 0
words_data = []

for doc in i_corpus:
	nwords= [w for w,f in doc]
	nfreq = [f for w,f in doc]
	new_doc =[]
	for i in range(0, len(i_dictionary)):
		if i in nwords:
			new_doc.append(nfreq[nwords.index(i)])
		else:
			new_doc.append(0.)
	words_data.append(new_doc)
	c= c+1

	if c%5000 == 0:
	 	print c, 'abstracts placed in words space..'


#####################################################
# Make topics distribution over words

# Create table with topic words
#_kwargs = dict(formatted=0, num_words=20)
#topic_words100 = [[w for _, w in tups] for tups in lda100.show_topics(**_kwargs)]
topic_words100 = dict(lda100.show_topics(100))


######################################################
# Combine topic results with data (journals,ids etc)

# Add topics to document information
combined = itertools.izip(i_corpus, ((d[1], d[2]) for d in clean_data))
topic_data = []  #for each article, collect topic with highest score
#topic_stats = []  # gets all topic-score pairs for each document

print 'Pairing topics with information..' 

for corp_txt,(scid, journal) in combined:
    _srtd = sorted(lda350[corp_txt], key=itemgetter(1), reverse=1)
    top = [topic for topic, scores in _srtd]
    score = [scores for topic, scores in _srtd]
    topic_data.append((scid, journal, top, score))
    #topic_stats.append(_srtd)


##########################################################################
# Create list with journals (target) and all topics prob (filled_data)
# keep only journal and topic information from data (topic_data)
# Also create dictionary with journals and top 3 topics 

print 'Creating lists/array with journals and all topics probabilities and \n dictionary with journals and top3 topics...'

# initializing
c = 0
filled_data = [] # store topics probabilities of each document 
target_data =[] # store journal of each document

journals_topics = defaultdict(int) # store journals/top3 topics number dictionary

for doc in topic_data:

	new_doc = []
	top3 = doc[2][0:3]
	name = doc[1]

	# for dictionary
	if name in journals_topics.keys():
		journals_topics[name].extend(top3)
	else:
		journals_topics[name] = top3

	# for matrix 
	for i in range(0, 100):
		if i in doc[2]:
			new_doc.append(doc[3][doc[2].index(i)])
		else:
			new_doc.append(0)
	filled_data.append(new_doc)
	target_data.append(doc[1])
	c= c+1

	if c%5000 == 0:
		print c, 'abstracts done..'



# Count number of documents in each topic in each journal
print 'Creating matrix journals/count of doc in topics...'

MyMatrix = []
for key , value in journals_topics.iteritems():
	counts = collections.Counter(value)
	tpl = collections.OrderedDict(sorted(counts.items()))
	new_val = [key]
	for i in range(0,100):
		if i in tpl.keys():
			new_val.append(tpl[i])
		else:
			new_val.append(0)

	MyMatrix.append(new_val)

MyX = MyMatrix
coloring= [value.pop(0) for value in MyX]


# Turn filled_data into array
mdata = np.array(filled_data)
mtarget = np.array(target_data)



###############################################################################
# Creat Distance Matrix based on combined probilities of topics in journal

# Get combined propabilities
print 'Creating journals distance matrix...'

def list_average(nums):
	''' Calculates the mean of a list's elements'''
	return sum(nums)/ float(len(nums))

# First: create dictionary to calculate and store combined probabilities
JTdistr = defaultdict(int) # to fill

for i in range(0, len(target_data)):
	name = target_data[i]
	topdistr = filled_data[i]

	if name in JTdistr.keys():
		JTdistr[name] = [list_average(n) for n in zip(*[JTdistr[name],topdistr]) ]
	else:
		JTdistr[name] = topdistr

## Second: From dictionary to lists: matrix of journals-topics distributions & journals names

MatrixJTdistr = []
TargJTdistr = []
for key, param in JTdistr.iteritems():
	TargJTdistr.append(key)
	MatrixJTdistr.append(param)

## turn list to array
dist_data = np.array(MatrixJTdistr)

## Make distance matrix
dist_matrix = pairwise.pairwise_distances(dist_data)

## get commas out of some journals names (eg. paleoclimatology, paleobiology)
TargJTdistr = [re.sub(',','', x) for x in TargJTdistr]

## Add journals names at first column of array
matrixtosave =  np.concatenate((np.array(TargJTdistr)[:, np.newaxis], dist_matrix), axis = 1)

## create first row of array (same as column with empty 1,1 cell)
myheader = ','.join([''] + TargJTdistr)
#myheader = [''] + TargJTdistr

## Export arrays 
## 1. With journals names
with open('../Results/distance_matrix.txt', 'wb') as f:
	np.savetxt(f, matrixtosave, fmt = '%5s',delimiter=  ',', header = myheader)
## 2. Without journals names
with open('../Results/noheader_distance_matrix.txt', 'wb') as f:
	np.savetxt(f,dist_matrix, fmt = '%5s',delimiter=  ',')


#################################################################
# BUID PHYLOGENY

import dendropy
import ete3
from ete3 import Tree

## Get distance matrix from folder
pdm = dendropy.PhylogeneticDistanceMatrix.from_csv(
        src=open("../Results/distance_matrix.txt"),
        delimiter=",")


## 1. UPGMA APPROACH
upgma_tree = pdm.upgma_tree()
print(upgma_tree.as_string("newick"))
print(upgma_tree.as_ascii_plot())

## export in file as png
upgma_tree.render('../Results/treetest.png', dpi = 360)


## 2. NEIGHBOUR-JOINING APPROACH
nj_tree = pdm.nj_tree()
print(nj_tree.as_string("newick"))
print(nj_tree.as_ascii_plot())


##################################################################
# K-MEANS CLUSTERING AND SILHOUETTE TEST

## needed ????
msdata = scale(mdata)
n_samples, n_features = mdata.shape
n_digits = len(np.unique(mtarget))
labels = mtarget

# my function 
def kmeans_silhouette(MinNClusters, MaxNClusters, mydata):

	''' Function that runs kmeans clustering for a range of different
	number of clusters and calculate silhouette score based on euclidean
	distances. 
	MaxNClusters: the maximum number of clusters that we want to run
	MinNClusters: the minimum number of clusters that w want to run
	(the distance between the number of clusters is by 1) 
	mydata: an array with values of the features to be grouped
	sample_size: the number of the features '''

	silouet_scores = []
	nclust= []
	for i in range(MinNClusters, MaxNClusters+1,1):
		kmeansfun = KMeans(init='k-means++', n_clusters= i, n_init=10)
		# n_init: Number of time the k-means algorithm will be run with different centroid seeds.
		# The final results will be the best output of n_init consecutive runs in terms of inertia
		kmeansfun.fit(mydata)
		sil_score= metrics.silhouette_score(mydata, kmeansfun.labels_, metric='euclidean')

		silouet_scores.append(sil_score)
		nclust.append(i)
		print 'For %s clusters, the average silhouette score is %s' %(i, sil_score)

	plt.plot(nclust, silouet_scores)
	plt.xlabel("Number of Clusters")
	plt.ylabel("Silhouette Scores")
	plt.legend(("Silhouette score"), loc='best')
	plt.draw()
	plt.pause(0.01)
	raw_input("Press [enter] to continue.")
	plt.close()
	return silouet_scores

# Run function
first_silscores = kmeans_silhouette(22, dist_data)

# Silouette score for clustering

sil_score= metrics.silhouette_score(mydata, kmeansfun.labels_, metric='euclidean')



##################################################################
# VISUALIZE

#####################################
# 0) Plot  number of abstracts from each journal
# (for presentation)

# Plot journals/doc-topics counts
countjourn = [sum(value) for value in MyX]

N = 67
ind = np.arange(N)    # the x locations for the groups
width = 0.5       # the width of the bars: can also be len(x) sequence
p1 = plt.bar(ind, countjourn, width)
             
plt.ylabel('Papers counts')
plt.xlabel('Journals')
plt.yticks(np.arange(0, 9000, 500))
plt.xticks(np.arange(1, 67, 1))

plt.savefig('../Results/journcounts.png', dpi=360)


#####################################################
# 1) LDAvis

import pyLDAvis
import pyLDAvis.gensim
from IPython.core.display import display, HTML

# Prepare pyLDAvis
print 'Preparing pyLDAvis...'
text_vis_data = pyLDAvis.gensim.prepare(lda100, i_corpus, i_dictionary)
text_vis_data50 = pyLDAvis.gensim.prepare(lda50, i_corpus, i_dictionary)
text_vis_datad_dtm100 = pyLDAvis.gensim.prepare(dtm100, ready_corpus, i_dictionary)


pyLDAvis.prepared_data_to_html(text_vis_data)
#pyLDAvis.show(text_vis_data50)
#display(text_vis_data)
pyLDAvis.save_html(text_vis_data50, 'lda50.html')


######################################################
# 2) TSNE

## importing the required packages

from time import time

from matplotlib import offsetbox
from sklearn import (manifold, datasets, decomposition, ensemble,
             discriminant_analysis, random_projection)
from sklearn.manifold import TSNE
from ggplot import *
import seaborn as sns


# Add zeros for tSNE- Needed????

c = 0
filled_corpus = []
for abstr in i_corpus:
	new_abstr = abstr
	absflat = [word for word,freq in new_abstr]

	for i in range(0,len(i_dictionary)):
		if i not in absflat:
			new_abstr.append((i,0.0))
	filled_corpus.append(sorted(new_abstr, key=lambda x: x[0]))
	c = c+1 

	if c%5000 == 0:
		print c, 'abstracts filled with zeros so far..'

print 'Wubba, lubba, dub, dub!'


dt = np.dtype('ushort, short')

my_array = np.zeros([82630,130525], dtype= short)

#np.asarray(i_corpus, dtype =dt)

###########################################
## Loading and curating the data
X = np.array(MyX)
matrix_data = np.array(MyMatrix)
#y = np.array(coloring)
y = []
for i in range(0,len(coloring)):
	y.append(i)
y = np.array(y)

# Run tsne

def TSNE_PLOT(X, coloring):

	RS=20150101
	MyTSNE = TSNE(random_state=RS).fit_transform(X)

	df_tsne = pd.DataFrame()
	df_tsne['x-tsne'] = MyTSNE[:,0]
	df_tsne['y-tsne'] = MyTSNE[:,1]
	df_tsne['label'] = coloring

	sns.palplot(sns.hls_palette(8, l=.3, s=.8))


	chartTSNE = ggplot( df_tsne, aes(x='x-tsne', y='y-tsne', color='label') ) +\
		geom_point(size=70,alpha=1) + \
		ggtitle("tSNE dimensions colored by journals")

	chartTSNE.save('testplot.png')



TSNE_PLOT(X, [2000,2015])

# again for all documents data
df_tsne['label'] = mtarget

# try pca 
X_pca = decomposition.TruncatedSVD(n_components=2).fit_transform(X)
df_pca = pd.DataFrame()
df_pca['x-pca'] = X_pca[:,0]
df_pca['y-pca'] = X_pca[:,1]
df_pca['label'] = coloring

chartPCA = ggplot( df_pca, aes(x='x-pca', y='y-pca', color='label') ) \
        + geom_point(size=70,alpha=1) \
        + ggtitle("PCA dimensions colored by journals")
chartPCA


###################################################################
# 3) HEATMAP

# Dataframe heat map

df_heatmap = pd.DataFrame()

for i in range(0,len(MyMatrix)):
	df_heatmap[MyMatrix[i][0]] = [x for k,x in enumerate(MyMatrix[i]) if k!=0]
	print i

df_heatmap = df_heatmap[df_heatmap.columns].astype(int)
myhtmp = sns.heatmap(df_heatmap, yticklabels = False)
myhtmp.set_xticklabels(myhtmp.get_xticklabels(), rotation=30)

plt.savefig('../Results/heatmap.png', dpi=120)
# plt.show(myhtmp)
# plt.close()


###############################################
# 4) Topic Coherence

l100topicscoh = [v for _, v in top_topics100coher]

plt.plot(l100topicscoh)
plt.ylabel('Top topics')
plt.show()



#######################################################
# 5) SILHOUETTE PLOTS TO CHOOSE N OF CLUSTERS

# Choose silouette n of clusters
# Adapted from:
# http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_silhouette_analysis.html#sphx-glr-auto-examples-cluster-plot-kmeans-silhouette-analysis-py

range_n_clusters = [4,6, 8 ,10, 12, 14, 16]

for n_clusters in range_n_clusters:
    # Create a subplot with 1 row and 2 columns
    fig, ax1 = plt.subplots(1,1)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(dist_data) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
    clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = clusterer.fit_predict(dist_data)

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = silhouette_score(dist_data, cluster_labels)
    print("For n_clusters =", n_clusters,
          "The average silhouette_score is :", silhouette_avg)

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(dist_data, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        #color = cm.spectral(float(i) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,alpha=0.7)
                          #facecolor=color,) # edgecolor=color, )

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')

    plt.draw()
    plt.pause(0.01)
    raw_input("Press [enter] to continue.")
    plt.close()



######################################################
# 6) KMEANS -PCA based plotting
# from:
# http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html#sphx-glr-auto-examples-cluster-plot-kmeans-digits-py

reduced_data = PCA(n_components=2).fit_transform(dist_data)
kmeans = KMeans(init='k-means++', n_clusters=12, n_init=10)
kmeans.fit(reduced_data)

# Step size of the mesh. Decrease to increase the quality of the VQ.
h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

# Plot the decision boundary. For that, we will assign a color to each
x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

# Obtain labels for each point in mesh. Use last trained model.
Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure(1)
plt.clf()
plt.imshow(Z, interpolation='nearest',
           extent=(xx.min(), xx.max(), yy.min(), yy.max()),
           cmap=plt.cm.Paired,
           aspect='auto', origin='lower')

plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
# Plot the centroids as a white X
centroids = kmeans.cluster_centers_
plt.scatter(centroids[:, 0], centroids[:, 1],
            marker='x', s=169, linewidths=3,
            color='w', zorder=10)
plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
          'Centroids are marked with white cross')
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
plt.draw()
plt.pause(0.01)
raw_input("Press [enter] to continue.")
plt.close()

