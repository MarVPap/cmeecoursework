#!/usr/bin/env python

__author__  = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


# Import packages
print 'Importing packages..'

from pprint import pprint  # pretty-printer
from collections import defaultdict  # remove words that appear only once
from gensim import corpora, models, similarities
from gensim.models import CoherenceModel
from nltk import word_tokenize
from nltk.corpus import stopwords
#from sklearn import feature_extraction

#from lxml import etree # to deal with xml file format

import re 			# for use of regular expressions
import os 
import sys 
import csv 			# to import and export csvs
import nltk 		# natural language toolkit
import string
import codecs
#import mpld3
import numpy as np  # for gensim package
import pandas as pd # for dataset manipulation
import unicodedata  # to deal with xml file format
#import urllib2
import matplotlib.pyplot as plt

# Uploading files from memory (created in ImportTextData.py)
print 'Importing dictionary and corpus from memory (created in ImportTextData.py file)'
if (os.path.exists("../Data/Dictionaries/")):

	# abs_dictionary   = corpora.Dictionary.load('../Data/Dictionaries/abstracts_test.dict')
	i_dictionary = corpora.Dictionary.load('../Data/Dictionaries/my_dictionary.dict')

	# abs_corpus   = corpora.MmCorpus('/Data/Dictionaries/abstracts_test.mm')
	i_corpus = corpora.MmCorpus('../Data/Dictionaries/my_corpus.mm')

	print("Used files generated from the previous part of the code")
	
else:
	#print("No dictionary files saved.")
	sys.exit("No dictionary files saved.")

# Import Clean dataframe 
with open('../Data/all_data_top_300.csv', 'r') as newcsv:
	reader = csv.reader(newcsv)
	clean_data = [tuple(line) for line in reader]

# Remove header
clean_data.pop(0)

# Keep only abstracts 
print 'Keeping only abstracts..'
my_abstracts = [paper[5] for paper in clean_data]


# # Transformations
# print 'Runninf tfidf...'
# # Step 1: initialize(train) the transformation model
#  tfidf = models.TfidfModel(i_corpus)
# # Step 2: use model to transform vectors
# #trans_corpus = tfidf[abs_corpus]
# #tfidf.print_topics(20)

# Step 3: create topics
print 'Running LDA...' # use transformed corpus ????

def run_ldas(i_corpus, i_dictionary, save_folder, ntoplist = [50,100,150,200, 250]):

    print 'Started running models..'
    for i in ntoplist:
        ldamod= models.LdaModel(i_corpus, id2word= i_dictionary, num_topics = i)
        ldamod.save('../Results/'+save_folder+'/lda_%s.lda'%i)
        print 'lda with %s topics ready!'%i


    # lda50 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics = ntoplist[1], minimum_probability = 0.0001)
    # lda50.save('../Results/'+save_folder+'/lda_50.lda')


    # lda100 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =ntoplist[2], minimum_probability = 0.0001)
    # lda100.save('../Results/'+save_folder+ '/lda_100.lda')
    # print 'LDA 100 done!'

    # lda150 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =ntoplist[3], minimum_probability = 0.0001)
    # lda150.save('../Results/'+save_folder+ '/lda_150.lda')
    # print 'LDA 150 done!'

    # lda200 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =ntoplist[4], minimum_probability = 0.0001)
    # lda200.save('../Results/'+save_folder+ '/lda_200.lda')
    # print 'LDA 200 done!'

    # lda250 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =250)
    # lda250.save('../Results/Models/'+save_folder+ 'lda_250.lda')
    # print 'LDA 250 done!'

    # lda300 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =300)
    # lda300.save('../Results/Models/'+save_folder+ 'lda_300.lda')
    # print 'LDA 300 done!'

    # lda350 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =350)
    # lda350.save('../Results/Models/'+save_folder+ 'lda_350.lda')
    # print 'LDA 350 done!'

    # lda400 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =400)
    # lda400.save('../Results/Models/'+save_folder+ 'lda_400.lda')
    # print 'LDA 400 done!'


    # lda450 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =450)
    # lda450.save('../Results/Models/'+save_folder+ 'lda_450.lda')
    # print 'LDA 450 done!'

    # lda500 = models.LdaModel(i_corpus, id2word= i_dictionary, num_topics =500)
    # lda500.save('../Results/Models/'+save_folder+ 'lda_500.lda')
    # print 'LDA 500 done!'

# my_lda.print_topics(50)
run_ldas(j_corpus, j_dictionary, 'Full_JSTOR')

# Load models
lda20 = models.LdaModel.load('../Results/ScopusAnalysis/Models/lda_20.lda')
lda40 = models.LdaModel.load('../Results/ScopusAnalysis/Models/lda_40.lda')
lda60 = models.LdaModel.load('../Results/ScopusAnalysis/Models/lda_60.lda')
lda80 = models.LdaModel.load('../Results/ScopusAnalysis/Models/lda_80.lda')
lda100 = models.LdaModel.load('../Results/ScopusAnalysis/Models/lda_100.lda')

#######
# Initializing query
# index = similarities.MatrixSimilarity(my_lda[abs_corpus])

#############################################
# Find Optimal Number of Topics
#
jlda100 = models.LdaModel(j_corpus, id2word= j_dictionary, num_topics =100)
jlda100.save('../Results/Models/Jlda_100.lda')

my_ldas = [lda50, lda100, lda150, lda200, lda250, lda300, lda350, lda400, lda450, lda500]

def evaluate_graph(dictionary, corpus, texts, limit):
#def evaluate_graph(my_models, dictionary, corpus, texts):

    """
    from: 
    https://gist.github.com/dsquareindia/ac9d3bf57579d02302f9655db8dfdd55
    
    Parameters:
    ----------
    dictionary : Gensim dictionary
    corpus : Gensim corpus
    limit : topic limit
    
    Returns:
    -------
    lm_list : List of LDA topic models
    """
    u_mass = []
    lm_list = []
    limits = [20, 50, 100, 200, 300, limit]
    for num_topics in limits:
        lm = models.LdaModel(corpus=corpus, num_topics=num_topics, id2word=dictionary)
        lm_list.append(lm)

        cm = CoherenceModel(model=lm, corpus= corpus, coherence='u_mass')
        u_mass.append(cm.get_coherence())
        
    # Show graph
    x = range(1, limit)
    plt.plot(x, u_mass)
    plt.xlabel("num_topics")
    plt.ylabel("Coherence score")
    plt.legend(("u_mass"), loc='best')
    plt.show()
    
    export_csv(u_mass,'../Results/Coherence1.csv', 'list')
    return lm_list


my_ldas = evaluate_graph(i_dictionary, i_corpus, my_abstracts, 400)


####################################################
# If already run models
def evaluate_models_coherence(my_models, corpus):

	u_mass = []
	c = 0
	for i in my_models:
		cm = CoherenceModel(model= i, corpus= corpus, coherence='u_mass')
		u_mass.append(cm.get_coherence())
		c = c+1
		print 'Model %s coherence added!' %c

		if c % 3 == 0:
			export_csv(u_mass, '../Results/ScopusAnalysis/coherence1.csv', 'flist', range(0,c,1))

	export_csv(u_mass, '../Results/ScopusAnalysis/UMassCoh2.csv', 'flist', range(0,c,1))

	# Show graph
	x = [20,40,60,80, 100]
	plt.plot(x, u_mass)
	plt.xlabel("Number of topics")
	plt.ylabel("Coherence score")
	plt.legend(("umass"), loc='best')
	plt.savefig('../Results/ScopusAnalysis/My_Coherence2.png', dpi = 360)
	plt.close()

	return u_mass

print 'Getting coherence list for models...'

coherence_list = evaluate_models_coherence(my_ldas, i_corpus)

################################################################################

top_topics350coher = lda350.top_topicc(i_corpus)
dic = corpora.Dictionary(i_dictionary)

coh100 = CoherenceModel(model=lda100, corpus= i_corpus, coherence='u_mass')
co100 = coh100.get_coherence()

coh100 = CoherenceModel(model=lda100, texts= my_abstracts, dictionary=i_dictionary, coherence='u_mass')

coh50.get_coherence()


cm50 = -4.596016008622805
co100 = -13.758146236053554



#################################################
# Clean dictionary

wordsfreq = i_dictionary.token2id

##################################################
# 

import itertools
from operator import itemgetter

# Create table with topic words
#_kwargs = dict(formatted=0, num_words=20)
#topic_words100 = [[w for _, w in tups] for tups in lda100.show_topics(**_kwargs)]
topic_words100 = dict(lda100.show_topics(100))



# Add topics to document information
combined = itertools.izip(i_corpus, ((d[1], d[2]) for d in clean_data))
topic_data = []  #for each article, collect topic with highest score
#topic_stats = []  # gets all topic-score pairs for each document

for corp_txt,(scid, journal) in combined:
    _srtd = sorted(lda100[corp_txt], key=itemgetter(1), reverse=1)
    top = [topic for topic, scores in _srtd]
    score = [scores for topic, scores in _srtd]
    topic_data.append((scid, journal, top, score))
    #topic_stats.append(_srtd)

#topic_stats = [tup for tups in topic_stats for tup in tups]  #flatten list(tuples) -> list

my_dataframe = pd.DataFrame(topic_data, columns=['ScopusID', 'Journal', 'Topic', 'Score'])
print my_dataframe.shape
my_dataframe.head()

export_csv(topic_data, 'lda100_topics_docs.csv', 'list')
export_csv(topic_words100, 'lda100_topics_words.csv', 'dictionary')
