# setting imports

from gensim.models import ldaseqmodel
from gensim.corpora import Dictionary
from gensim.matutils import hellinger
import itertools
from operator import itemgetter
import collections
 
###############################
# Prepair time slices

print 'Pairing dates with corpus data..' 

combined2 = itertools.izip(i_corpus, (d[6] for d in clean_data))
date_data = []  #for each article, collect time slices


print 'Making list..'
for corp_txt,year in combined2:
	date_data.append((year, corp_txt))

date_data = sorted(date_data, key=itemgetter(0), reverse=0)

# remove mistaken 2017 
to_remove = []
for i in date_data:
	if i[0] == '2017':
		to_remove.append(i)
		print 'Found one!'
for i in to_remove:
	date_data.remove(i)


# Count each years papers
print 'Counting number each year'

YearsCounts= defaultdict(int) # to fill
yearlist = sorted(list(set([date for date, corp in date_data])))
TimeSteps =[] ## My time steps for model

for i in date_data:
	YearsCounts[i[0]] +=1

for i in yearlist:
	TimeSteps.append(YearsCounts[i])

# my sorted corpus for model
s_corpus = [corp for date,corp in date_data] 

ready_corpus = [[(i,int(f)) for i,f in ab] for ab in s_corpus]

print 'Time steps and sorted corpus ready for the model!'


###############################
# Run model (and time it)

# print ' '
# print 'Running DTM...'

# dylda_50 = ldaseqmodel.LdaSeqModel(corpus = s_corpus, id2word= i_dictionary, time_slice =TimeSteps, num_topics=50, chunksize=100)

# print 'Saving DTM..'
# dylda_50.save('dynlda50')

# print 'Done!'

#############################
# in C Version

# 1. Write seq file with number of documents for each time step

seq_outfile = open(os.path.join('../Data/DTM', 'years-seq.dat'), 'w')
seq_outfile.write(str(len(TimeSteps)) + '\n') # n of timestamps

for count in TimeSteps:
	seq_outfile.write(str(count)+ '\n') # write total tweets per year

seq_outfile.close()
print 'Done writing seq'

# 2. Write corpus in format needed and create file to put in
multfile = open(os.path.join('../Data', 'DTM', 'mult.dat'), 'w')
c = 0
# turn float count into integer
ready_corpus = [[(i,int(f)) for i,f in ab] for ab in s_corpus]

for i in ready_corpus:
	multfile.write(str(len(i))+ ' ')
	for (wordID, weigth) in i:
		multfile.write(str(wordID)+ ':' + str(weigth) + ' ')
	multfile.write('\n')
	c = c+1
	if c % 5000 == 0 :
		print c, 'documents passed!'

multfile.close()
print('mult file saved')

# 3. Set paths to executable

# path to dtm home folder
dtm_home = os.environ.get('DTM_HOME', "dtm-master")
# path to the binary
dtm_path = os.path.join(dtm_home, 'dtm', 'main')

# 4. Run models

dtm100 = DtmModel(dtm_path, ready_corpus, TimeSteps, num_topics =100, id2word = i_dictionary,initialize_lda= True)

dim100 = gensim.models.wrappers.DtmModel(dtm_path, ready_corpus, TimeSteps, model= 'fixed', num_topics =100, id2word = i_dictionary,initialize_lda= True)

dtm100.save('../Results/Models/dtm/dtm100.lda')


#returns term_frequency, vocab, doc_lengths, topic-term distributions and 
#doc_topic distributions, specified by pyLDAvis format. all of these are needed 
#to visualise topics for DTM for a particular time-slice via pyLDAvis.
#input parameter is the year to do the visualisation.


# See all methods of object!!!
#[method for method in dir(dtm100) if callable(getattr(dtm100, method))]

#############################################################
# Handle data

#Import to tethne
dtm = tethne.model.corpus.dtmmodel.from_gerrish('/Results/test_dtm_run/output/', 
'/Data/DTM/-mult.dat', '/Data/DTM/-seq.dat')

##############################################################
# With dtm in python

topicstest= dtm100.show_topic(topicid =1, time =0, num_words = 10)
# show_topics(num_topics=10, times=5, num_words=10, log=False, formatted=True)
# formatted prints as prob*string of words strig

DocTopDist = dtm100.gamma_
years= range(2000, 2016)

################################################
# Topic word table

TopicsYWords = []
for i in range(0,100):
	newtopic = []
	for k in range(0,16):
		newtopic.append(dtm100.show_topic(topicid =i, time =k, num_words = 20))

	TopicsYWords.append(newtopic)
# TopicsYWords is  a list of lists of lists of tuples
# 1st level corresponds to topics (0,100)
# 2nd level are the years (0,16)
# 3rd are 20 words with their probablilies of each year-topic

# See distances in word distribution of topics through time
# Same as above but get all words from each topic (not just top 20)

FullTopicsYWords = []
for i in range(0,100):
	newtopic = []
	for k in range(0,16):
		newtopic.append(dtm100.show_topic(topicid =i, time =k, num_words = len(i_dictionary)))

	FullTopicsYWords.append(newtopic)

print 'First List Done!'

FullTopWordsDistr = [] 
for top in FullTopicsYWords:
	newtop = []
	for year in top:
		ytwords = [w for p,w in year]
		ytprob = [p for p,w in year]
		newfulline = []

		for k in range(0,len(i_dictionary)):
			nword = i_dictionary[k]
			if nword in ytwords:
				newfulline.append(ytprob[ytwords.index(nword)])
			else:
				newfulline.append(0)
				print 'Word not from topic found!!'
		newtop.append(newfulline)
	# print 'Topic Done!'

	FullTopWordsDistr.append(newtop)
# FullTopWordsDIstr is a list of lists of lists with all the words probabilities
# of the dictionary for each topic in each year
# 1st level: topics
# 2nd: years
# 3rd: words probabilities

## PCA
Topic1 = np.array(FullTopWordsDistr[0])
Topic100 = FullTopWordsDistr[0][0]
Topic115 = FullTopWordsDistr[0][15]
X = np.array([Topic100, Topic115])

from sklearn.decomposition import PCA

###

def PCA_PLOT(X, coloring, save_path, i):

	X_pca= PCA(n_components=2).fit_transform(X)

	df_pca = pd.DataFrame()
	df_pca['x-pca'] = X_pca[:,0]
	df_pca['y-pca'] = X_pca[:,1]
	df_pca['label'] = coloring

	sns.palplot(sns.hls_palette(15, l=.3, s=.8))


	chartPCA = ggplot( df_pca, aes(x='x-pca', y='y-pca', color='label') ) +\
		geom_point(size=70,alpha=1) + \
		ggtitle("Topic %s"%i)

	chartPCA.save(save_path)

years = range(2000,2016)

for i in range(0,100):
	#X = np.array(FullTopWordsDistr[i])
	#PCA_PLOT(X, years, '../Results/PCAs/%s.png'%i , i)
	FL = [FullTopWordsDistr[i][0],FullTopWordsDistr[i][15]]
	XFL =np.array(FL)
	PCA_PLOT(XFL, [2000,2015], '../Results/PCAs/%s_00_15.png'%i , i)



import numpy as np
from sklearn.decomposition import PCA
import pandas as pd
import matplotlib.pyplot as plt
from sklearn.preprocessing import scale





def TSNE_PLOT(X, coloring, save_path):

	RS=20150101
	MyTSNE = TSNE(random_state=RS).fit_transform(X)

	df_tsne = pd.DataFrame()
	df_tsne['x-tsne'] = MyTSNE[:,0]
	df_tsne['y-tsne'] = MyTSNE[:,1]
	df_tsne['label'] = coloring

	sns.palplot(sns.hls_palette(8, l=.3, s=.8))


	chartTSNE = ggplot( df_tsne, aes(x='x-tsne', y='y-tsne', color='label') ) +\
		geom_point(size=70,alpha=1) + \
		ggtitle("tSNE dimensions colored by journals")

	chartTSNE.save(save_path)

plt.plot(FullTopWordsDistr[0])
plt.pause(0.01)
raw_input("Press [enter] to continue.")
plt.close()


years = range(2000,2016)

for i in range(0,100):
	X = np.array(FullTopWordsDistr[i])
	TSNE_PLOT(X, years, '../Results/TSNEs/%s.png'%i)

#################
#########
# Word CLouds 

from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

# Initialize the word clouds

my_color= np.array(Image.open('masktest.png'))

wc = WordCloud(
    background_color="white",
    max_words=50,
    width = 1024,
    height = 720,
    prefer_horizontal = 0.99999 ,
    mask= my_color,
    stopwords=stopwords.words("english"),
    margin = 10
)

image_colors = ImageColorGenerator(my_color)

# Save word cloud for each year/topic group

c=0
y=0
for topic in TopicsYWords:
	c= c+1
	#if TopicsYWords.index(topic) < 48:
	#if c < 73:
	#	continue
	for year in topic:
		y= y+1
		n_path= '../Results/Wordclouds/%s_%s.png' %(c,y)
		new = defaultdict(int)

		for i in year:
			new[i[1]] = i[0]
		wc.generate_from_frequencies(new)
		plt.imshow(wc.recolor(color_func=image_colors), interpolation="bilinear")
		wc.to_file(n_path)
#  pdfjam  
	print 'Topic %s done!' %c
	y=0


############
# Join wordcounts in pdf (name for terminal)
# pdfjam %%%% --frame true --pagenumbering true --outfile AllPlots.pdf

name = []
for i in range(14):
	name.append('%s__CL.png'%i)
' '.join(name)

#pdfjam 0__CL.png 1__CL.png 2__CL.png 3__CL.png 4__CL.png 5__CL.png 6__CL.png 7__CL.png 8__CL.png 9__CL.png 10__CL.png 11__CL.png 12__CL.png 13__CL.png --nup 4x4 --frame true --outfile ClustWC140.pdf




#####################################################
# Topic Documents

# vocabulary: [4]
# term_frequency: [3] (from vocabulary)
# doc_lengths: [2]
# topic_term distribution: [1]
# doc_topic distribution: [0] - each array[] probability of 1 document for each topic from 100

doctopicdistr0,topictermdistr0, doc_lengths0 , termfreq0, vocab0 = dtm100.dtm_vis(ready_corpus, 0)

def list_average(nums):
	''' Calculates the mean of a list's elements'''
	return sum(nums)/ float(len(nums))

#extract for each topic distrib for year
f = 0
s = 0
YearTop = defaultdict(int)
YearJournTop = defaultdict(int)
YearDocTop = defaultdict(int)
years = range(2000,2016,1)


for i in range(0,16): # loop through years
	yearsdistr = dtm100.dtm_vis(ready_corpus, i)[0]
	topyear = defaultdict(int)
	journtopyear = defaultdict(int)

	for k in range(f, f+TimeSteps[s]): # loop through documents of each year
	#for k in range(0, len(ready_corpus)): # to go through all document (mean of topics)
		newdoc = list(yearsdistr[k])
		jrnl = date_data2[k][1]
		# print 'In documents'

		for x in range(0,100): # loop through topics in each document
			topyear[x] = (topyear[x]+newdoc[x])/2
		
		if jrnl in journtopyear.keys():
			journtopyear[jrnl] = [list_average(n) for n in zip(*[journtopyear[jrnl],topyear.values()])]
			#journtopyear[jrnl].append(topyear.values())
		else:
			journtopyear[jrnl] = [topyear.values()]


	#YearTop.append(topyear.values()) # if I was a list
	YearTop[years[i]] = topyear.values()
	YearJournTop[years[i]] = journtopyear
	f += TimeSteps[s]
	s += 1
	print 'Year', i ,'done!'

# make dataframe
dtm100YTdf= pd.DataFrame.from_dict(YearTop) # years columns / topics rows
tdtm100YTdf = dtm100YTdf.transpose() # topics columns/ years rows
tdtm100YTdf.to_csv('../Results/dtm100_YearlyTopicDistr.csv')

dtm100YTdf.plot()

############################
# Create Topic Distribution over documents with year and journal in
f = 0
s = 0
DocTopYdistr = []
PCAdf = []
for i in range(0,16):
	yearsdistr = dtm100.dtm_vis(ready_corpus, i)[0]
	topyear = defaultdict(int)
	journtopyear = defaultdict(int)
	for k in range(f, f+TimeSteps[s]): 
		newdoc = list(yearsdistr[k])
		nline = [date_data2[k][1] , years[i]] + newdoc
		DocTopYdistr.append(nline)
		PCAdf.append(newdoc)

	f += TimeSteps[s]
	s += 1
	print 'Year', i ,'done!'

# Calculate mean and std
values = range(2,102)
ValDist = [itemgetter(*values)(item) for item in DocTopYdistr]
np.mean(ValDist) # 0.01
np.std(ValDist) # 0.044

# Documents/Topics dataframe
export_csv(DocTopYdistr, '../Results/DocTopYearDistr.csv', 'nlist')
DTYdf= pd.DataFrame(DocTopYdistr)
pca_df = pd.DataFrame(PCAdf)

# dataframe for journals
#convert it to numpy arrays
X=pca_df.values

#Scaling the values
X = scale(X)
pca = PCA()

pca.fit(X)

#The amount of variance that each PC explains
var= pca.explained_variance_ratio_

#Cumulative Variance explains
var1=np.cumsum(np.round(pca.explained_variance_ratio_, decimals=4)*100)
plt.plot(var1)
plt.show()
plt.close()

##
journals = [y[0] for y in DocTopYdistr]

PCA_PLOT(pca_df , journals, '../Results/PCAs/DocTopics.png', 0)


# First: create dictionary to calculate and store combined probabilities
JTdistr = defaultdict(int) # to fill

for i in range(0, len(target_data)):
	name = target_data[i]
	topdistr = filled_data[i]

	if name in JTdistr.keys():
		JTdistr[name] = [list_average(n) for n in zip(*[JTdistr[name],topdistr]) ]
	else:
		JTdistr[name] = topdistr

##################################################
#Add journals
combined3 = itertools.izip(i_corpus, ((d[2],d[6]) for d in clean_data))

date_data2 = []
print 'Making list..'
for corp_txt, (journal, year) in combined3:
	date_data2.append((year, journal, corp_txt))

date_data2 = sorted(date_data2, key=itemgetter(0), reverse=0)

# remove mistaken 2017 
to_remove = []
for i in date_data2:
	if i[0] == '2017':
		to_remove.append(i)
		print 'Found one!'
for i in to_remove:
	date_data2.remove(i)

# my sorted corpus for model
s2_corpus = [corp for date,corp in date_data2] 

ready_corpus = [[(i,int(f)) for i,f in ab] for ab in s2_corpus]



##########################################################################
# DTM change of topics through time
from math import log

YTDistr = import_csv('../Results/dtm100_YearlyTopicDistr.csv', 'list')
#take header out
YTDistr.pop(0)

# Go through the years and take begining and end
TopChange = []
for i in range(1,101):
	topc = [t[i] for t in YTDistr]
	T0 = log(topc[0])
	TN = log(topc[15])
	dist = T0-TN
	TopChange.append((i,dist))

plt.scatter(*zip(*TopChange))
plt.pause(0.1)
raw_input("Press [enter] to continue.")
plt.close()