#!/usr/bin/env python
######
# Import data for analysis


__author__  = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


# Import packages
print 'Importing packages..'

from pprint import pprint  # pretty-printer
from collections import defaultdict  # remove words that appear only once
from gensim import corpora, models, similarities
from gensim.models import CoherenceModel
from nltk import word_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer # for stemming
import pyLDAvis
import pyLDAvis.gensim
from IPython.core.display import display, HTML

from time import time
from matplotlib import offsetbox
import seaborn as sns

import pyLDAvis
import pyLDAvis.gensim
from IPython.core.display import display, HTML

import itertools
from operator import itemgetter
import collections

from scipy.cluster.hierarchy import cophenet
from scipy.cluster.hierarchy import dendrogram, linkage


import re 			# for use of regular expressions
import os 
import sys 
import csv 			# to import and export csvs
import nltk 		# natural language toolkit
import string
import codecs
#import mpld3
import numpy as np  # for gensim package
import pandas as pd # for dataset manipulation
import unicodedata  # to deal with xml file format
#import urllib2
import matplotlib.pyplot as plt

# Uploading files from memory (created in ImportTextData.py)
print 'Importing dictionary and corpus from memory (created in ImportTextData.py file)'
if (os.path.exists("../Data/ScopusDictCorp/")):

	# abs_dictionary   = corpora.Dictionary.load('../Data/Dictionaries/abstracts_test.dict')
	#i_dictionary = corpora.Dictionary.load('../Data/Dictionaries/my_dictionary.dict')
	#j_dictionary = corpora.Dictionary.load('../Data/Dictionaries/jstor_dictionary.dict')
	scopus_dict = corpora.Dictionary.load('../Data/ScopusDictCorp/scopus_dict.dict')
	# abs_corpus   = corpora.MmCorpus('/Data/Dictionaries/abstracts_test.mm')
	#i_corpus = corpora.MmCorpus('../Data/Dictionaries/my_corpus.mm')
	#j_corpus = corpora.MmCorpus('../Data/Dictionaries/jstor_corpus.mm')
	scopus_corpus = corpora.MmCorpus('../Data/ScopusDictCorp/scopus_corpus.mm')

	print("Used files generated from the previous part of the code")
	
else:
	#print("No dictionary files saved.")
	sys.exit("No dictionary files saved.")

#  Import Clean dataframe 

with open('../Data/all_data_top_300.csv', 'r') as newcsv:
	reader = csv.reader(newcsv)
	clean_data = [tuple(line) for line in reader]

# IMPORT DATA
CDataDict = import_csv('../Results/ScopusAnalysis/CleanData.csv', 'dictionary', 'strings')
my_abstracts = [x[2].lower() for x in CDataDict.values()]

# Remove header
#clean_data.pop(0)
# Keep only abstracts 


# Import ldas
print 'Importing LDAs...'

#lda100 = models.LdaModel.load('../Results/Models/lda_100.lda')
#lda100_long = models.LdaModel.load('../Results/Models/lda_100_large.lda')

#dtm100 = models.wrappers.DtmModel.load('../Results/ModelsScopus/dtm/dtm100.lda')

# lda20 = models.LdaModel.load('../Results/TESTMODELS/lda_20.lda')
# lda50 = models.LdaModel.load('../Results/TESTMODELS/lda_50.lda')
# lda100 = models.LdaModel.load('../Results/TESTMODELS/lda_100.lda')
# lda150 = models.LdaModel.load('../Results/TESTMODELS/lda_150.lda')
# lda200 = models.LdaModel.load('../Results/TESTMODELS/lda_200.lda')
# lda100as = models.LdaModel.load('../Results/ModelsScopus/scopus_lda100_assymetric.lda')


# lda0 = models.LdaModel.load('../Results/ModelsScopus/scopus_lda100_rd0.lda')
# lda1 = models.LdaModel.load('../Results/ModelsScopus/scopus_lda100_rd1.lda')
# lda1 = models.LdaModel.load('../Results/ModelsScopus/scopus_lda100_rd1.lda')
# mdiff , annot = lda0.diff(lda1)

########################################################################
# Importing JSTOR

#xmlInfo = import_csv('../Results/JSTOR/xmlInfo.csv', 'list','strings')

print 'Wubba, lubba, dub, dub!'