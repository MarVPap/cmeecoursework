
# Run Analysis

# Make documents distribution over words 
DocWordDistr = create_DocWordDistr(i_corpus, i_dictionary)

# Make list of tuples of 50 most common words in topics with probabilities (for wordclouds)

TopicsYWords= create_topics_words_table(lda100_long, 50)


# FOR TOPIC DISTRIBUTION OVER WORDS

# Make topics distribution over words fpr dtm
TopWordsDistrLDA = create_TopWordsDistr(lda100_long, i_dictionary, '../Results/New_Full/TopWordDistrLDA.csv', time = False)
export_csv(TopWordsDistrLDA, '../Results/New_Full/TopWordDistrLDA_long.csv', 'nlist')
# Make topics distribution o, ver words fpr dtm
TopWordsDistr = create_TopWordsDistr(dtm100, i_dictionary, '../Results/New_Full/YearTopWordDistr.csv', time = True)

# from dtm calculate average for all years:
TWdistr = [] # to fill
for topic in TopWordsDistr:
	aver_distr = [float(sum(elem))/len(elem) for elem in zip(*topic)]
	TWdistr.append(aver_distr)
# created a list (100 topics) of lists(2310 words probabilities) for all topics in words
export_csv(TWdistr, '../Results/New_Full/TWdistr.csv', 'nlist')


# Create distance matrix
print 'Creating distance matrix...'

def hellinger(X, conc):
#https://blakeboswell.github.io/2016/gensim-hellinger/
	if conc == True:
		return numpy.triu(squareform(pdist(numpy.sqrt(X)))/numpy.sqrt(2))
	else:
		return squareform(pdist(numpy.sqrt(X)))/numpy.sqrt(2)


TopicsDistMatrix = hellinger(TopWordsDistrLDA)
numpy.savetxt("../Results/New_Full/TopDistMatrix.csv", TopicsDistMatrix, delimiter=",")

Topics = ['T_'+ str(i) for i in range(1,101)]
matrixtosaveT = numpy.concatenate((numpy.array(Topics)[:, numpy.newaxis], TopicsDistMatrix), axis = 1)
myheaderT =  ','.join([''] + Topics)

with open("../Results/New_Full/TopDistMatrixH.csv", 'wb') as f:
		numpy.savetxt(f, matrixtosaveT, fmt = '%5s',delimiter=  ',', header = myheaderT)


# Create dendogram
PhylTopicDistM = dendropy.PhylogeneticDistanceMatrix.from_csv(src=open("../Results/New_Full/TopDistMatrixH.csv"),
	        delimiter=",")

# ##  NEIGHBOUR-JOINING APPROACH

# TopicTree = PhylTopicDistM.nj_tree()

# ## Save
# tosavetree = TopicTree._as_newick_string() + ';'
# NjTreeT = ete3.Tree(tosavetree)
# NjTreeT.write(format=1, outfile= "../Results/New_Full/TopicsTree.nw")


# TOPIC CLUSTERING BASED ON THE TREE
heatTopics = seaborn.heatmap(TopicsDistMatrix, xticklabels= Topics, yticklabels=False,  cbar=False)
plt.savefig("../Results/New_Full/heatTop3.eps",  format='eps', dpi=2000)

seaborn.clustermap(TopicsDistMatrix, method="ward")
plt.savefig("../Results/New_Full/heatTop5.eps",  format='eps', dpi=2000)

# HIERARCHICAL CLUSTEING

from scipy.cluster.hierarchy import cophenet
from scipy.cluster.hierarchy import dendrogram, linkage

# Create flat matrix 
LinkTop = linkage(TopicsDistMatrix, method= 'ward')
metric, coph_dists = cophenet(LinkTop, pdist(TopicsDistMatrix))

#plottin Dendogram

def check_clustering(X, clustmethod, d_thres ,figformat):

	LinkTop = linkage(X, method= clustmethod)

	def plot_dendogram(X, linktype, d_thres, svdict = "../Results/New_Full/sdendogram_"):
		plt.figure(figsize=(25, 10))
		plt.title('Hierarchical Clustering Dendrogram - %s' %clustmethod)
		plt.xlabel('Topics')
		plt.ylabel('distance')
		dendrogram(
			X, leaf_rotation=90.,  # rotates the x axis labels
			leaf_font_size=10.,  # font size for the x axis labels 
			)
		savedict = svdict + linktype + figformat
		plt.savefig(savedict)


	plot_dendogram(LinkTop, clustmethod, d_thres)

	clusters = fancy_dendrogram(
		LinkTop,
		#truncate_mode='lastp',
		#p=6,
		leaf_rotation=90.,
		leaf_font_size=7.,
		#show_contracted=True,
		#annotate_above=40,
		max_d= d_thres
		)
	plt.close()

	return clusters


plt.title('Hierarchical Clustering Dendrogram (truncated)')
plt.xlabel('sample index')
plt.ylabel('distance')
dendrogram(
    LinkTop,
    truncate_mode='lastp',  # show only the last p merged clusters
    p=21,  # show only the last p merged clusters
    show_leaf_counts=False,  # otherwise numbers in brackets are counts
    leaf_rotation=90.,
    leaf_font_size=12.,
    show_contracted=True,  # to get a distribution impression in truncated branches
)
plt.savefig("../Results/New_Full/dendogramtrunk.pdf")

# Choose clusters
# 1. Inconsistency method
from scipy.cluster.hierarchy import fcluster

clusters= fcluster(LinkTop, 30 , criterion = 'maxclust')

plt.close('all')
plt.figure(figsize=(10,10))
fancy_dendrogram(
    LinkTop,
    #truncate_mode='lastp',
    #p=6,
    leaf_rotation=90.,
    leaf_font_size=7.,
    #show_contracted=True,
    #annotate_above=40,
    max_d=1.4,
)
plt.savefig("../Results/New_Full/Wardtotest.eps")

#########################################################
# Run clustering for all clustering methods
my_clusters = defaultdict(int)
all_methods = ['complete','single', 'average', 'ward', 'centroid','median', 'weighted']

for i in all_methods:
	new = cluster_topics(TopWordsDistrLDA, 1.4, figtype= 'eps',clustmethod = i)
	my_clusters[i] = new
	print '%s done!' %i

# do same for dtm
dtm_clusters = defaultdict(int)
for i in all_methods:
	new = cluster_topics(TWdistr, 1.4, figtype= 'dtm.eps',clustmethod = i)
	dtm_clusters[i] = new
	print '%s done!' %i

# know that we choose ward
dtm_Ward = 	cluster_topics(TWdistr, 1.4, figtype= 'dtm.eps',clustmethod = 'ward')


###########
my_clusters = defaultdict(int)
for i in all_methods:
	#new = cluster_topics(TopWordsDistrLDA, 0.5, figtype= 'eps',clustmethod = i)
	new = check_clustering(TopicsDistMatrix,i, 1.4,'.eps')
	my_clusters[i] = new
	
	print '%s done!' %i

# choose ward from 1st

my_ward = my_clusters['ward']
dtm_ward = dtm_clusters['ward']

def create_ClustDistr(my_ward, TopWordsDistr):

	wcolors = my_ward['color_list']
	wtopics = my_ward['leaves']

	wcolors = [x for x in wcolors if x != 'b'] # rv blue nodes- big

	# GET CLUSTERS

	TopicsClusters = defaultdict(int)
	c = 0
	t = 0
	n = 0
	iprev = wcolors[0]
	new = []
	for i in wcolors:
		if i == iprev:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1

		else:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			c = c+1
			t = t+1
			n = n+1
			new = []
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1
			iprev = i

			print 'New cluster n. %s' %n
	new.append(wtopics[t])
	TopicsClusters[c] = new

	# Create words distributions for each cluster (Average all)

	ClustDistr = []
	for i in TopicsClusters.values():
		toclust = []
		for top in i:
			toclust.append(TopWordsDistr[top])

		newclust = [float(sum(elem))/len(elem) for elem in zip(*toclust)]
		ClustDistr.append(newclust)

	return ClustDistr

# See words of clusters
# Create list of tuples (word, frequency) for each cluster

garbage = ['et', 'al', 'elsevi', 'b', 'scienc', 'bv', 'nov', 'inf', 'n']

def clusters_forWC(ClustDistr, n_words, garbage):

	ClustWords = []
	c = 0

	for i in ClustDistr:

		new_line = []
		for n, w in i_dictionary.iteritems():
			#freqs.append(i[n])
			if w in garbage:
				continue
			else:
				new_line.append((w , i[n]))


		ClustWords.append(new_line)
		print 'Cluster %s done!' %c
		c = c+1 

	# Keep only 50 
	import heapq

	ClustW50 = []
	n = 0
	for i in ClustWords:
		incloud = []
		freqs = [f for w,f in i]
		lar = heapq.nlargest(n_words, freqs)
		for k in lar:
			incloud.append(i[freqs.index(k)])

		ClustW50.append(incloud)
		print 'Cluster %s sorted!' %n
		n += 1

	return ClustW50

# Make word clouds 

make_wordclouds(ClustW50, 'cl')


############################################
# GREAT! NOW REMOVE TOPIC 78 CAUSE RUBISH
del TopWordsDistrLDA[78]
# run again and compare results
TopicsDistMatrix = hellinger(TopWordsDistrLDA, False)
LinkTop = linkage(TopicsDistMatrix, method= 'ward')
metric, coph_dists = cophenet(LinkTop, pdist(TopicsDistMatrix))
print 'Comphenet is %s' %metric

plt.close('all')
plt.figure(figsize=(10,10))
w_clust_n = fancy_dendrogram(
   	LinkTop,
    #truncate_mode='lastp',
    #p=6,
    leaf_rotation=90.,
    leaf_font_size=7.,
    #show_contracted=True,
    #annotate_above=40,
    max_d=1.43,
)
plt.savefig("../Results/New_Full/Wardtotest_clean2.eps")

# Remove bad wrong words and standardize from 0 to 1 again


# seems good so do rest 
ClustDistr = create_ClustDistr(w_clust_n, TopWordsDistrLDA)
ClustW50 = clusters_forWC(ClustDistr, 50, garbage)
make_wordclouds(ClustW50, 'newC2') # nice!

# DO FOR DTM 
# check individual topics to throw away
garbage = []
Dtm50= clusters_forWC(TWdistr, 50, garbage)
make_wordclouds(Dtm50, 'dtm')


dtm_ward = dtm_clusters['ward']
ClustDistrDTM = create_ClustDistr(dtm_ward,  TWdistr)
ClustW50DTM = clusters_forWC(ClustDistrDTM, 50, garbage)
make_wordclouds(ClustW50DTM, 'Cdtm') # nice!


# remove bad topics
# 42,52,46,32, 38, 48,69] #bad topics
bad_topics = [32, 38,42,46, 48,52,69] #long

ind = 0
for i in bad_topics:
	k = i - ind
	del TWdistr[k]
	ind = ind+1

# run again and compare results
TopicsDistMatrixD = hellinger(TWdistr, False)
LinkTopD = linkage(TopicsDistMatrixD, method= 'ward')
metric, coph_dists = cophenet(LinkTopD, pdist(TopicsDistMatrixD))
print 'Comphenet is %s' %metric

plt.close('all')
plt.figure(figsize=(10,10))
w_clust_nD = fancy_dendrogram(LinkTopD,leaf_rotation=90.,leaf_font_size=7.,max_d=1.43)

plt.savefig("../Results/New_Full/Wardtotest_cleanDTM.eps")

ClustDistrDTM2 = create_ClustDistr(w_clust_nD,  TWdistr)
ClustW50DTM2 = clusters_forWC(ClustDistrDTM2, 50, garbage)
make_wordclouds(ClustW50DTM2, 'Cdtm_cl') # nice!


# take probabilities of clusters changing for each year from ind topics

YTdistr = import_csv('../Results/dtm100_YearlyTopicDistr.csv', 'list', 'floats')
#remove years from begging of list
YTdistrDTM=  [y[1:] for y in YTdistr]
YTdistrDTM.pop(0)

def get_ClusterGroups(my_ward):

	wcolors = my_ward['color_list']
	wtopics = my_ward['leaves']

	wcolors = [x for x in wcolors if x != 'b'] # rv blue nodes- big

	# GET CLUSTERS

	TopicsClusters = defaultdict(int)
	c = 0
	t = 0
	n = 0
	iprev = wcolors[0]
	new = []
	for i in wcolors:
		if i == iprev:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1

		else:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			c = c+1
			t = t+1
			n = n+1
			new = []
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1
			iprev = i

			print 'New cluster n. %s' %n
	new.append(wtopics[t])
	TopicsClusters[c] = new

	return TopicsClusters

TopClust = get_ClusterGroups(w_clust_nD)

# average yearly topic probability for topics clustered together
YCdistr = defaultdict(int)
for i in range(len(TopClust)):
	toaverage = []
	for k in TopClust[i]:
		toaverage.append([tr[k] for tr in YTdistrDTM])

	newclust = [float(sum(elem))/len(elem) for elem in zip(*toaverage)]
	YCdistr[i] = newclust

dtm100YCdf= pd.DataFrame.from_dict(YCdistr) # years columns / clusters rows
tdtm100YCdf = dtm100YCdf.transpose() # topics columns/ years rows
tdtm100YCdf.to_csv('../Results/dtm100_YearlyClustersDistr.csv')




# Heatmap for journals and topics on top 
import plotly.plotly as py
import plotly.graph_objs as go

trace = go.Heatmap(z=create_JournTopDistr(lda100_long),
                   x=aligned_topics,
                   y= my_journals)
data=[trace]
py.iplot(data, filename='labelled-heatmap')



###################################################
# KMEANS AND SILHOUETTE

first_silscores = kmeans_silhouette(40, LinkTop)
first_silscores = kmeans_silhouette(95, TWdistr)
first_silscores = kmeans_silhouette(50, TopWordsDistrLDA)


# 22 and 24 topics seems to perform better

# Check individual clusters:

range_n_clusters = [2, 4,6 , 20, 22]

for n_clusters in range_n_clusters:
    # Create a subplot with 1 row and 2 columns
    fig, ax1 = plt.subplots(1,1)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(TopWordsDistrLDA) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
    clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = clusterer.fit_predict(TopWordsDistrLDA)

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = silhouette_score(TopWordsDistrLDA, cluster_labels)
    print("For n_clusters =", n_clusters,
          "The average silhouette_score is :", silhouette_avg)

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(TopWordsDistrLDA, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,alpha=0.7)
                          #facecolor=color,) # edgecolor=color, )

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

  
    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')

    #plt.draw()
    #plt.pause(0.01)
    #raw_input("Press [enter] to continue.")
    savename = "../Results/New_Full/NEWsilnew%s.eps" %n_clusters
    plt.savefig(savename, eps =1000)

    plt.close()

# Choose 22!!!!
clusterer = KMeans(n_clusters= 6, random_state=10)
cluster_labels = clusterer.fit_predict(TWdistr)



d = dendrogram(LinkTop, link_color_func=f)


import random
colors = []
for i in range(22):
	r = lambda: random.randint(0,255)
	colors.append('#%02X%02X%02X' % (r(),r(),r()))

# Dendrogram
D = dendrogram(Z=LinkTop, color_threshold=None, leaf_font_size=12, leaf_rotation=45, link_color_func=lambda x:coldict[x])

# Combine Ttopic results with data (journals,ids etc)
TopicData = create_full_data(lda100, i_corpus, clean_data, 1, 2)


# Create Journals Topic Distribution, Distance matrices for Journals and Topics, and Two trees for journals and topics
DistData = create_JournTopDistr(TopicData, lda100, jtddict = '../Results/New_Full/distance_matrix_hell.txt',
jmdirect = '../Results/New_Full/distance_Jmatrix_new.txt', tmdirect= '../Results/New_Full/distance_Tmatrix_new.txt',
treeJdirect = '../Results/New_Full/NjHellTreeNew.nw', treeTdirect= '../Results/New_Full/NjTWHTreeNew.nw')


# Run k-means clustering for different clusters and calculate silhouette scores

SilScores = kmeans_silhouette(22, DistData)



