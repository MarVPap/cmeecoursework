#!/usr/bin/env python

##################################
# Clustering and Phylogeny 

# from time import time
# import numpy as np
# import matplotlib.pyplot as plt

# import sklearn
# from sklearn import metrics
# from sklearn.cluster import KMeans
# from sklearn.datasets import load_digits
# from sklearn.decomposition import PCA
# from sklearn.preprocessing import scale
# from sklearn.metrics import silhouette_samples, silhouette_score, pairwise



###################################
# kmeans

msdata = scale(mdata)
n_samples, n_features = mdata.shape
n_digits = len(np.unique(mtarget))
labels = mtarget


#################################
# build tree
import dendropy
import ete3
from ete3 import Tree

pdm = dendropy.PhylogeneticDistanceMatrix.from_csv(
        src=open("../Results/distance_matrix.txt"),
        delimiter=",")
upgma_tree = pdm.upgma_tree()
print(upgma_tree.as_string("newick"))
print(upgma_tree.as_ascii_plot())

nj_tree = pdm.nj_tree()
print(nj_tree.as_string("newick"))
print(nj_tree.as_ascii_plot())

upgma_tree.render('../Results/treetest.png', dpi = 360)

#############################################################
# K means clustering and silhouette

def kmeans_silhouette(MaxNClusters, mydata):

	''' Function that runs kmeans clustering for a range of different
	number of clusters and calculate silhouette score based on euclidean
	distances. 
	MaxNClusters: the maximum number of clusters that we want to run
	MinNClusters: the minimum number of clusters that w want to run
	(the distance between the number of clusters is by 2) 
	mydata: an array with values of the features to be grouped
	sample_size: the number of the features '''

	silouet_scores = []
	nclust= []
	for i in range(4, MaxNClusters + 2,2):
		kmeansfun = KMeans(init='k-means++', n_clusters= i, n_init=10)
		# n_init: Number of time the k-means algorithm will be run with different centroid seeds.
		# The final results will be the best output of n_init consecutive runs in terms of inertia
		kmeansfun.fit(mydata)
		sil_score= metrics.silhouette_score(mydata, kmeansfun.labels_, metric='euclidean')

		silouet_scores.append(sil_score)
		nclust.append(i)
		print 'For %s clusters, the average silhouette score is %s' %(i, sil_score)

	plt.plot(nclust, silouet_scores)
	plt.xlabel("Number of Clusters")
	plt.ylabel("Silhouette Scores")
	plt.legend(("Silhouette score"), loc='best')
	#plt.draw()
	#plt.pause(0.01)
	plt.savefig("../Results/New_Full/kemeanstest.eps") # raw_input("Press [enter] to continue.")
	plt.close()
	return silouet_scores

# Run function
first_silscores = kmeans_silhouette(40, dist_data)




#######################################################################

##########################

#digits = load_digits()
#data = scale(digits.data)

#n_samples, n_features = data.shape
#n_digits = len(np.unique(digits.target))
#labels = digits.target

sample_size = 300

print("n_digits: %d, \t n_samples %d, \t n_features %d"
      % (n_digits, n_samples, n_features))


print(79 * '_')
print('% 9s' % 'init'
      '    time  inertia    homo   compl  v-meas     ARI AMI  silhouette')


def bench_k_means(estimator, name, data):
    t0 = time()
    estimator.fit(data)
    print('% 9s   %.2fs    %i   %.3f   %.3f   %.3f   %.3f   %.3f    %.3f'
          % (name, (time() - t0), estimator.inertia_,
             metrics.homogeneity_score(labels, estimator.labels_),
             metrics.completeness_score(labels, estimator.labels_),
             metrics.v_measure_score(labels, estimator.labels_),
             metrics.adjusted_rand_score(labels, estimator.labels_),
             metrics.adjusted_mutual_info_score(labels,  estimator.labels_),
             metrics.silhouette_score(data, estimator.labels_,
                                      metric='euclidean',
                                      sample_size=sample_size)))

bench_k_means(KMeans(init='k-means++', n_clusters=n_digits, n_init=10),
              name="k-means++", data=mdata)

bench_k_means(KMeans(init='random', n_clusters=n_digits, n_init=10),
              name="random", data=mdata)


# in this case the seeding of the centers is deterministic, hence we run the
# kmeans algorithm only once with n_init=1
pca = PCA(n_components=n_digits).fit(mdata)
bench_k_means(KMeans(init=pca.components_, n_clusters=n_digits, n_init=1),
              name="PCA-based",
              data=mdata)
print(79 * '_')




######################################
# Visualize
# from:

# http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_digits.html#sphx-glr-auto-examples-cluster-plot-kmeans-digits-py

reduced_data = PCA(n_components=2).fit_transform(dist_data)
kmeans = KMeans(init='k-means++', n_clusters=12, n_init=10)
kmeans.fit(reduced_data)

# Step size of the mesh. Decrease to increase the quality of the VQ.
h = .02     # point in the mesh [x_min, x_max]x[y_min, y_max].

# Plot the decision boundary. For that, we will assign a color to each
x_min, x_max = reduced_data[:, 0].min() - 1, reduced_data[:, 0].max() + 1
y_min, y_max = reduced_data[:, 1].min() - 1, reduced_data[:, 1].max() + 1
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))

# Obtain labels for each point in mesh. Use last trained model.
Z = kmeans.predict(np.c_[xx.ravel(), yy.ravel()])

# Put the result into a color plot
Z = Z.reshape(xx.shape)
plt.figure(1)
plt.clf()
plt.imshow(Z, interpolation='nearest',
           extent=(xx.min(), xx.max(), yy.min(), yy.max()),
           cmap=plt.cm.Paired,
           aspect='auto', origin='lower')

plt.plot(reduced_data[:, 0], reduced_data[:, 1], 'k.', markersize=2)
# Plot the centroids as a white X
centroids = kmeans.cluster_centers_
plt.scatter(centroids[:, 0], centroids[:, 1],
            marker='x', s=169, linewidths=3,
            color='w', zorder=10)
plt.title('K-means clustering on the digits dataset (PCA-reduced data)\n'
          'Centroids are marked with white cross')
plt.xlim(x_min, x_max)
plt.ylim(y_min, y_max)
plt.xticks(())
plt.yticks(())
plt.draw()
plt.pause(0.01)
raw_input("Press [enter] to continue.")
plt.close()




#################################################
# Choose silouette n of clusters
# Adapted from:
# http://scikit-learn.org/stable/auto_examples/cluster/plot_kmeans_silhouette_analysis.html#sphx-glr-auto-examples-cluster-plot-kmeans-silhouette-analysis-py

# Generating the sample data from make_blobs
# This particular setting has one distinct cluster and 3 clusters placed close
# together.
# X, y = make_blobs(n_samples=500,
#                   n_features=2,
#                   centers=4,
#                   cluster_std=1,
#                   center_box=(-10.0, 10.0),
#                   shuffle=True,
#                   random_state=1)  # For reproducibility

range_n_clusters = [12, 18, 20, 22, 24]

for n_clusters in range_n_clusters:
    # Create a subplot with 1 row and 2 columns
    fig, ax1 = plt.subplots(1,2)
    fig.set_size_inches(18, 7)

    # The 1st subplot is the silhouette plot
    # The silhouette coefficient can range from -1, 1 but in this example all
    # lie within [-0.1, 1]
    ax1.set_xlim([-0.1, 1])
    # The (n_clusters+1)*10 is for inserting blank space between silhouette
    # plots of individual clusters, to demarcate them clearly.
    ax1.set_ylim([0, len(dist_data) + (n_clusters + 1) * 10])

    # Initialize the clusterer with n_clusters value and a random generator
    # seed of 10 for reproducibility.
    clusterer = KMeans(n_clusters=n_clusters, random_state=10)
    cluster_labels = clusterer.fit_predict(dist_data)

    # The silhouette_score gives the average value for all the samples.
    # This gives a perspective into the density and separation of the formed
    # clusters
    silhouette_avg = silhouette_score(dist_data, cluster_labels)
    print("For n_clusters =", n_clusters,
          "The average silhouette_score is :", silhouette_avg)

    # Compute the silhouette scores for each sample
    sample_silhouette_values = silhouette_samples(dist_data, cluster_labels)

    y_lower = 10
    for i in range(n_clusters):
        # Aggregate the silhouette scores for samples belonging to
        # cluster i, and sort them
        ith_cluster_silhouette_values = \
            sample_silhouette_values[cluster_labels == i]

        ith_cluster_silhouette_values.sort()

        size_cluster_i = ith_cluster_silhouette_values.shape[0]
        y_upper = y_lower + size_cluster_i

        #color = cm.spectral(float(i) / n_clusters)
        ax1.fill_betweenx(np.arange(y_lower, y_upper),
                          0, ith_cluster_silhouette_values,alpha=0.7)
                          #facecolor=color,) # edgecolor=color, )

        # Label the silhouette plots with their cluster numbers at the middle
        ax1.text(-0.05, y_lower + 0.5 * size_cluster_i, str(i))

        # Compute the new y_lower for next plot
        y_lower = y_upper + 10  # 10 for the 0 samples

    ax1.set_title("The silhouette plot for the various clusters.")
    ax1.set_xlabel("The silhouette coefficient values")
    ax1.set_ylabel("Cluster label")

    # The vertical line for average silhouette score of all the values
    ax1.axvline(x=silhouette_avg, color="red", linestyle="--")

    ax1.set_yticks([])  # Clear the yaxis labels / ticks
    ax1.set_xticks([-0.1, 0, 0.2, 0.4, 0.6, 0.8, 1])

    # 2nd Plot showing the actual clusters formed
    colors = cm.spectral(cluster_labels.astype(float) / n_clusters)
    ax2.scatter(X[:, 0], X[:, 1], marker='.', s=30, lw=0, alpha=0.7,
                c=colors)

    # Labeling the clusters
    centers = clusterer.cluster_centers_
    # Draw white circles at cluster centers
    ax2.scatter(centers[:, 0], centers[:, 1],
                marker='o', c="white", alpha=1, s=200)

    for i, c in enumerate(centers):
        ax2.scatter(c[0], c[1], marker='$%d$' % i, alpha=1, s=50)

    ax2.set_title("The visualization of the clustered data.")
    ax2.set_xlabel("Feature space for the 1st feature")
    ax2.set_ylabel("Feature space for the 2nd feature")

    plt.suptitle(("Silhouette analysis for KMeans clustering on sample data "
                  "with n_clusters = %d" % n_clusters),
                 fontsize=14, fontweight='bold')

    #plt.draw()
    #plt.pause(0.01)
    #raw_input("Press [enter] to continue.")
    plt.savefig("../Results/New_Full/dendfancKI.eps")

    plt.close()


