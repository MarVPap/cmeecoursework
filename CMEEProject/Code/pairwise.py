#!/usr/bin/env python

# Cluster/distancesz

# Imports
import itertools
from operator import itemgetter
import collections

from time import time
import sklearn
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.metrics import silhouette_samples, silhouette_score, pairwise

from scipy.cluster import hierarchy


##########################################################
# Prepare data (from new.py)

######################################################
# Combine topic results with data (journals,ids etc)

# Add topics to document information
combined = itertools.izip(i_corpus, ((d[1], d[2]) for d in clean_data))
topic_data = []  #for each article, collect topic with highest score
#topic_stats = []  # gets all topic-score pairs for each document

print 'Pairing topics with information..' 

for corp_txt,(scid, journal) in combined:
    _srtd = sorted(lda100[corp_txt], key=itemgetter(1), reverse=1)
    top = [topic for topic, scores in _srtd]
    score = [scores for topic, scores in _srtd]
    topic_data.append((scid, journal, top, score))
    #topic_stats.append(_srtd)


##########################################################################
# Create list with journals (target) and all topics prob (filled_data)
# keep only journal and topic information from data (topic_data)
# Also create dictionary with journals and top 3 topics 

print 'Creating lists/array with journals and all topics probabilities and \n dictionary with journals and top3 topics...'

# initializing
c = 0
filled_data = [] # store topics probabilities of each document 
target_data =[] # store journal of each document

journals_topics = defaultdict(int) # store journals/topics topics number dictionary

c = 0
filled_data = []
target_data =[]
for doc in topic_data:
	new_doc = []
	for i in range(0, 100):
		if i in doc[2]:
			new_doc.append(doc[3][doc[2].index(i)])
		else:
			new_doc.append(0)
	filled_data.append(new_doc)
	target_data.append(doc[1])
	c= c+1

	if c%5000 == 0:
		print c, 'abstracts done..'


# Turn filled_data into array
mdata = np.array(filled_data)
mtarget = np.array(target_data)




###############################################################################
# Creat Distance Matrix based on combined probilities of topics in journal

# Get combined propabilities

def list_average(nums):
	''' Calculates the mean of a list's elements'''
	return sum(nums)/ float(len(nums))

print 'Creating dictionary with journals and topics...'

# First: create dictionary to calculate and store combined probabilities
JTdistr = defaultdict(int) # to fill

for i in range(0, len(target_data)):
	name = target_data[i]
	topdistr = filled_data[i]

	if name in JTdistr.keys():
		JTdistr[name] = [list_average(n) for n in zip(*[JTdistr[name],topdistr]) ]
	else:
		JTdistr[name] = topdistr

## Second: From dictionary to lists: matrix of journals-topics distributions & journals names

MatrixJTdistr = []
TargJTdistr = []
for key, param in JTdistr.iteritems():
	TargJTdistr.append(key)
	MatrixJTdistr.append(param)

## turn list to array
dist_data = np.array(MatrixJTdistr)

# save
TargJTdistr.insert(0, 'Journals')
MatrixJTdistr.insert(0, Topics)

saveJTD =  np.concatenate((np.array(TargJTdistr)[:, np.newaxis], MatrixJTdistr), axis = 1)


export_csv(saveJTD, '../Results/JTDistr.csv', 'nlist')

##################################################################
# Make different distance matrices to test

print 'Creating distance matrices...'

## 1. Pairwise Euklidean distance
dm_eukl = pairwise.pairwise_distances(dist_data, metric = 'euclidean')
## 2. Manhattan distance
dm_manh = pairwise.pairwise_distances(dist_data, metric = 'manhattan')
dmTopmanh = pairwise.pairwise_distances(X, metric = 'manhattan')
## 3. Hellinger distance 
# Also network
from scipy.spatial.distance import pdist, squareform

def hellinger(X):
    return squareform(pdist(np.sqrt(X)))/np.sqrt(2)

# for topics
X = lda100.state.get_lambda()
X = X / X.sum(axis=1)[:, np.newaxis] # normalize vector
dmTophell = hellinger(X)

# for journals
dm_hell = hellinger(dist_data)


# build network
import networkx
import itertools as itt

edges = [(i, j, {'weight': 1 - dm_hell[i,j]})
         for i, j in itt.combinations(range(67), 2)]
k = np.percentile(np.array([e[2]['weight'] for e in edges]), 80)

G = networkx.Graph()
G.add_nodes_from(range(67))
G.add_edges_from([e for e in edges if e[2]['weight'] > k])

nx.draw(G, node_size=15, node_color='red', font_size=8, font_weight='bold')

#plt.tight_layout()
plt.savefig("Graph.png", format="PNG")

plt.show()

# Create linkage matrix 
print 'Creating linkages matrices...'

# single method: Nearest Point Algorithm
lm_eukl = hierarchy.linkage(dm_eukl, method= 'single')
lm_manh = hierarchy.linkage(dm_manh, method= 'single')
lm_hell = hierarchy.linkage(dm_hell, method= 'single')
tw_hell = hierarchy.linkage(dmTophell, method= 'single')
tw_manh = hierarchy.linkage(dmTopmanh, method= 'single')

# Create flat matrix 
print 'Creating flat matrices...'
fcl_eukl = hierarchy.fcluster(lm_eukl, dm_eukl.max()*0.5, 'distance' )
fcl_manh = hierarchy.fcluster(lm_manh, dm_manh.max()*0.5, 'distance')
fcl_hell = hierarchy.fcluster(lm_hell, dm_hell.max()*0.5, 'distance')
ftw_hell = hierarchy.fcluster(tw_hell, dmTophell.max()*0.5, 'distance')


# Check matrices 
print 'Testing distance matrices...'

metrics.silhouette_score(fcl_eukl, kmeansfun.fil(fcl_eukl).labels_, metric='precomputed')

#plottin Dendogram
from scipy.cluster.hierarchy import dendrogram, linkage

def plot_dendogram(X):
	plt.figure(figsize=(25, 10))
	plt.title('Hierarchical Clustering Dendrogram')
	plt.xlabel('sample index')
	plt.ylabel('distance')
	dendrogram(
		X, leaf_rotation=90.,  # rotates the x axis labels
    leaf_font_size=8.,  # font size for the x axis labels 
    )
	# plt.draw()
	plt.pause(0.01)
	raw_input("Press [enter] to continue.")
	plt.close()

plot_dendogram(dm_eukl)



############################################
# Work as phulogeny

TargJTdistr = [re.sub(',','', x) for x in TargJTdistr]
Topics = ['Topic_'+ str(i) for i in range(1,101)]
matrixtosaveMan =  np.concatenate((np.array(TargJTdistr)[:, np.newaxis], dm_manh), axis = 1)
matrixtosaveEukl =  np.concatenate((np.array(TargJTdistr)[:, np.newaxis], dm_eukl), axis = 1)
matrixtosaveHell =  np.concatenate((np.array(TargJTdistr)[:, np.newaxis], dm_hell), axis = 1)
matrixtosaveTWHel =  np.concatenate((np.array(Topics)[:, np.newaxis], dmTophell ), axis = 1)
matrixtosaveTWMan =  np.concatenate((np.array(Topics)[:, np.newaxis], dmTopmanh ), axis = 1)


myheader = ','.join([''] + TargJTdistr)
myheaderT =  ','.join([''] + Topics)

#myheader = [''] + TargJTdistr

#save
with open('../Results/distance_matrix_manh.txt', 'wb') as f:
	np.savetxt(f, matrixtosaveMan, fmt = '%5s',delimiter=  ',', header = myheader)

with open('../Results/distance_matrix_eukl.txt', 'wb') as f:
	np.savetxt(f, matrixtosaveEukl, fmt = '%5s',delimiter=  ',', header= myheader)

with open('../Results/distance_matrix_hell.txt', 'wb') as f:
	np.savetxt(f, matrixtosaveHell, fmt = '%5s', delimiter=  ',', header= myheader)

with open('../Results/distance_TW_matrix_hell.txt', 'wb') as f:
	np.savetxt(f, matrixtosaveTWHel, fmt = '%5s', delimiter=  ',', header= myheaderT)

with open('../Results/distance_TW_matrix_manh.txt', 'wb') as f:
	np.savetxt(f, matrixtosaveTWMan, fmt = '%5s', delimiter=  ',', header= myheaderT)


# Build tree
# build tree
import dendropy
import ete3
from ete3 import Tree

pdmM = dendropy.PhylogeneticDistanceMatrix.from_csv(
        src=open("../Results/distance_matrix_manh.txt"),
        delimiter=",")
pdmE = dendropy.PhylogeneticDistanceMatrix.from_csv(
        src=open("../Results/distance_matrix_eukl.txt"),
        delimiter=",")

pdmH = dendropy.PhylogeneticDistanceMatrix.from_csv(
        src=open("../Results/distance_matrix_hell.txt"),
        delimiter=",")

pdmHT = dendropy.PhylogeneticDistanceMatrix.from_csv(
        src=open("../Results/distance_TW_matrix_hell.txt"),
        delimiter=",")


## 2. NEIGHBOUR-JOINING APPROACH
nj_treeM = pdmM.nj_tree()
nj_treeE = pdmE.nj_tree()
nj_treeH = pdmH.nj_tree()
nj_treeHT = pdmHT.nj_tree()



twnj_tree = twpdm.nj_tree() # for topic words distances

## Save

t21 = nj_treeM._as_newick_string() + ';'
t22 = nj_treeE._as_newick_string() + ';'
t23 = nj_treeH._as_newick_string() + ';'
t231 = nj_treeHT._as_newick_string() + ';'

t3 = twnj_tree._as_newick_string() + ';'

NjTreeM = ete3.Tree(t21)
NjTreeE = ete3.Tree(t22)
NjTreeH = ete3.Tree(t23)
NjTreeHT = ete3.Tree(t231)

NjTreetw = ete3.Tree(t3)


NjTreeM.write(format=1, outfile= 'NjManhTree.nw')
NjTreeE.write(format=1, outfile= 'NjEuklTree.nw')
NjTreeH.write(format=1, outfile= 'NjHellTree.nw')
NjTreeHT.write(format=1, outfile= 'NjTWHTree.nw')


NjTreetw.write(format=1, outfile= '../Results/tw_nj_tree.nw')

NjTreeM.render('../Results/njtreeM.png', w = 1000, dpi = 720)
NjTreeE.render('../Results/njtreeE.png', w = 1000, dpi = 720)


######################
#  style

from ete3 import TreeStyle, NodeStyle
ts = TreeStyle()

D_leaf_color = {1:"DarkSeaGreen", 2:"Moccasin", 3: "LightSteelBlue", 4: "Khaki", 
5:"DarkSeaGreen", 6: "Red", 7:"DarkSeaGreen", 8:"Moccasin", 9: "LightSteelBlue", 10: "Khaki", 
11:"DarkSeaGreen" }
c = 0
for node in NjTree.traverse():
    if len(node)>= 20:
    	c = c+1
    	node.img_style["bgcolor"] = D_leaf_color[c]
    	#color = D_leaf_color[c]
    	#name_face =Face(node.name, fgcolor=color, fsize=10)
        #node.add_face(name_face)

        # col = 0
        # for i, name in enumerate(set(node.get_leaf_names())):
        #     if i>0 and i%2 == 0:
        #         col += 1

ts = TreeStyle()

