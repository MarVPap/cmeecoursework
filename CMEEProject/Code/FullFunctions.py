#!/usr/bin/env python

import itertools
from operator import itemgetter
import collections

from time import time
from sklearn import metrics
from sklearn.cluster import KMeans
from sklearn.datasets import load_digits
from sklearn.decomposition import PCA
from sklearn.preprocessing import scale
from sklearn.metrics import silhouette_samples, silhouette_score, pairwise
#from __future__ import print_function

from scipy.spatial.distance import pdist, squareform
# Build tree
import dendropy
import ete3
from ete3 import Tree
####################################################
# A) DATA WRANGLING 

####################################################
# Make documents distribution over words 

def create_DocWordDistr(corpus, dictionary):
	''' Creates a count distribution over words for each document in a corpus.
	Adding zeros to the words in the dictionary that dont appear in a document
	and the frequency of the ones that exist. Returns a list of lists Documents X Words.
	Every list is a document and every element in the list is a word count.
	'''
	c = 0
	words_data = []

	print 'Creating documents distribution over words...'
	for doc in corpus:
		nwords= [w for w,f in doc]
		nfreq = [f for w,f in doc]
		new_doc =[]
		for i in range(0, len(dictionary)):
			if i in nwords:
				new_doc.append(nfreq[nwords.index(i)])
			else:
				new_doc.append(0.)
		words_data.append(new_doc)
		c= c+1

		if c%5000 == 0:
			print c, 'abstracts placed in words space..'

	print 'Done'
	return words_data



#####################################################
# Make topics distribution over words

def create_TopWordsDistr(ldamodel, i_dictionary, time = False , export_dir = ''):
	''' Extract the probability distribution of topics over words from the model
	and saves it in a dictionary
	'''
	# Creates table with topic words
	print 'Extracting topics distributions over words from model.. '
	import my_functions


	# TopicsYWords is  a list of lists of lists of tuples
	# 1st level corresponds to topics (0,100)
	# 2nd are all words with their probablilies of each year-topic

	if time is True:
		print 'Dynamic model started...'

		TopicsYWords = []
		for i in range(ldamodel.num_topics):
			newtopic = []
			for k in range(0,16):
				newtopic.append(ldamodel.show_topic(topicid =i, time =k, num_words = len(i_dictionary)))

			TopicsYWords.append(newtopic)

		FullTopWordsDistr = [] 
		for top in TopicsYWords:
			newtop = []
			for year in top:
				ytwords = [w for p,w in year]
				ytprob = [p for p,w in year]
				newfulline = []

				for k in range(0,len(i_dictionary)):
					nword = i_dictionary[k]
					if nword in ytwords:
						newfulline.append(ytprob[ytwords.index(nword)])
					else:
						newfulline.append(0)
						print 'Word not from topic found!!'
				newtop.append(newfulline)
			# print 'Topic Done!'

			FullTopWordsDistr.append(newtop)

		#export_csv(FullTopWordsDistr, export_dir, 'nlist')

	else:
		print 'Classic LDA started...'
		FullTopWordsDistr = []

		for i in range(ldamodel.num_topics):
			newtop = ldamodel.show_topic(i, len(i_dictionary))
			twords = [w for w, v in newtop]
			wprob = [v for w,v in newtop]
			newfulline = []

			for k in range(0,len(i_dictionary)):
				nword = i_dictionary[k]
				if nword in twords:
					newfulline.append(wprob[twords.index(nword)])
				else:
					newfulline.append(0)
					print 'Word not from topic found!!'
			
			FullTopWordsDistr.append(newfulline)


	return FullTopWordsDistr


# Get words per topic table (for word clouds)

def create_topics_words_table(ldamodel, n_words, time = False):

	TopicsYWords = []

	if time is True:
		for i in range(ldamodel.num_topics):
			newtopic = []

			for k in range(0,16):
				newtopic.append(ldamodel.show_topic(topicid =i, time =k, num_words =  n_words))

			TopicsYWords.append(newtopic)

	else: 
		for i in range(ldamodel.num_topics):
			TopicsYWords.append(ldamodel.show_topic(i, n_words))

	return TopicsYWords


######################################################
# Combine topic results with data (journals,ids etc)

def create_full_data(ldamodel, corpus, dataset, col1, col2):
	'''Combines model results with general data of the documents
	eg. year published, author. Produces a lists of tuples with:
	info from dataset and topics probabilities '''

	# Add topics to document information
	combined = itertools.izip(corpus, ((d[col1], d[col2]) for d in dataset))
	topic_data = []  #for each article, collect topic with highest score
	#topic_stats = []  # gets all topic-score pairs for each document

	print 'Pairing topics with information..' 
	print '(This may take a while)'

	for corp_txt,(scid, journal) in combined:
		srtd = sorted(ldamodel[corp_txt], key=itemgetter(1), reverse=1)
		top = [topic for topic, scores in srtd]
		score = [scores for topic, scores in srtd]
		topic_data.append((scid, journal, top, score))
		#topic_stats.append(_srtd)

	print 'Done'
	return topic_data



##########################################################################
# Create list with journals (target) and all topics prob (filled_data)
# keep only journal and topic information from data (topic_data)


def create_JournTopDistr(topic_data, ldamodel, jtddict = '../Results/distance_matrix_hell.txt',
 jmdirect = '../Results/distance_Jmatrix_new.txt', tmdirect= '../Results/distance_Tmatrix_new.txt', treeJdirect = '../Results/NjHellTreeNew.nw',
 treeTdirect= '../Results/NjTWHTreeNew.nw'):

	print 'Creating lists/array with journals and all topics probabilities...'
	import collections
	import numpy

	# initializing
	c = 0
	filled_data = [] # store topics probabilities of each document 
	target_data =[] # store journal of each document

	#journals_topics = collections.defaultdict(int) # store journals/top3 topics number dictionary

	for doc in topic_data:
		new_doc = []
		for i in range(0, 100):
			if i in doc[2]:
				new_doc.append(doc[3][doc[2].index(i)])
			else:
				new_doc.append(0)
		filled_data.append(new_doc)
		target_data.append(doc[1])
		c= c+1

		if c%5000 == 0:
			print c, 'abstracts done..'


	# Turn filled_data into array
	#mdata = numpy.array(filled_data)
	#mtarget = numpy.array(target_data)


	print 'All Documents done!'


	###############################################################################
	# Creat Distance Matrix based on combined probilities of topics in journal

	# Get combined propabilities
	print 'Combining probabilities and creating journals topics distribution...'

	def list_average(nums):
		''' Calculates the mean of a list's elements'''
		return sum(nums)/ float(len(nums))

	# First: create dictionary to calculate and store combined probabilities
	JTdistr = collections.defaultdict(int) # to fill

	for i in range(0, len(target_data)):
		name = target_data[i]
		topdistr = filled_data[i]

		if name in JTdistr.keys(): ####!!! wrong  !!!
			JTdistr[name] = [list_average(n) for n in zip(*[JTdistr[name],topdistr]) ]
		else:
			JTdistr[name] = topdistr

	## Second: From dictionary to lists: matrix of journals-topics distributions & journals names

	MatrixJTdistr = []
	TargJTdistr = []
	for key, param in JTdistr.iteritems():
		TargJTdistr.append(key)
		MatrixJTdistr.append(param)

	## turn list to array
	dist_data = numpy.array(MatrixJTdistr)

	# save
	Topics = ['Topic_'+ str(i) for i in range(1,101)]

	TargJTdistr.insert(0, 'Journals')
	MatrixJTdistr.insert(0, Topics)

	saveJTD =  numpy.concatenate((numpy.array(TargJTdistr)[:, numpy.newaxis], MatrixJTdistr), axis = 1)


	export_csv(saveJTD, jtddict , 'nlist')

	#return dist_data


	#jtdistr = create_JournTopDistr(Topic_Data)


	#################################################################
	# CREATE DISTANCE MATRICES


	#def create_DistanceMatrix(j_dist_data, t_dist_data, disttype, ldamodel):
	'''Creates distance matrices of journals or topics. 
	Type of distances choose one from euclidean, manhattan, hellinger'''

	print 'Creating distance matrices...'
	## Pairwise distance
	#dm_J = pairwise.pairwise_distances(dist_data, metric = disttype)
	#dm_T = pairwise.pairwise_distances(t_dist_data, metric = disttype)
	##  Hellinger distance 
	# Also network

	def hellinger(X):
	    return squareform(pdist(numpy.sqrt(X)))/numpy.sqrt(2)

	# for topics
	X = ldamodel.state.get_lambda()
	X = X / X.sum(axis=1)[:, numpy.newaxis] # normalize vector
	dm_T = hellinger(X)
	dm_J = hellinger(dist_data)


	# Create linkage matrix 
	print 'Creating linkages matrices...'

	# single method: Nearest Point Algorithm
	lm_J = hierarchy.linkage(dm_J, method= 'single')
	lm_T = hierarchy.linkage(dm_T, method= 'single')

	# Create flat matrix 
	print 'Creating flat matrices...'
	fcl_hell = hierarchy.fcluster(lm_J, dm_J.max()*0.5, 'distance')
	ftw_hell = hierarchy.fcluster(lm_T, dm_T.max()*0.5, 'distance')


	############################################
	# Work as phulogeny

	TargJTdistr = [re.sub(',','', x) for x in TargJTdistr]
	matrixtosaveJ =  numpy.concatenate((numpy.array(TargJTdistr)[:, numpy.newaxis], dm_Jeukl), axis = 1)
	matrixtosaveT =  numpy.concatenate((numpy.array(Topics)[:, numpy.newaxis], dm_T), axis = 1)


	myheader = ','.join([''] + TargJTdistr)
	myheaderT =  ','.join([''] + Topics)

	#save
	with open(jmdirect, 'wb') as f:
		numpy.savetxt(f, matrixtosaveJ, fmt = '%5s',delimiter=  ',', header = myheader)

	with open(tmdirect, 'wb') as f:
		numpy.savetxt(f, matrixtosaveT, fmt = '%5s', delimiter=  ',', header= myheaderT)



	#################################################################
	# BUID PHYLOGENY

	pdmH = dendropy.PhylogeneticDistanceMatrix.from_csv(
	        src=open(jmdirect),
	        delimiter=",")

	pdmHT = dendropy.PhylogeneticDistanceMatrix.from_csv(
	        src=open(tmdirect),
	        delimiter=",")


	## 2. NEIGHBOUR-JOINING APPROACH

	nj_treeH = pdmH.nj_tree()
	nj_treeHT = pdmHT.nj_tree()

	## Save

	t23 = nj_treeH._as_newick_string() + ';'
	t231 = nj_treeHT._as_newick_string() + ';'


	NjTreeH = ete3.Tree(t23)
	NjTreeHT = ete3.Tree(t231)


	NjTreeH.write(format=1, outfile= treeJdirect)
	NjTreeHT.write(format=1, outfile= treeTdirect)


	return dist_data


##################################################################
# K-MEANS CLUSTERING AND SILHOUETTE TEST

# my function 
def kmeans_silhouette(MinNClusters, MaxNClusters, mydata):

	''' Function that runs kmeans clustering for a range of different
	number of clusters and calculate silhouette score based on euclidean
	distances. 
	MaxNClusters: the maximum number of clusters that we want to run
	MinNClusters: the minimum number of clusters that w want to run
	(the distance between the number of clusters is by 1) 
	mydata: an array with values of the features to be grouped
	sample_size: the number of the features '''

	print 'Running k-means and sillouette analysis...'
	silouet_scores = []
	nclust= []
	for i in range(MinNClusters, MaxNClusters+1,1):
		kmeansfun = KMeans(init='k-means++', n_clusters= i, n_init=50)
		# n_init: Number of time the k-means algorithm will be run with different centroid seeds.
		# The final results will be the best output of n_init consecutive runs in terms of inertia
		kmeansfun.fit(mydata)
		sil_score= metrics.silhouette_score(mydata, kmeansfun.labels_, metric='precomputed')

		silouet_scores.append(sil_score)
		nclust.append(i)
		print 'For %s clusters, the average silhouette score is %s' %(i, sil_score)

	plt.plot(nclust, silouet_scores)
	plt.xlabel("Number of Clusters")
	plt.ylabel("Silhouette Scores")
	plt.legend(("Silhouette score"), loc='best')
	plt.draw()
	plt.pause(0.01)
	raw_inumpyut("Press [enter] to continue.")
	plt.close()

	return kmeansfun


# Silouette score for Clustering

#sil_score= metrics.silhouette_score(mydata, kmeansfun.labels_, metric='euclidean')

######################################################################
# CLUSTERING


def cluster_topics(X, d_threshold, figtype, heatmethod= "weighted", clustmethod ='weighted'):
	# Create distance matrix
	print 'Creating distance matrix...'
	import numpy
	def hellinger(X, conc):
		#https://blakeboswell.github.io/2016/gensim-hellinger/
		if conc == True:
			return numpy.triu(squareform(pdist(numpy.sqrt(X)))/numpy.sqrt(2))
		else:
			return squareform(pdist(numpy.sqrt(X)))/numpy.sqrt(2)

	TopicsDistMatrix = hellinger(X, False)
	numpy.savetxt("../Results/ScopusAnalysis/TopDistMatrix.csv", TopicsDistMatrix, delimiter=",")

	print 'Saving matrix with labels..'
	Topics = ['T_'+ str(i) for i in range(100)]
	matrixtosaveT = numpy.concatenate((numpy.array(Topics)[:, numpy.newaxis], TopicsDistMatrix), axis = 1)
	myheaderT =  ','.join([''] + Topics)

	with open("../Results/ScopusAnalysis/TopDistMatrixH.csv", 'wb') as f:
			numpy.savetxt(f, matrixtosaveT, fmt = '%5s',delimiter=  ',', header = myheaderT)


	# # TOPIC CLUSTERING 
	# # make heatmap of topics vs words
	# # seaborn.heatmap(TopWordsDistrLDA, xticklabels= Topics, yticklabels=False,  cbar=False)
	# # plt.savefig("../Results/New_Full/Words_Topics_Heatmap.eps",  format='eps', dpi=2000)

	# # make heatmap of topics vs topics
	# seaborn.clustermap(TopicsDistMatrix, method= clustmethod)
	# tosavename = "../Results/New_Full/TopicsHeatmap_%s.%s" %(clustmethod,figtype)
	# plt.savefig(tosavename) #  format='png', dpi=2000)
	# plt.close('all')

	# HIERARCHICAL CLUSTEING

	from scipy.cluster.hierarchy import cophenet
	from scipy.cluster.hierarchy import dendrogram, linkage
	import scipy

	# Create flat matrix 
	LinkTop = linkage(TopicsDistMatrix, method= clustmethod)
	metric, coph_dists = cophenet(LinkTop, pdist(TopicsDistMatrix))
	print 'Cophenet metric of linkage clustering is: %s' %metric
	#plottin Dendogram

	# Choose clusters
	# 1. elbow
	plt.close('all')
	last = LinkTop[-50:, 2]
	last_rev = last[::-1]
	idxs = np.arange(1, len(last) + 1)
	plt.plot(idxs, last_rev)

	acceleration = np.diff(last, 2)  # 2nd derivative of the distances
	acceleration_rev = acceleration[::-1]
	plt.plot(idxs[:-2] + 1, acceleration_rev)
	elsavename = "../Results/New_Full/elbow_%s_%s" %(clustmethod, figtype)
	plt.savefig(elsavename)
	plt.close('all')
	k = acceleration_rev.argmax() + 2  # if idx 0 is the max of this we want 2 clusters
	print "clusters:", k # 30 clusters


	# 2. Inconsistency method
	#from scipy.cluster.hierarchy import fcluster

	#clusters = fcluster(LinkTop, k , criterion = 'maxclust')


	def fancy_dendrogram(*args, **kwargs):
		max_d = kwargs.pop('max_d', None)
		
		if max_d and 'color_threshold' not in kwargs:
			kwargs['color_threshold'] = max_d
		annotate_above = kwargs.pop('annotate_above', 0)

		ddata = dendrogram(*args, **kwargs)

		if not kwargs.get('no_plot', False):
			plt.title('Hierarchical Clustering Dendrogram: %s & c = %s' %(clustmethod, metric))
			plt.xlabel('Topics')
			plt.ylabel('distance')
			for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
				x = 0.5 * sum(i[1:3])
				y = d[1]
				if y > annotate_above:
					plt.plot(x, y, 'o', c=c)
					#plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
					#			textcoords='offset points',
					#			va='top', ha='center')
					plt.grid(False)
			if max_d:
				#plt.axhline(y=max_d, c='k')
				1+1
		return ddata

	# PLOT DENDOGRAM
	plt.figure(figsize=(10,10))
	clusters = fancy_dendrogram(
		LinkTop,
		#truncate_mode='lastp',
		#p=30,
		leaf_rotation=90.,
		leaf_font_size=6.,

		#show_contracted=True,
		#annotate_above=40,
		max_d= d_threshold,
	)

	d_save= "../Results/New_Full/fdendrogram_%s_%s" %(clustmethod,figtype)
	plt.savefig(d_save)
	plt.close('all')

	return clusters


##################################################################
# VISUALIZE

# Word clouds

def make_wordclouds(TopicsYWords, wadd = '', time = False):
	from PIL import Image
	from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator
	import numpy as np

	my_color= np.array(Image.open('circl.png'))

	wc = WordCloud(
		background_color="white",
		max_words=25,
		width = 1024,
		height = 720,
		prefer_horizontal = 0.99999 ,
		mask= my_color,
		#stopwords=stopwords.words("english"),
		margin = 10
	)

	image_colors = ImageColorGenerator(my_color)

	# Save word cloud for each year/topic group

	c=0

	for topic in TopicsYWords:
		n_path= '../Results/ScopusAnalysis/WCReport/%s_%s.png' %(c,wadd)
		new = defaultdict(int)

		for i in topic:
			new[i[0]] = i[1]

		wc.generate_from_frequencies(new)
		plt.imshow(wc.recolor(color_func=image_colors), interpolation="bilinear")
		wc.to_file(n_path)
		print 'Topic %s done!' %c
		c= c+1


