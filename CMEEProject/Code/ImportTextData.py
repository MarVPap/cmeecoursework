#!/usr/bin/env python

__author__  = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


# Import packages
print '\n'
print 'Importing packages..'

from pprint import pprint  # pretty-printer
from collections import defaultdict  # remove words that appear only once
from gensim import corpora, models, similarities
from nltk import word_tokenize
from nltk.corpus import stopwords
from sklearn import feature_extraction
from nltk.stem.snowball import SnowballStemmer # for stemming

#from lxml import etree # to deal with xml file format

import re 			# for use of regular expressions
import os 
import sys 
import csv 			# to import and export csvs
import nltk 		# natural language toolkit
import numpy as np  # for gensim package
import codecs
import string
import pandas as pd # for dataset manipulation
import unicodedata  # to deal with xml file format
#import urllib2
  


# INSERT ABSTRACTS

def import_data_nested(my_xml_directory, part_start, part_end):

	def read_xmls(xml_directory): # '../Data/Downloads/'
		''' A function that uses a directory and goes through every
		xml file in this directory. It uses regular expressions to keep
		the following information: name and surname of first author, name and surname
		of all authors, papers ID, number of times the paper has been cited till
		the day of download, the date it was publiced, the first author's ID, and 
		the number of references. It is then uses the gender_guesser package
		to guess the gender of all authors of each paper. Finally it exports
		all these information in a list of list (each sublist contains every papers
		information).'''
		
		# create objects
		MyCSV = [] 		# general list to store all the data
		x = 0 			# to count the papers info added
		y = 0 			# to count the errors occured
		
		for xmlFile in os.listdir(xml_directory): 	# for every xml name in the input directory
			xmlFile  = xml_directory +'/'+ xmlFile 	# create path to the file
			f        = open(xmlFile, 'rt') 					# open the file to read
			textFile = f.read()
			f.close()
			
			## Change files format to make it ready for regex	
			textFile = textFile.replace('\n', ' ').replace('\t', ' ') 	# replace all newlines and tabs with spaces
			textFile = textFile.decode('ascii', 'ignore')				# change format
			textFile = str((textFile)) 									# convert everything to string
			textFile = re.sub(' +',' ', textFile) 						# replace all + with spaces
			
			#######################################################
			## Use pf regular expressions (specifically look
			## behind and look infront commands) to extract elements
			
			ScopusID = re.search(r'(?<=SCOPUS_ID:)\d+', textFile)				 # for the scopus ID
			Abstract = re.search(r'(?<=<ce:para> ).*?(?=</ce:para> )', textFile) # Journal name
			Journal  = re.search(r'(?<=<prism:publicationname> ).*?(?=</prism:publicationname> )', textFile)
			Date     = re.search(r'(?<=<prism:coverdate> )\d{4}-\d{2}-\d{2}', textFile) # the date published (format: year-month-day)
			CitedBy = re.search(r'(?<=<citedby-count> )\d+', textFile) # the number of times cited till download date

		
			########################################
			# EXTRACT INFORMATION
			
			try:			
				## Combine everything found into a tuple:
				Info = (ScopusID.group(),Journal.group(), Date.group(), CitedBy.group(), Abstract.group())
				# My_abstracts = Abstract.group()
				MyCSV.append(Info) 	# Add each papers tuple with information to the general results list (of tuples)
				# MyCSV.update({Info:My_abstracts})
				x = x+1 					# count paper added
				
			except AttributeError:  		# In case one of the variables is missing in a specific xml file (e.g. some papers without references or number of them)
				y=y+1 						# count error
				print "One abstract was missing- Attribute Error skipped for the file:"
				print xmlFile 				# name of the file for check 
				
				continue
			
		print "There were added" , x, "elements in my results list for this part."
		print "There were", y, "errors or files with missing information."
		
		return MyCSV


	## Call the function for all the folders with our files (all parts)
	# compile documents into a list

	print 'Reading XML files....'

	all_abstracts = []
	for i in range(part_start,part_end): 					# len(partsTOP)) for general apply
		print "Part_%s started" %i
		MyXMLFilesDir = my_xml_directory + "/Part_%s" %i
		part_csv = read_xmls(MyXMLFilesDir) # Take info from all xml files in each folder
		all_abstracts.extend(part_csv) 			# Adds all elements of my part list in my general list

		print "Part_%s finished" %i
	print '\n'
	print "Hoorey! Your Abstract List is ready!"

	return all_abstracts



def prepare_data(all_abstracts, save_dict_directory, save_corp_directory):

	print 'Creating abstracts list of words without special characters and numbers...'
	all_abstr = [re.sub('[!#$%&()*+,-1234567890./:;\'<=>?@^"_{|}~]','', x.lower()) for x in all_abstracts]
	
	# Tokenize
	all_abstr = [x.split() for x in all_abstr]

	# remove common words and tokenize
	print 'Excluding common/stop words...'
	stop = stopwords.words('english')
	#stop.extend([u'ecology',u'understand',u'understading',u'premise'])

	to_remove=[]
	for abstr in all_abstr:
		to_remove=[]
		for word in abstr:
			if word in stop:
				to_remove.append(word)

		for i in to_remove:
			abstr.remove(i)


	## Stemming 
	print 'Stemming corpus...'
	stemmer = SnowballStemmer('english')
	stem_abs_corpus = [[stemmer.stem(word) for word in abstr] for abstr in all_abstr]

	## Export vocabulary for stemmed words..
	# stemmed_words =  [[(word, stemmer.stem(word)) for word in abstr] for abstr in all_abstr]
	# export_csv(stemmed_words, '../Data/stemmed_words.csv', 'list')

	## Remove words that appear only once
	print 'Removing words that appear only once or everywhere(0.7)...'
	frequency = defaultdict(int)
	corpfreq = defaultdict(int)

	for text in stem_abs_corpus:
	    new_tok = []
	    for token in text:
	    	new_tok.append(token)
	        frequency[token] += 1

	    newset = list(set(new_tok))
	    for tok in newset:
	    	corpfreq[tok] += 1

	

	stem_abs_corpus = [[token for token in abstr if corpfreq[token] > len(stem_abs_corpus)*0.005 and corpfreq[token]<len(stem_abs_corpus)*0.7] for abstr in stem_abs_corpus]
	
	export_csv(frequency, '../Data/new_frequency_table.csv', 'dictionary')
	export_csv(corpfreq, '../Data/corpus_word_frequency.csv', 'dictionary')


	print 'Saving stemmed as dictionary..'

	my_dictionary = corpora.Dictionary(stem_abs_corpus)
	my_dictionary.save(save_dict_directory)
	
	print 'Dictionary saved info:', my_dictionary

	print 'Tokenizing: converting strings to vectors...'
	## Matches every word in the dictionary to a number and then creates
	## a list of tuples for each document with the first element the word 
	## number and second the times it appears in the document

	## convert dictionary to a bag of words corpus for reference 
	stem_corpus = [my_dictionary.doc2bow(text) for text in stem_abs_corpus]
	# abs_corpus = [my_dictionary.doc2bow(text) for text in abs_corpus]

	# print 'Calculating words frequency (as wordsfreq)..'
	# wordsfreq = my_dictionary.token2id
	# print(my_dictionary.token2id)

	print 'Saving as bag of words corpus..'
	corpora.MmCorpus.serialize(save_corp_directory, stem_corpus)


	print 'Preparation of data done!!'
	print '2 files have been created in memory:'
	print '1.'+ save_dict_directory + ': dictionary of all words in the main corpus'
	print '2.'+ save_corp_directory + ': tokenized vectors for words in main corpus'

	return abs_corpus

#######################################################
# Create document-term matrix
# doc_term_matrix = 

#######################################################
# Create Dataset 
all_data = import_data('../Data/XML', 0 , 5)

print 'Saving Abstracts in file..'
# Add header
CSVHead = ('ScopusID','Journal','Date', 'CitedBy', 'Abstract')
all_data.insert(0,CSVHead) # Insert header as first element of main list      

print 'Exporting...'
with open('../Data/all_data.csv', 'w') as tosave:
	writer = csv.writer(tosave, dialect ='excel')
	writer.writerows(all_data)
#	for i in test_corpus:
#		tosave.write('%s\n' % i)

#######################################################
# Import Clean dataframe (same in TopicAnal.py)

with open('../Data/all_data_top_300.csv', 'r') as newcsv:
	reader = csv.reader(newcsv)
	clean_data = [tuple(line) for line in reader]

# Remove header
clean_data.pop(0)

# Keep only abstracts 
print 'Keeping only abstracts..'
my_abstracts = [paper[5] for paper in clean_data]


#####################################################
# Create test corpus
print '\n'
print '**************************'
print 'Creating corpus..'
print '\n'
the_abstracts = import_data('../Data/XML', 0 , 5)
print '\n'
nostem_corpus = prepare_data(the_abstracts, '../Data/Dictionaries/my_corpus.dict', '../Data/Dictionaries/my_corpus.mm' )
print '\n'
print '**************************'

######################################################


