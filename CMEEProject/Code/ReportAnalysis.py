#!/usr/bin/env python


# Import packages
print '\n'
print 'Importing packages..'

from pprint import pprint  # pretty-printer
from collections import defaultdict  # remove words that appear only once
from gensim import corpora, models, similarities
from gensim.models import CoherenceModel
#from nltk import word_tokenize
from nltk.corpus import stopwords
from sklearn import feature_extraction
from nltk.stem.snowball import SnowballStemmer # for stemming

#from lxml import etree # to deal with xml file format

import re 			# for use of regular expressions
import os 
import sys 
import csv 			# to import and export csvs
import nltk 		# natural language toolkit
import numpy as np  # for gensim package
import codecs
import string
import pandas as pd # for dataset manipulation
import unicodedata  # to deal with xml file format
import matplotlib.pyplot as plt

import itertools
import operator
from operator import itemgetter
import collections

###############################################
# IMPORT DATA - PREPARE DICT & CORPUS - SAVE
###############################################

print '*****************************'
print 'Import data...'

# Import Clean dataframe (same in TopicAnal.py)
with open('../Data/all_data_top_300.csv', 'r') as newcsv:
	reader = csv.reader(newcsv)
	my_data = [tuple(line) for line in reader]

# Remove header
my_data.pop(0)

# Keep only abstracts 
print 'Keeping only abstracts..'
all_abstracts = [paper[5] for paper in my_data]

#Import common words
stopw = []
with open('../Data/stopwords.txt', 'r') as newcsv:
	stopw = newcsv.readlines()
stopw = [re.sub('\n','',x) for x in stopw]

#Count ecology and evolution words appear
stemmer = SnowballStemmer('english')
ecoevol = ['ecolog', 'ecology', 'ecologist', 'evolutionari', 'evolution', 'evolutionary', 'evolut', 'evolv']

ecol = ['ecolog', 'ecology', 'ecologist', 'ecologi', 'ecologis']
evol = ['evolutionari', 'evolution', 'evolutionary', 'evolut', 'evolv','evolutionar']


# Turn clean dataset into dictionary based on ID
FDataDict = defaultdict(int)
for i in my_data:
	FDataDict[i[1]] = [i[2], i[6], i[5]]


##################################################
print '******************************'
print 'Cleaning abstracts...'


ECEVJourn = defaultdict(int) # to count words evol/ecol in each Journal
WCJourn = defaultdict(int)
IDAdict = defaultdict(int) # to save cleaned stemmed abstract based on id
EcolJourn = defaultdict(int)
EvolJourn = defaultdict(int)

c = 0
wr = 0
wrstout = []
#totest = []
for i in my_data:

	abstr = re.sub('[!#$%&()*+,-1234567890./:;\'<=>?@^"_{|}~]','', i[5].lower())
	# Tokenize
	abstr = abstr.split()

	clean_abstr = []

	for word in abstr:
		if word not in stopw:
			clean_abstr.append(word)

	stem_abstr = [stemmer.stem(word) for word in clean_abstr]

	clean_abstr = []
	for word in stem_abstr:

		if word not in stopw:
			clean_abstr.append(word)
			WCJourn[i[2]] += 1

		else: 
			wr += 1
			wrstout.append(word)

		if word in ecoevol:
			ECEVJourn[i[2]] += 1
			#totest.append(word)

		if word in ecol:
			EcolJourn[i[2]] += 1

		if word in evol:
			EvolJourn[i[2]] += 1


	IDAdict[i[1]] = clean_abstr
	c +=1

	if c % 1000 == 0:
		print '%s abstracts cleaned!' %c

	
RatioJourn= defaultdict(int)
REcolJourn= defaultdict(int)
REvolJourn= defaultdict(int)

for journ in ECEVJourn.keys():
	RatioJourn[journ] = (float(ECEVJourn[journ]) / WCJourn[journ] )*100
	REcolJourn[journ] = (float(EcolJourn[journ]) / WCJourn[journ] )*100
	REvolJourn[journ] = (float(EvolJourn[journ]) / WCJourn[journ] )*100


###################################################
print '******************************'
print 'Decide Journals...'

OrderedJournals = sorted(ECEVJourn.items(), key=operator.itemgetter(1)) #get ordered tuples
OrderedJournalsR = sorted(RatioJourn.items(), key=operator.itemgetter(1)) #get ordered tuples
EcolOrder = sorted(REcolJourn.items(), key=operator.itemgetter(1)) #get ordered tuples
EvolOrder = sorted(REvolJourn.items(), key=operator.itemgetter(1)) #get ordered tuples

export_csv(EcolOrder, '../Results/ScopusAnalysis/JRankEcol.csv', 'nlist')
export_csv(EvolOrder, '../Results/ScopusAnalysis/JRankEvol.csv', 'nlist')

export_csv(IDAdict,'../Results/ScopusAnalysis/Clean_id_stem.csv', 'fldictionary' )

#myJournals = [x for x,y in OrderedJournalsR if y > 0.56]
EVJ= [x for x,y in EvolOrder]
EVJ2= EVJ[47:]
EVJ3 =[]
for i in reversed(np.array(EVJ2)):
	EVJ3.append(i)


ECJ= [x for x,y in EcolOrder]
ECJ2= ECJ[47:]
ECJ3 =[]
for i in reversed(np.array(ECJ2)):
	ECJ3.append(i)

myJournals = list(set(ECJ3+EVJ3))

# Calculate specialists and generalists
Jsgscore = []
for i in myJournals:
	ecscore = ECJ.index(i)
	evscore = EVJ.index(i)

	Jsgscore.append([i,ecscore - evscore])

export_csv(myJournals, '../Results/ScopusAnalysis/myjournals.csv', 'flist')
export_csv(Jsgscore, '../Results/ScopusAnalysis/GSJscores.csv', 'nlist')
# Cut data

CDataDict = defaultdict(int) # dictionary for data that we keep
CData = []
final_abstr = [] # to store all abstracts that go into the model

# Keep in dictionary only abstract that I want
for pid, ab in IDAdict.iteritems():
	forcorp = [pid]
	if FDataDict[pid][0] in myJournals:
		final_abstr.append(ab)
		CDataDict[pid] = FDataDict[pid] # dictionary matching id to data

		forcorp.append(FDataDict[pid][0])
		forcorp.append(FDataDict[pid][1])

		CData.append(forcorp)

#export_csv(CDataDict, '../Results/ScopusAnalysis/CleanData.csv', 'fdictionary')
export_csv(CData, '../Results/ScopusAnalysis/CleanData.csv', 'nlist')
###################################################
# Frequency cleaning

print 'Removing words that appear < 0.005 of corpus and > 0.8 ...'

corpfreq = defaultdict(int)
for text in final_abstr:

    newset = list(set(text)) # keep unique instances
    for tok in newset:
    	corpfreq[tok] += 1

	
abstr_4_corpus = [[token for token in abstr if corpfreq[token] > len(final_abstr)*0.005 and corpfreq[token]<len(final_abstr)*0.8] for abstr in final_abstr]



###################################################
# 
print 'Saving stemmed as dictionary..'

scopus_dict = corpora.Dictionary(abstr_4_corpus)
scopus_dict.save('../Data/ScopusDictCorp/scopus_dict.dict')
	
print 'Tokenizing: converting strings to vectors...'
## Matches every word in the dictionary to a number and then creates
## a list of tuples for each document with the first element the word 
## number and second the times it appears in the document
## convert dictionary to a bag of words corpus for reference 

scopus_corpus = [scopus_dict.doc2bow(text) for text in abstr_4_corpus]

AbID = defaultdict(int) # dictionary that matches IDs with corpus
c = 0
for i in scopus_corpus:
	AbID[CData[c][0]] = i
	c +=1

print 'Saving as bag of words corpus..'
corpora.MmCorpus.serialize('../Data/ScopusDictCorp/scopus_corpus.mm', scopus_corpus)

export_csv(AbID,'../Results/ScopusAnalysis/IDCorpus.csv', 'fdictionary' )


###############################################
# RUN LDA 
###############################################

print 'Running LDA...'
lda100 = models.LdaModel(scopus_corpus, id2word= scopus_dict, num_topics =100)

make_wordclouds(WTlda, wadd = '')
WTlda = create_topics_words_table(lda100, 50 , time = False)
# lda100.save('../Results/ModelsScopus/scopus_lda100.lda')
# print 'LDA 100 done!'

# print 'Running LDA ASSYMETRIC...'
# lda100as = models.LdaModel(scopus_corpus, id2word= scopus_dict, num_topics =100, alpha = 'auto')

# lda100as.save('../Results/ModelsScopus/scopus_lda100_assymetric.lda')
# print 'LDA 100 done!'

# RUN DIFFERENT RANDOM STATES
#umass_coh = []
AllTWs100 = defaultdict(int)
AllTWs150 = defaultdict(int)
AllTWs50 = defaultdict(int)

for i in range(10):

	
	print 'Running model %s ..'%i
	lda100 = models.LdaModel(scopus_corpus, id2word= scopus_dict, num_topics =100, random_state= i, iterations = 10000, minimum_probability = 0.0000001)
	lda100.save('../Results/ModelsScopus/scopus_lda100_rd%s.lda'%i)
	
	TW0 = create_TopWordsDistr(lda100, scopus_dict)
	AllTWs100[i] = TW0

	#print 'Calculating coherence..'
	#coh100 = CoherenceModel(model=lda100, corpus= scopus_corpus, coherence='u_mass')
	#umass_coh.append(coh100.get_coherence())
	
	#print 'Preparing Word Clouds...'
	#WTlda = create_topics_words_table(lda100, 20 , time = False)
	#make_wordclouds(WTlda, wadd = '%s'%i)
	print '   '

make_wordclouds(WTlda, wadd = '')


###############################################
# CHOOSE MODEL RUN
###############################################
# MODEL RUN AND SELECTION
###############################################

## 1) WITH DISTANCE BETWEEN TOPICS - HEATMAP
## Calculate KL- divergence (OR CHANGE FOR HELLINGER)

def models_distance(distr1, distr2, returnelem = 'N'):

	distmatrix = np.zeros((len(distr1), len(distr2)))
	for i in range(len(distr1)):
		for k in range(len(distr2)):
			distmatrix[i,k] = JSD(distr1[i], distr2[k])
			#distmatrix[i,k] = hellinger(distr1[i], distr2[k])

	
	row_ind , col_ind = linear_sum_assignment(distmatrix)
	jc = np.array(col_ind)
	jr = np.array(row_ind)
	outmatrix = distmatrix[:,jc]
	outmatrix = outmatrix[jr,:]

	return outmatrix



model_comp4 = np.zeros((len(AllTWs50), len(AllTWs50)))

for i in range(10):
	#if i == 9:
	#	break
	for k in range(i,10):
		print 'Models %s and %s are being compared..' %(i,k)
		model_comp4[i,k] = models_distance(AllTWs50[i],AllTWs50[k], 'similar')


# 3 and 6 seems more similar

for i in [4]:
	ld1 = models.LdaModel.load('../Results/ModelsScopus/scopus_lda100_rd%s.lda'%i)
	WTlda = create_topics_words_table(ld1, 20 , time = False)
	#make_wordclouds(WTlda, wadd = '%s'%i)

ld1 = models.LdaModel.load('../Results/ModelsScopus/scopus_lda100_rd9.lda')
ld2 = models.LdaModel.load('../Results/ModelsScopus/scopus_lda100_rd5.lda')

TW1 = create_TopWordsDistr(ld1, scopus_dict)
TW2 = create_TopWordsDistr(ld2, scopus_dict)

matrix27 = models_distance(AllTWs50[2], AllTWs50[7], 'matrix')
matrix22 = models_distance(AllTWs50[2], AllTWs50[2], 'matrix')

matrix44 = models_distance(TW1, TW2, 'matrix')

plt.close('all')
KL01 = sns.heatmap(matrix44, cmap = 'bone')
plt.savefig('../Results/ScopusAnalysis/heatmapdisttry.eps')

###############################################################
# B. With Jensen-Divergence
#from scipy.stats import entropy
from numpy.linalg import norm
#import numpy as np
def create_TopWordsDistr(ldamodel, i_dictionary):
	''' Extract the probability distribution of topics over words from the model
	and saves it in a dictionary
	'''
	# Creates table with topic words
	print 'Extracting topics distributions over words from model.. '
	import my_functions

	# TopicsYWords is  a list of lists of lists of tuples
	# 1st level corresponds to topics (0,100)
	# 2nd are all words with their probablilies of each year-topic
	FullTopWordsDistr = []

	for i in range(ldamodel.num_topics):
		newtop = ldamodel.show_topic(i, len(i_dictionary))
		twords = [w for w, v in newtop]
		wprob = [v for w,v in newtop]
		newfulline = []

		for k in range(0,len(i_dictionary)):
			nword = i_dictionary[k]
			newfulline.append(wprob[twords.index(nword)])

		FullTopWordsDistr.append(newfulline)

	return FullTopWordsDistr


def JSD(P, Q):
	'''Jensen-Shanon divergence from 
	https://stackoverflow.com/questions/15880133/jensen-shannon-divergence
	'''
	from numpy.linalg import norm
	_P = P / norm(P, ord=1)
	_Q = Q / norm(Q, ord=1)
	_M = 0.5 * (_P + _Q)
	return 0.5 * (scipy.stats.entropy(_P, _M) + scipy.stats.entropy(_Q, _M))

def TopicsEntropy(modtw):
	entropies = []
	for i in range(len(modtw)):
		entropies.append(scipy.stats.entropy(modtw[i], qk = None))
	return entropies

def models_distance(distr1, distr2, rsd1, rsd2):
	from scipy.optimize import linear_sum_assignment

	distmatrix = np.zeros((len(distr1), len(distr2)))
	for i in range(len(distr1)):
		for k in range(len(distr2)):
			distmatrix[i,k] = JSD(distr1[i], distr2[k])
			#distmatrix[i,k] = hellinger(distr1[i], distr2[k])

	
	row_ind , col_ind = linear_sum_assignment(distmatrix)
	jc = np.array(col_ind)
	jr = np.array(row_ind)
	outmatrix = distmatrix[:,jc]
	outmatrix = outmatrix[jr,:]
	
	#np.savetxt('../Results/ScopusAnalysis/EntropyResults/JSD%s_%s_%s.txt'%(len(distr1),rsd1,rsd2), outmatrix)
	
	JSDsum = []
	for i in range(len(distr1)):
		JSDsum.append(distmatrix[row_ind[i], col_ind[i]])

	return sum(JSDsum)/len(distr1)

def ModelsJSD(modelsdirectory, diction, n_models, n_top):

	topicentropy = []
	forR = []
	JSDmatrixtoexport = np.zeros((n_models, n_models)) 
	for i in range(n_models):
		thisntop = []


		mod1 = modelsdirectory + '%s.lda'%i
		lda1= models.LdaModel.load(mod1)
		tw1 = create_TopWordsDistr(lda1, diction)

		topicentropy.append(TopicsEntropy(tw1))

		if i == n_models-1 :
				print 'All models compared!'
				break

		for k in range(i+1,n_models):
			mod2 = modelsdirectory + '%s.lda'%k
			lda2= models.LdaModel.load(mod2)
			tw2 = create_TopWordsDistr(lda2, diction)

			print 'Cacluating JSD of models %s and %s, with n of topics %s' %(i,k,n_top)

			newJSD = models_distance(tw1,tw2,i,k)
			JSDmatrixtoexport[i,k] = newJSD
			#forR.append(newJSD)


	#TSSum = sum(TermStabilities)
	#print TSSum
	np.savetxt('../Results/ScopusAnalysis/EntropyResults/ModDist_%s.txt'%n_top, JSDmatrixtoexport)
	#export_csv(topicentropy, '../Results/ScopusAnalysis/EntropyResults/TopicsEntropy_%s.txt'%(n_top), 'nlist')

	return JSDmatrixtoexport


topics_test = [2,5, 10,15]+range(20,260,10) + [300]
num_models = 20 # how many models for each topic, how many seeds!

import scipy
run_ldas(topics_test, num_models, scopus_corpus, scopus_dict, save_direct ='../Results/ScopusAnalysis/LDAScStability/')

print 'Distance started...'
HDict = defaultdict(int)
forRlist = []
for i in topics_test:
	forRlist.append(ModelsJSD('../Results/ScopusAnalysis/LDAScStability/lda_%s_rds_'%i, scopus_dict, num_models, i))
	export_csv(forRlist,'../Results/ScopusAnalysis/EntropyResults/AddJSDfinal.csv', 'nlist')

export_csv(forRlist,'../Results/ScopusAnalysis/EntropyResults/AddJSDfinal.csv', 'nlist')

#ld1 = models.LdaModel.load('../Results/ScopusAnalysis/LDAScStability/lda_130_rds_2.lda')
#ld2 = models.LdaModel.load('../Results/ScopusAnalysis/LDAScStability/lda_130_rds_5.lda')
HDict[200] =  ModelsJSD('../Results/ScopusAnalysis/LDAScStability/lda_200_rds_', scopus_dict, num_models, 200)

MD130 = np.loadtxt('../Results/ScopusAnalysis/EntropyResults/JSD130_1_2.txt')
KL01 = sns.heatmap(MD130, cmap = 'bone')
plt.savefig('../Results/ScopusAnalysis/heatmapMD130.eps')


np.savetxt('../Results/ScopusAnalysis/StabilityResults/FullStabilityCorrect.txt', FullStability)


def ModelsTopicsEntropy(modelsdirectory, diction, n_models, n_top):
	'''short version of ModelsJSD that just calculates the entropy of each topics of each model with the 
	same number of topics'''
	
	topicentropy = []
	for i in range(n_models):
		mod1 = modelsdirectory + '%s.lda'%i
		lda1= models.LdaModel.load(mod1)
		tw1 = create_TopWordsDistr(lda1, diction)

		topicentropy.append(TopicsEntropy(tw1))

	export_csv(topicentropy, '../Results/ScopusAnalysis/EntropyResults/TopicsEntropy_%s.txt'%(n_top), 'nlist')

	return topicentropy

# additional run for stability
topics_test = range(210,250,10)
num_models = 20 # how many models for each topic, how many seeds!

run_ldas(topics_test, num_models, scopus_corpus, scopus_dict, save_direct ='../Results/ScopusAnalysis/LDAScStability/')

print 'Coherence started...'

Cohmatrixtoexport = np.zeros((len(topics_test), num_models)) 
ci = 0
for i in topics_test:
	print 'Cacluating coherence of models with n of topics %s' %i

	thisntop = []
	for k in range(num_models):
		mod1 = '../Results/ScopusAnalysis/LDAScStability/lda_%s_rds_%s.lda'%(i,k)
		lda1= models.LdaModel.load(mod1)

		coh1 = CoherenceModel(model=lda1, corpus= scopus_corpus, coherence='u_mass')
		Cohmatrixtoexport[ci,k] = coh1.get_coherence()
		print 'One coh done!'
	
	np.savetxt('../Results/ScopusAnalysis/StabilityResults/NewCoh160170.txt', Cohmatrixtoexport)

	
	ci += 1

np.savetxt('../Results/ScopusAnalysis/StabilityResults/NewCoh160170.txt', Cohmatrixtoexport)


#############################################################
## 1) WITH STABILITY

def run_ldas(num_topics, num_models, my_corpus, my_dict, save_direct, num_iter= 100000, min_prob = 0.000000001):

	for i in num_topics:

		for k in range(num_models):
			print 'Running model %s with %s topics..'%(k,i)

			lda = models.LdaModel(my_corpus, id2word= my_dict, num_topics =i, random_state= k, iterations = num_iter, minimum_probability = min_prob)
			svdir = save_direct +'lda_%s_rds_%s.lda'%(i,k)
			lda.save(svdir)

		if i == num_topics[len(num_topics)/4]:
			print '***************************'
			print ' 25 %% of work done!'
			print '\n'
		if i == num_topics[len(num_topics)/2]:
			print '***************************'
			print ' 50 %% of work done!'
			print '\n'
		if i == num_topics[3*len(num_topics)/4]:
			print '***************************'
			print ' 75 %% of work done!'
			print '\n'

	print 'All models saved with names lda_n_rds_s.lda,'
	print 'with n: num of topics and s: num of rds.'




def get_topics_nterms(ldamodel, n_words):
	TopicsYWords = []
	for i in range(ldamodel.num_topics):
		TopicsYWords.append(ldamodel.show_topic(i, n_words))

	onlywords = [[w for w,f in top] for top in TopicsYWords]

	return onlywords


def JackInd(T1words, T2words):
	return 1-float(len(set(T1words).intersection(T2words)))/ (len(set(T1words+ T2words)))

def AvJackInd(T1words, T2words, terms):

	tosum = []
	for i in range(1,terms+1):
		t1w = T1words[:i]
		t2w = T2words[:i]

		tosum.append(JackInd(t1w, t2w))

	avsum = sum(tosum)

	return avsum/terms


def TermStability(lda1, lda2, terms):

	n_top = lda1.num_topics 
	# get top n terms of each model
	AllTopWords1 = get_topics_nterms(lda1, terms)
	AllTopWords2 = get_topics_nterms(lda2, terms)
	#initialize empty similarity matrix
	AverageJaccardM = np.zeros((n_top, n_top)) 

	# Calculate average jaccard matrix
	for i in range(n_top):
		for k in range(n_top):
			T1 = AllTopWords1[i]
			T2 = AllTopWords2[k]

			AverageJaccardM[i,k] = AvJackInd(T1,T2,terms)

	print AverageJaccardM
	np.savetxt('../Results/ScopusAnalysis/testdif130.txt', AverageJaccardM)

	# print 'Average Jaccard Matrix done!'
	# Hungarian method on similarity matrix for minimum weight match
	from scipy.optimize import linear_sum_assignment
	#get index matches for rows and columns(topic1 and topic2)
	row_ind , col_ind = linear_sum_assignment(AverageJaccardM)
	
	# save one JC matrix
	# jc = np.array(col_ind)
	# jr = np.array(row_ind)
	# outmatrix = AverageJaccardM[:,jc]
	# outmatrix = outmatrix[jr,:]
	# np.savetxt('../Results/ScopusAnalysis/JImatrix.txt', outmatrix)

	# Calculate term stability (based on the sum of index of all mathing topics)
	TSsum = []
	for i in range(n_top):
		TSsum.append(AverageJaccardM[row_ind[i], col_ind[i]])

	# print 'Term stability calculated!'
	print '   '
	return float(sum(TSsum))/n_top



def AverageTermStability(modelsdirectory, n_models, terms, n_top):

	TermStabilities = []
	TSmatrixtoexport = np.zeros((n_models, n_models)) 
	c  = 0
	for i in range(n_models):
		thisntop = []
		if i == n_models-1 :
				print 'All models compared!'
				break
		for k in range(i+1,n_models):
			mod1 = modelsdirectory + '%s.lda'%i
			mod2 = modelsdirectory + '%s.lda'%k

			lda1= models.LdaModel.load(mod1)
			lda2= models.LdaModel.load(mod2)

			print 'Cacluating stability of models %s and %s, with n of topics %s' %(i,k,n_top)

			newts = TermStability(lda1, lda2, terms)
			TermStabilities.append(newts)
			c +=1
			TSmatrixtoexport[i,k] = newts


	TSSum = sum(TermStabilities)
	#print TSSum
	np.savetxt('../Results/ScopusAnalysis/StabilityResults/AvTSt_%s_%s.txt'%(n_top,terms), TSmatrixtoexport)


	return 1- (TSSum/c)


testStab100 = AverageTermStability('../Results/ModelsScopus/scopus_lda100_rd', 10, 20)
#testStab50 = AverageTermStability('../Results/ModelsScopus/scopus_lda50_rd', 10, 20)



###################################
# full analysis
topics_test = [2,5,10,15] + range(20,160,10) +[200,250,300]
terms_test = [10, 20, 50]#, 100] #add 100 if needed or have time afterwards
num_models = 20 # how many models for each topic, how many seeds!

run_ldas(topics_test, num_models, scopus_corpus, scopus_dict, save_direct ='../Results/ScopusAnalysis/LDAScStability/')

print 'Stability started...'
FullStability = np.zeros((len(topics_test), len(terms_test))) 

ci = 0 # to count the topics number
for i in topics_test:
	ti = 0 # to count the terms number
	for k in terms_test:
		newav = AverageTermStability('../Results/ScopusAnalysis/LDAScStability/lda_%s_rds_'%i, num_models, k ,i)
		FullStability[ci,ti] = newav
		ti += 1
	ci += 1

#ld1 = models.LdaModel.load('../Results/ScopusAnalysis/LDAScStability/lda_130_rds_2.lda')
#ld2 = models.LdaModel.load('../Results/ScopusAnalysis/LDAScStability/lda_130_rds_5.lda')


np.savetxt('../Results/ScopusAnalysis/StabilityResults/FullStabilityCorrect.txt', FullStability)

####
print 'Coherence started...'

FullCoherence = np.zeros((len(topics_test), num_models)) 


print 'Calculating coherence'
Cohmatrixtoexport = np.zeros((len(topics_test), num_models)) 

ci = 0
for i in topics_test:
	print 'Cacluating coherence of models with n of topics %s' %i

	thisntop = []
	for k in range(num_models):
		mod1 = '../Results/ScopusAnalysis/LDAScStability/lda_%s_rds_%s.lda'%(i,k)
		lda1= models.LdaModel.load(mod1)

		coh1 = CoherenceModel(model=lda1, corpus= scopus_corpus, coherence='u_mass')
		Cohmatrixtoexport[ci,k] = coh1.get_coherence()
	
	ci += 1

np.savetxt('../Results/ScopusAnalysis/StabilityResults/FullCoherence.txt', Cohmatrixtoexport)


#### Round 2 for filling up

# full analysis
topics_test = [3,4,6,7,8,9,11,12,13,14,16,17,18,19]
num_models = 20 # how many models for each topic, how many seeds!

run_ldas(topics_test, num_models, scopus_corpus, scopus_dict, save_direct ='../Results/ScopusAnalysis/LDAScStability/')

print 'Stability started...'
FullStability2 = np.zeros((len(topics_test), len(terms_test))) 

ci = 0 # to count the topics number
for i in topics_test:
	ti = 0 # to count the terms number
	for k in terms_test:
		newav = AverageTermStability('../Results/ScopusAnalysis/LDAScStability/lda_%s_rds_'%i, num_models, k ,i)
		FullStability2[ci,ti] = newav
		ti += 1
	ci += 1


np.savetxt('../Results/ScopusAnalysis/StabilityResults/AdditStability.txt', FullStability)


print 'Coherence started...'

FullCoherence2 = np.zeros((len(topics_test), num_models)) 


print 'Calculating coherence'
Cohmatrixtoexport2 = np.zeros((len(topics_test), num_models)) 

ci = 0
for i in topics_test:
	print 'Cacluating coherence of models with n of topics %s' %i

	thisntop = []
	for k in range(num_models):
		mod1 = '../Results/ScopusAnalysis/LDAScStability/lda_%s_rds_%s.lda'%(i,k)
		lda1= models.LdaModel.load(mod1)

		coh1 = CoherenceModel(model=lda1, corpus= scopus_corpus, coherence='u_mass')
		Cohmatrixtoexport2[ci,k] = coh1.get_coherence()
	
	ci += 1

np.savetxt('../Results/ScopusAnalysis/StabilityResults/AdditCoherence.txt', Cohmatrixtoexport2)


#########
# print 
dm = np.loadtxt('../Results/ScopusAnalysis/JImatrix.txt')
KL01 = sns.heatmap(dm, cmap = 'bone')
plt.savefig('../Results/ScopusAnalysis/heatmapJC.eps')





###############################################################
# Create fake corpus

fake_corpus = []
for i in scopus_corpus:
	onlyfreq = [y for x,y in i]
	newwords = np.random.choice(range(len(scopus_dict)),len(onlyfreq))
	newwords2 = np.sort(newwords)
	fakeabstr = zip(newwords2, onlyfreq)
	#fakeabstr2 = sorted(fakeabstr , key=lambda x: x[0])
	fake_corpus.append(fakeabstr)

# Run null models

topics_test = [2,5,10,15] + range(20,160,10) #+ [200,250,300]
terms_test = [10, 20, 50]#, 100] #add 100 if needed or have time afterwards
num_models = 20 # how many models for each topic, how many seeds!

run_ldas(topics_test, num_models, fake_corpus, scopus_dict, save_direct ='../Results/ScopusAnalysis/LDAScStability/Null/')

print 'Stability started...'
FakeStability = np.zeros((len(topics_test), len(terms_test))) 

ci = 0 # to count the topics number
for i in topics_test:
	ti = 0 # to count the terms number
	for k in terms_test:
		newav = AverageTermStability('../Results/ScopusAnalysis/LDAScStability/Null/lda_%s_rds_'%i, num_models, k ,i)
		FakeStability[ci,ti] = newav
		ti += 1
	ci += 1


np.savetxt('../Results/ScopusAnalysis/StabilityResults/FakeStability.txt', FakeStability)



###############################################################
# CHOOSE LDA!!! 140 TOPICS WITH RDS 9

win_lda = models.LdaModel.load('../Results/ScopusAnalysis/LDAScStability/lda_140_rds_9.lda')


###############################################
# GET TOPICS DISTRIBUTIONS OUT 
###############################################

# Make wordclouds

WTlda = create_topics_words_table(win_lda, 25 , time = False)
make_wordclouds(WTlda, wadd = '')

# Get topics distribution over words
TopWordsDistrLDA = create_TopWordsDistr(win_lda, scopus_dict)

# save csv
wordsheader = scopus_dict.values()
export_csv(TopWordsDistrLDA,'../Results/ScopusAnalysis/TopWordDistrLDA.csv' , 'nlist', wordsheader)



# GET DOCUMENT DISTRIBUTIONS OVER TOPICS

def create_DocTopDistr(ldamodel, corpus, dataset):
	'''Combines model results with general data of the documents
	eg. year published, author. Produces a lists of tuples with:
	info from dataset and topics probabilities '''

	# Add topics to document information
	combined = itertools.izip(corpus, ((d[0],d[1], d[2]) for d in dataset))
	topic_data = []  #for each article, collect topic with highest score
	#topic_stats = []  # gets all topic-score pairs for each document

	print 'Pairing topics with information..' 
	print '(This may take a while)'

	c = 0
	filled_data = [] # store topics probabilities of each document 

	for corp_txt,(scid, journ, year) in combined:
		if year == '2017':
			print '2017 found and skipped'
			continue
		srtd = sorted(ldamodel[corp_txt], key=itemgetter(1), reverse=1)
		top = [topic for topic, scores in srtd]
		score = [scores for topic, scores in srtd]

		new_doc = [scid, journ, year] # add id infront
		for i in range(ldamodel.num_topics):
			if i in top:
				new_doc.append(score[top.index(i)])
			else:
				new_doc.append(0)
		filled_data.append(new_doc)
		c= c+1

		if c%5000 == 0:
			print c, 'abstracts done..'


	Topics = ['topic'+ str(i) for i in range(1, ldamodel.num_topics +1)]
	Topics.insert(0, 'year')
	Topics.insert(0, 'journal')
	Topics.insert(0, 'ScID')

	export_csv(filled_data, '../Results/ScopusAnalysis/DocTopDistrLDA.csv', 'nlist', Topics)

	print 'All Documents done!'
	return filled_data


#LDAFullDf = create_full_data(lda100, scopus_corpus , CData)
DocTopDistrLDA = create_DocTopDistr(win_lda, scopus_corpus, CData)


#####################################
# CLUSTER 

def cluster_topics(LinkTop, d_threshold, figtype, heatmethod= "weighted", clustmethod ='weighted'):
	# Create distance matrix

	def fancy_dendrogram(*args, **kwargs):
		max_d = kwargs.pop('max_d', None)
		
		if max_d and 'color_threshold' not in kwargs:
			kwargs['color_threshold'] = max_d
		annotate_above = kwargs.pop('annotate_above', 0)

		ddata = dendrogram(*args, **kwargs)

		if not kwargs.get('no_plot', False):
			#plt.title('Hierarchical Clustering Dendrogram: %s & c = %s' %(clustmethod, metric))
			plt.xlabel('Topics')
			plt.ylabel('Distance')
			for i, d, c in zip(ddata['icoord'], ddata['dcoord'], ddata['color_list']):
				x = 0.5 * sum(i[1:3])
				y = d[1]
				if y > annotate_above:
					plt.plot(x, y, 'o', c=c)
					#plt.annotate("%.3g" % y, (x, y), xytext=(0, -5),
					#			textcoords='offset points',
					#			va='top', ha='center')
					plt.grid(False)
			if max_d:
				#plt.axhline(y=max_d, c='k')
				1+1
		return ddata

	# PLOT DENDOGRAM
	plt.figure(figsize=(10,10))
	ax = plt.gca()
	ax.set_facecolor('white')
	clusters = fancy_dendrogram(
		LinkTop,
		truncate_mode='lastp',
		#p=30,
		leaf_rotation=90.,
		leaf_font_size=6.,

		#show_contracted=True,
		#annotate_above=40,
		max_d= d_threshold,
	)

	d_save= "../Results/ScopusAnalysis/fdendrogram_new_%s_%s" %(clustmethod,figtype)
	plt.savefig(d_save, edgecolor='none')
	plt.close('all')

	return clusters


def create_ClustDistr(my_ward, TopWordsDistr):

	wcolors = my_ward['color_list']
	wtopics = my_ward['leaves']

	wcolors = [x for x in wcolors if x != 'b'] # rv blue nodes- big

	# GET CLUSTERS

	TopicsClusters = defaultdict(int)
	c = 0
	t = 0
	n = 0
	iprev = wcolors[0]
	new = []
	for i in wcolors:
		if i == iprev:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1

		else:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			c = c+1
			t = t+1
			n = n+1
			new = []
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1
			iprev = i

			print 'New cluster n. %s' %n
	new.append(wtopics[t])
	TopicsClusters[c] = new

	# Create words distributions for each cluster (Average all)

	ClustDistr = []
	for i in TopicsClusters.values():
		toclust = []
		for top in i:
			toclust.append(TopWordsDistr[top])

		newclust = [float(sum(elem))/len(elem) for elem in zip(*toclust)]
		ClustDistr.append(newclust)

	return ClustDistr

# See words of clusters
# Create list of tuples (word, frequency) for each cluster

def clusters_forWC(ClustDistr, n_words, i_dictionary):

	ClustWords = []
	c = 0

	for i in ClustDistr:

		new_line = []
		for n, w in i_dictionary.iteritems():
			#freqs.append(i[n])
			new_line.append((w , i[n]))

		ClustWords.append(new_line)
		print 'Cluster %s done!' %c
		c = c+1 

	# Keep only 50 
	import heapq

	ClustW50 = []
	n = 0
	for i in ClustWords:
		incloud = []
		freqs = [f for w,f in i]
		lar = heapq.nlargest(n_words, freqs)
		for k in lar:
			incloud.append(i[freqs.index(k)])

		ClustW50.append(incloud)
		print 'Cluster %s sorted!' %n
		n += 1

	return ClustW50

# Make word clouds 

make_wordclouds(ClustW50, 'cl')


############################################
#  Run
TopicsDistMatrix = hellinger(TopWordsDistrLDA)
LinkTop = linkage(TopicsDistMatrix, method= 'ward')
metric, coph_dists = cophenet(LinkTop, pdist(TopicsDistMatrix))
print 'Comphenet is %s' %metric

cl = cluster_topics(LinkTop, 1.54, '.eps' , heatmethod= "weighted", clustmethod ='weighted')

ClustDistr = create_ClustDistr(cl, TopWordsDistrLDA)
ClustW50 = clusters_forWC(ClustDistr, 50, scopus_dict)
make_wordclouds(ClustW50, '_CL') # nice!

toprow = cl['leaves'] 
export_csv(toprow, '../Results/ScopusAnalysis/TDendrRow.csv', 'flist')

###########################################################
# GET DOCUMENTS AND JOURNALS DISTRIBUTIONS OVER TOPICS


# cluster journals
JTdstr = import_csv('../Results/ScopusAnalysis/JournTopDistr.csv', 'list', 'strings')
JTdstr.pop(0)
JTdstrDict = {x[1]:[float(y) for y in x[2:]] for x in JTdstr}

JTmatrix = np.array(JTdstrDict.values())
JournDistMatrix = hellinger(JTmatrix)
LinkJourn = linkage(JournDistMatrix, method= 'ward')
cl = cluster_topics(LinkJourn, 1, 'Journ.eps' , heatmethod= "weighted", clustmethod ='weighted')

journrow = cl['leaves'] 
JR = [JTdstrDict.keys()[x] for x in journrow]
export_csv(JR, '../Results/ScopusAnalysis/JDendrRow.csv', 'flist')


################################################
# FANCY WORD CLOUDS


from PIL import Image
from wordcloud import WordCloud, STOPWORDS, ImageColorGenerator

# Initialize the word clouds

my_color= np.array(Image.open('circl.png'))

wc = WordCloud(
    background_color="white",
    max_words=25,
    #width = 1024,
    #height = 720,
    #prefer_horizontal = 0.99999 ,
    mask= my_color,
    stopwords=stopwords.words("english"),
    #margin = 5
)

image_colors = ImageColorGenerator(my_color)

# Save word cloud for each year/topic group

n_path= '../Results/ScopusAnalysis/WCReport/circle.png'
TopWords20 =create_topics_words_table(win_lda, 25, time = False)
TW35 = TopWords20[35]
new = defaultdict(int)
for i in TW35:
	new[i[0]] = i[1]
wc.generate_from_frequencies(new)
plt.imshow(wc.recolor(color_func=image_colors), interpolation="bilinear")
wc.to_file(n_path)

plt.imshow(wc, interpolation='bilinear')
plt.axis("off")
plt.figure()
plt.imshow(my_color, cmap=plt.cm.gray, interpolation='bilinear')
plt.axis("off")



#####
# TOPICS- WORDS LIST
ToExport = []
ToExportProb = []
for i in range(win_lda.num_topics):
	newtop = win_lda.show_topic(i,25)
	newwords = [x for x,y in newtop]
	ToExport.append(newwords)
	ToExportProb.append(newtop)

export_csv(ToExport, '../Results/ScopusAnalysis/Top25Words.csv', 'nlist')
pickle.dump(ToExportProb, open('../Results/ScopusAnalysis/Top25Words.p', 'wb'))
loadedtest = pickle.load( open( "../Results/ScopusAnalysis/Top25Words.p", "rb" ) )


LabelDocs = import_csv('../Results/ScopusAnalysis/DocIDtoLabel.csv', 'list', 'strings')
LabelDocs.pop(0)
forRogDoc = defaultdict(int)
for i in range(len(LabelDocs)):
	thisid = LabelDocs[i][0]

	for k in my_data:
		if k[1] == thisid:
			thisab = [k[5]]
			continue

	if forRogDoc[int(LabelDocs[i][1])] == 0:
		forRogDoc[int(LabelDocs[i][1])] = thisab
	else:
		forRogDoc[int(LabelDocs[i][1])] = forRogDoc[int(LabelDocs[i][1])] + thisab

import pickle

pickle.dump(forRogDoc, open('../Results/ScopusAnalysis/TopDocsLabel.p', 'wb'))

loadedtest = pickle.load( open( "../Results/ScopusAnalysis/TopDocsLabel.p", "rb" ) )

############################################
# RUN DTM 
############################################
# setting imports

from gensim.models import ldaseqmodel
from gensim.corpora import Dictionary
from gensim.matutils import hellinger
import itertools
from operator import itemgetter
import collections
 
###############################
# Prepair time slices

print 'Pairing dates with corpus data..' 

combined2 = itertools.izip(scopus_corpus, (d[2] for d in CData))
date_data = []  #for each article, collect time slices


print 'Making list..'
for corp_txt,year in combined2:
	date_data.append((year, corp_txt))

date_data = sorted(date_data, key=itemgetter(0), reverse=0)

# remove mistaken 2017 
to_remove = []
for i in date_data:
	if i[0] == '2017':
		to_remove.append(i)
		print 'Found one!'
for i in to_remove:
	date_data.remove(i)


# Count each years papers
print 'Counting number each year'

YearsCounts= defaultdict(int) # to fill
yearlist = sorted(list(set([date for date, corp in date_data])))
TimeSteps =[] ## My time steps for model

for i in date_data:
	YearsCounts[i[0]] +=1

for i in yearlist:
	TimeSteps.append(YearsCounts[i])

# my sorted corpus for model
s_corpus = [corp for date,corp in date_data] 

dtm_corpus = [[(i,int(f)) for i,f in ab] for ab in s_corpus]

print 'Time steps and sorted corpus ready for the model!'


#############################
# in C Version

# 1. Write seq file with number of documents for each time step

seq_outfile = open(os.path.join('../Data/DTMScopus', '-seq.dat'), 'w')
seq_outfile.write(str(len(TimeSteps)) + '\n') # n of timestamps

for count in TimeSteps:
	seq_outfile.write(str(count)+ '\n') # write total tweets per year

seq_outfile.close()
print 'Done writing seq'

# 2. Write corpus in format needed and create file to put in
multfile = open(os.path.join('../Data', 'DTMScopus', '-mult.dat'), 'w')
c = 0

for i in dtm_corpus:
	multfile.write(str(len(i))+ ' ')
	for (wordID, weigth) in i:
		multfile.write(str(wordID)+ ':' + str(weigth) + ' ')
	multfile.write('\n')
	c = c+1
	if c % 5000 == 0 :
		print c, 'documents passed!'

multfile.close()
print('mult file saved')

# 3. Set paths to executable

# path to dtm home folder
dtm_home = os.environ.get('DTM_HOME', "dtm-master")
# path to the binary
dtm_path = os.path.join(dtm_home, 'dtm', 'main')

# 4. Run models

dtm140 = gensim.models.wrappers.DtmModel(dtm_path, dtm_corpus, TimeSteps, num_topics =140, id2word = scopus_dict ,initialize_lda= True)
dtm140.save('../Results/ScopusAnalysis/DTM/dtm140.lda')

# influece model??
dim100 = gensim.models.wrappers.DtmModel(dtm_path, dtm_corpus, TimeSteps, model= 'fixed', num_topics =100, id2word = i_dictionary,initialize_lda= True)


######################
# GET DTM RESULTS
######################
# formatted prints as prob*string of words strig
dtm140 = models.wrappers.DtmModel.load('../Results/ScopusAnalysis/DTM/dtm140.lda')

DocTopDist = dtm140.gamma_
years= range(2000, 2016)

################################################
# Topic word table

TopicsYWords = []
for i in range(0,140):
	newtopic = []
	for k in range(0,16):
		newtopic.append(dtm140.show_topic(topicid =i, time =k, num_words = 20))

	TopicsYWords.append(newtopic)
# TopicsYWords is  a list of lists of lists of tuples
# 1st level corresponds to topics (0,100)
# 2nd level are the years (0,16)
# 3rd are 20 words with their probablilies of each year-topic

# See distances in word distribution of topics through time
# Same as above but get all words from each topic (not just top 20)

FullTopicsYWords = []
for i in range(dtm140.num_topics):
	newtopic = []
	for k in range(0,16):
		newtopic.append(dtm140.show_topic(topicid =i, time =k, num_words = len(scopus_dict)))

	FullTopicsYWords.append(newtopic)

print 'First List Done!'

FullTopWordsDistr = [] 
for top in FullTopicsYWords:
	newtop = []
	for year in top:
		ytwords = [w for p,w in year]
		ytprob = [p for p,w in year]
		newfulline = []

		for k in range(0,len(scopus_dict)):
			nword = scopus_dict[k]
			if nword in ytwords:
				newfulline.append(ytprob[ytwords.index(nword)])
			else:
				newfulline.append(0)
				print 'Word not from topic found!!'
		newtop.append(newfulline)
	# print 'Topic Done!'

	FullTopWordsDistr.append(newtop)
# FullTopWordsDIstr is a list of lists of lists with all the words probabilities
# of the dictionary for each topic in each year
# 1st level: topics
# 2nd: years
# 3rd: words probabilities
#########################
# Make in csv format for R

t = 0
YWJDistrCSV = []
for top in FullTopWordsDistr:
	t +=1 
	y=0
	for year in top:
		newtop = ['topic%s'%t, years[y]]
		newtop.extend(year)
		y +=1
		YWJDistrCSV.append(newtop)

	#print 'Topic Done!'

csvhead = ['topic', 'year']
csvhead.extend(scopus_dict.values())

export_csv(YWJDistrCSV, '../Results/ScopusAnalysis/TopWordYDistrDTM.csv', 'nlist', header = csvhead)



# Save word cloud for each year/topic group

c=0
y=0
for topic in TopicsYWords:
	c= c+1
	#if TopicsYWords.index(topic) < 48:
	#if c < 73:
	#	continue
	for year in topic:
		y= y+1
		n_path= '../Results/Wordclouds/%s_%s.png' %(c,y)
		new = defaultdict(int)

		for i in year:
			new[i[1]] = i[0]
		wc.generate_from_frequencies(new)
		plt.imshow(wc.recolor(color_func=image_colors), interpolation="bilinear")
		wc.to_file(n_path)
#  pdfjam  
	print 'Topic %s done!' %c
	y=0


############
# Join wordcounts in pdf (name for terminal)
# pdfjam %%%% --frame true --pagenumbering true --outfile AllPlots.pdf

name = []
for i in range(100):
	name.append('%s_3.png'%i)
' '.join(name)

#pdfjam 0_3.png 1_3.png 2_3.png 3_3.png 4_3.png 5_3.png 6_3.png 7_3.png 8_3.png 9_3.png 10_3.png 11_3.png 12_3.png 13_3.png 14_3.png 15_3.png 16_3.png 17_3.png 18_3.png 19_3.png 20_3.png 21_3.png 22_3.png 23_3.png 24_3.png 25_3.png 26_3.png 27_3.png 28_3.png 29_3.png 30_3.png 31_3.png 32_3.png 33_3.png 34_3.png 35_3.png 36_3.png 37_3.png 38_3.png 39_3.png 40_3.png 41_3.png 42_3.png 43_3.png 44_3.png 45_3.png 46_3.png 47_3.png 48_3.png 49_3.png 50_3.png 51_3.png 52_3.png 53_3.png 54_3.png 55_3.png 56_3.png 57_3.png 58_3.png 59_3.png 60_3.png 61_3.png 62_3.png 63_3.png 64_3.png 65_3.png 66_3.png 67_3.png 68_3.png 69_3.png 70_3.png 71_3.png 72_3.png 73_3.png 74_3.png 75_3.png 76_3.png 77_3.png 78_3.png 79_3.png 80_3.png 81_3.png 82_3.png 83_3.png 84_3.png 85_3.png 86_3.png 87_3.png 88_3.png 89_3.png 90_3.png 91_3.png 92_3.png 93_3.png 94_3.png 95_3.png 96_3.png 97_3.png 98_3.png 99_3.png --nup 4x4 --frame true --outfile WCLda100.pdf


###########################################################
# DO CLUSTERS FOR DTM 
# check individual topics to throw away

# run again and compare results
TopicsDistMatrixD = hellinger(TWdistr, False)
LinkTopD = linkage(TopicsDistMatrixD, method= 'ward')
metric, coph_dists = cophenet(LinkTopD, pdist(TopicsDistMatrixD))
print 'Comphenet is %s' %metric

plt.close('all')
plt.figure(figsize=(10,10))
w_clust_nD = fancy_dendrogram(LinkTopD,leaf_rotation=90.,leaf_font_size=7.,max_d=1.43)

plt.savefig("../Results/New_Full/Wardtotest_cleanDTM.eps")

ClustDistrDTM2 = create_ClustDistr(w_clust_nD,  TWdistr)
ClustW50DTM2 = clusters_forWC(ClustDistrDTM2, 50, garbage)
make_wordclouds(ClustW50DTM2, 'Cdtm_cl') # nice!

#########################################################################
# take probabilities of clusters changing for each year from ind topics

YTdistr = import_csv('../Results/dtm100_YearlyTopicDistr.csv', 'list', 'floats')
#remove years from begging of list
YTdistrDTM=  [y[1:] for y in YTdistr]
YTdistrDTM.pop(0)

def get_ClusterGroups(my_ward):

	wcolors = my_ward['color_list']
	wtopics = my_ward['leaves']

	wcolors = [x for x in wcolors if x != 'b'] # rv blue nodes- big

	# GET CLUSTERS

	TopicsClusters = defaultdict(int)
	c = 0
	t = 0
	n = 0
	iprev = wcolors[0]
	new = []
	for i in wcolors:
		if i == iprev:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1

		else:
			new.append(wtopics[t])
			TopicsClusters[c] = new
			c = c+1
			t = t+1
			n = n+1
			new = []
			new.append(wtopics[t])
			TopicsClusters[c] = new
			t = t+1
			iprev = i

			print 'New cluster n. %s' %n
	new.append(wtopics[t])
	TopicsClusters[c] = new

	return TopicsClusters

TopClust = get_ClusterGroups(w_clust_nD)

# average yearly topic probability for topics clustered together
YCdistr = defaultdict(int)
for i in range(len(TopClust)):
	toaverage = []
	for k in TopClust[i]:
		toaverage.append([tr[k] for tr in YTdistrDTM])

	newclust = [float(sum(elem))/len(elem) for elem in zip(*toaverage)]
	YCdistr[i] = newclust

dtm100YCdf= pd.DataFrame.from_dict(YCdistr) # years columns / clusters rows
tdtm100YCdf = dtm100YCdf.transpose() # topics columns/ years rows
tdtm100YCdf.to_csv('../Results/dtm100_YearlyClustersDistr.csv')



#####################################################
# Topic Documents

# vocabulary: [4]
# term_frequency: [3] (from vocabulary)
# doc_lengths: [2]
# topic_term distribution: [1]
# doc_topic distribution: [0] - each array[] probability of 1 document for each topic from 100

doctopicdistr0,topictermdistr0, doc_lengths0 , termfreq0, vocab0 = dtm100.dtm_vis(ready_corpus, 0)

def list_average(nums):
	''' Calculates the mean of a list's elements'''
	return sum(nums)/ float(len(nums))

#extract for each topic distrib for year
f = 0
s = 0
YearTop = defaultdict(int)
YearJournTop = defaultdict(int)
YearDocTop = defaultdict(int)
years = range(2000,2016,1)


for i in range(0,16): # loop through years
	yearsdistr = dtm100.dtm_vis(dtm_corpus, i)[0]
	topyear = defaultdict(int)
	#journtopyear = defaultdict(int)

	for k in range(f, f+TimeSteps[s]): # loop through documents of each year
	#for k in range(0, len(ready_corpus)): # to go through all document (mean of topics)
		newdoc = list(yearsdistr[k])
	#	jrnl = date_data[k][1]
		# print 'In documents'

		for x in range(0,100): # loop through topics in each document
			topyear[x] = (topyear[x]+newdoc[x])/2
		
	#	if jrnl in journtopyear.keys():
	#		journtopyear[jrnl] = [list_average(n) for n in zip(*[journtopyear[jrnl],topyear.values()])]
	#		#journtopyear[jrnl].append(topyear.values())
	#	else:
	#		journtopyear[jrnl] = [topyear.values()]


	YearTop[years[i]] = topyear.values()
	#YearJournTop[years[i]] = journtopyear
	f += TimeSteps[s]
	s += 1
	print 'Year', i ,'done!'

# make dataframe
dtm100YTdf= pd.DataFrame.from_dict(YearTop) # years columns / topics rows
tdtm100YTdf = dtm100YTdf.transpose() # topics columns/ years rows
tdtm100YTdf.to_csv('../Results/ScopusAnalysis/dtm100_YearlyTopicDistr.csv')

dtm100YTdf.plot()

############################
# Create Topic Distribution over documents with year and journal in
f = 0
s = 0
DocTopYdistr = []
PCAdf = []
for i in range(0,16):
	yearsdistr = dtm100.dtm_vis(dtm_corpus, i)[0]
	topyear = defaultdict(int)
	journtopyear = defaultdict(int)
	for k in range(f, f+TimeSteps[s]): 
		newdoc = list(yearsdistr[k])
		nline = [years[i]] + newdoc
		DocTopYdistr.append(nline)
		PCAdf.append(newdoc)

	f += TimeSteps[s]
	s += 1
	print 'Year', i ,'done!'

# Calculate mean and std
values = range(2,102)
ValDist = [itemgetter(*values)(item) for item in DocTopYdistr]
np.mean(ValDist) # 0.01
np.std(ValDist) # 0.044

# Documents/Topics dataframe
export_csv(DocTopYdistr, '../Results/ScopusAnalysis/DocTopDistrDTM.csv', 'nlist')
DTYdf= pd.DataFrame(DocTopYdistr)
pca_df = pd.DataFrame(PCAdf)

# dataframe for journals
#convert it to numpy arrays
X=pca_df.values

#Scaling the values
X = scale(X)
pca = PCA()

pca.fit(X)

#The amount of variance that each PC explains
var= pca.explained_variance_ratio_

#Cumulative Variance explains
var1=np.cumsum(np.round(pca.explained_variance_ratio_, decimals=4)*100)
plt.plot(var1)
plt.show()
plt.close()

##
journals = [y[0] for y in DocTopYdistr]

PCA_PLOT(pca_df , journals, '../Results/PCAs/DocTopics.png', 0)


# First: create dictionary to calculate and store combined probabilities
JTdistr = defaultdict(int) # to fill

for i in range(0, len(target_data)):
	name = target_data[i]
	topdistr = filled_data[i]

	if name in JTdistr.keys():
		JTdistr[name] = [list_average(n) for n in zip(*[JTdistr[name],topdistr]) ]
	else:
		JTdistr[name] = topdistr

##################################################
#Add journals
combined3 = itertools.izip(i_corpus, ((d[2],d[6]) for d in clean_data))

date_data2 = []
print 'Making list..'
for corp_txt, (journal, year) in combined3:
	date_data2.append((year, journal, corp_txt))

date_data2 = sorted(date_data2, key=itemgetter(0), reverse=0)

# remove mistaken 2017 
to_remove = []
for i in date_data2:
	if i[0] == '2017':
		to_remove.append(i)
		print 'Found one!'
for i in to_remove:
	date_data2.remove(i)

# my sorted corpus for model
s2_corpus = [corp for date,corp in date_data2] 

ready_corpus = [[(i,int(f)) for i,f in ab] for ab in s2_corpus]



##########################################################################
# DTM change of topics through time
from math import log

YTDistr = import_csv('../Results/dtm100_YearlyTopicDistr.csv', 'list')
#take header out
YTDistr.pop(0)

# Go through the years and take begining and end
TopChange = []
for i in range(1,101):
	topc = [t[i] for t in YTDistr]
	T0 = log(topc[0])
	TN = log(topc[15])
	dist = T0-TN
	TopChange.append((i,dist))

plt.scatter(*zip(*TopChange))
plt.pause(0.1)
raw_input("Press [enter] to continue.")
plt.close()


##################################################
# # Variation of information (VI)
#
# Meila, M. (2007). Comparing clusterings-an information
#   based distance. Journal of Multivariate Analysis, 98,
#   873-895. doi:10.1016/j.jmva.2006.11.013
#
# https://en.wikipedia.org/wiki/Variation_of_information
