#!/bin/bash
#Author: Marina marina.papadopoulou16@imperial.ac.uk
#Script: unnest downlaoded dataset
#Date: July 2017


###################
# Unzip everything

unzip 'ZIPS/*.zip'
echo 'unziping done!'

find . -maxdepth 1 -type d -exec mv '{}' JSTOR/ ';'

# Copy only .xml files

rsync -avm --include='*.xml' -f 'hide,! */' JSTOR/. JSTOR_XML

# GET ALL XML FILES OUT TO NEW DIRECTORY

find JSTOR_XML/ -maxdepth 10 -type f -exec mv -i '{}' JSTOR_XML/ ';'


# DELETE EMPTY FOLDERS
cd JSTOR_XML/
rm -R `ls -1 -d */`

# GET 1-GRAMS
rsync -avm --include='*-NGRAMS1.txt' -f 'hide,! */' JSTOR/. JSTOR_1GRAMS

# GET ALL 1grams FILES OUT TO NEW DIRECTORY

find JSTOR_1GRAMS/ -maxdepth 10 -type f -exec mv -i '{}' JSTOR_1GRAMS/ ';'

# DELETE EMPTY FOLDERS
cd JSTOR_1GRAMS/
rm -R `ls -1 -d */`
