#####################################################
# Functions of common use
print 'Defining usefull functions..'

import csv
import numpy

#####################################################
# Export a list into a csv

def export_csv(my_data, save_directory, element_type, header=None):
	# save_directory: should be a string with directory and file name
	# eg. '../Data/all_data.csv'
	# element_type: 'list' or 'dictionary'

	print 'Exporting data...'

	if element_type == 'nlist':
		with open(save_directory, 'w') as tosave:
			writer = csv.writer(tosave, dialect ='excel')
			if header != None:
				writer.writerow(header)
			writer.writerows(my_data)
		print 'List saved!'


	elif element_type == 'flist':
		with open(save_directory, 'wb') as tosave:
			writer = csv.writer(tosave)
			if header != None:
				writer.writerow(header)
			writer.writerow(my_data)
		print 'List saved!'


	elif element_type == 'fdictionary':
		with open(save_directory, 'wb') as csv_file:
			writer = csv.writer(csv_file)
			for key, value in my_data.iteritems():
				writer.writerow((key, value))
		print 'Dictionary saved!'
	

	elif element_type == 'fldictionary':
		with open(save_directory, 'wb') as csv_file:
			writer = csv.writer(csv_file)
			for key, value in my_data.iteritems():
				newelem= [key]
				for v in value:
					newelem.append(v)
				writer.writerow(newelem)
				
		print 'Dictionary saved!'


	elif element_type == 'ndictionary':
		with open(save_directory, 'wb') as csv_file:
			writer = csv.writer(csv_file)
			#journals = my_data.values()[0].keys()
			for key in my_data.keys():
				for key2 in my_data[key].keys():
					newelem = [key] + [key2] + my_data[key][key2] 
					writer.writerow(newelem)
		print 'Dictionary saved!'

	else:
		print 'Unrecognizable element type, please choose between: nlist, flist, fdictionary, ndictionary. \n Sorry nothing saved..'


#######################################################
# Import csv as list of tuples

def import_csv(my_file_path, element_type, element_info):
	# my_file_path: '../Data/all_data_top_300.csv'
	with open(my_file_path, 'r') as newcsv:
		if element_info == 'floats':
			reader = csv.reader(newcsv, quoting=csv.QUOTE_NONNUMERIC)
		elif element_info == 'strings':
			reader = csv.reader(newcsv)
		else:
			print 'Wrong element_info: please choose \'strings\' or \'floats\' '

		if element_type == 'list':
			imported_file = [tuple(line) for line in reader]
			print 'List imported!'

		elif element_type == 'dictionary':
			#aslist = [list(line) for line in reader]
			imported_file = dict((rows[0],rows[1:]) for rows in reader)

			print 'Dictionary imported!'
			
		else:
			print 'Unrecognizable element type, sorry nothing imported..'

	return imported_file


# Hellinger distances 

# _SQRT2 = np.sqrt(2)     # sqrt(2) with default precision np.float64

# def hellinger1(p, q):
#     return norm(np.sqrt(p) - np.sqrt(q)) / _SQRT2

# def hellinger2(p, q):
#     return euclidean(np.sqrt(p), np.sqrt(q)) / _SQRT2

# def hellinger3(p, q):
# 	return np.sqrt(np.sum((np.sqrt(p) - np.sqrt(q)) ** 2)) / _SQRT2

def hellinger(X):
#https://blakeboswell.github.io/2016/gensim-hellinger/
	from scipy.spatial.distance import pdist, squareform
	return squareform(pdist(numpy.sqrt(X)))/numpy.sqrt(2)
