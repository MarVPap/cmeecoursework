#!/bin/bash
#Author: Marina marina.papadopoulou16@imperial.ac.uk
#Script: 
#Date: Feb 2017

## install package for word counting

#sudo apt-get install untex

echo "Marina'a Mini-Project is about to start!"

## 1. Run python script to read through the data
python Get_Data.py

echo "Python Finished!"

## 2. Run R script
echo "R started to run!"
echo "Please wait, this may take some minutes..."
Rscript My_Analysis.R

echo "Your results are ready!"

## 3. Prepare report with Latex

echo 'Latex Report Preparation started' 

## count latex file
untex MiniProject.tex |wc -w >> MyWordCount.txt

## Compile pdf
pdflatex MiniProject.tex
bibtex MiniProject 
pdflatex MiniProject.tex
pdflatex MiniProject.tex
evince MiniProject.pdf & 

## Cleanup
rm *.aux
rm *.log
rm *.out
rm *.toc
rm *.bbl
rm *.blg
rm MyWordCount.txt
#rm *.snm
#rm *.nav
#rm *.dvi
#rm *~

# Done!
echo 'Mini Project finished!!! Enjoy!'

