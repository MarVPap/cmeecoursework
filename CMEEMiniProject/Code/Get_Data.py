#!/usr/bin/env python

"""get_data() : this function in full work takes a list of journal names as an input, downloads .xml files with summary 
of papers of these journals. Then using regular expressions to run through all these files and produces 4 .csv files  in 
the results directory. The first is the main dataset, where each row represents a papers informationin. The other 3 have 
the first and last names of all the authors of each paper, and also their gender. All 4 files have a common column with 
the scopus id of each paper. 
Note: to fully run this code the user should apply for a key from Scopus API system and replace the existing one in the script.
For illustration purposes, downloading functions will be hashed, the code is going to read a sample of our data, and the resultsing
.csv are not going to be the one used in further analysis, but a far smaller sample of it.
	"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.2'


###############################
## Imprort packages
###############################

import re # for use of regular expressions
# import numpy as np # for pyscopus package
# import pandas # for pyscopus package
# from bs4 import BeautifulSoup  # for pyscopus package
# import time # to time our function
import os 
# import shutil # for high-level operations
# import sqlite3
import csv # to import and export csvs
from lxml import etree # to deal with xml file format
import unicodedata # to deal with xml file format
# import xml.etree.ElementTree as ET # to deal with xml file format
import sys 
import urllib2
import gender_guesser.detector as gender # package that assignes gender based on first names
# from timeit import default_timer # to time our function
# from pyscopus.scopus import Scopus # package to use the Scopus API portal
 
g = gender.Detector() # Function used to find gender from a first name

# Assign our scopus API key which give us access to the Scopus system to download papers information
# MyAPIKey = '53a289e216978d0dcfd7f5386ea5b104'
# scopus = Scopus(MyAPIKey)

# Anounce start
print "Pyhton script started collecting data!"

#########################################################################
######### Import journals list ##########################################
#########################################################################

def download_my_data():
	
	## First we should import a csv file with the titles of the journals we want to
	## collect data for. 
	
	def open_csv_to_list(MyJournalsList):
		''' Opens a csv and returns a list with all its elements'''
		
		with open (MyJournalsList, 'r') as j:  # 'r' for reading
			r = csv.reader(j, delimiter=',') # for every object separated by "," creates a row
			journals = []
			for row in r:
				journals.append(row)  
			# print '1 ok ' # unhush to ckeck for every journal imported from the file
			
		# Keep flat list with paper names
		JournalList= [item for JournalList in journals for item in JournalList]
		return(JournalList) # Returns a list with the journals title
	
	
	MyJournalsListTop = '../Data/Journal_Lists/Top100List.csv' # The path to the file with the journals title
	GTop = open_csv_to_list(MyJournalsListTop) # Call the function on the path and assign the list to a variable GTop
	
	
	
	#########################################################################
	########### GET PAPERS LIST #############################################
	#########################################################################
	
	## Now we wish to get papers information for each one of the jounals we are interested in
	
	def get_papers_info(Journ_Search_Res):
		
		''' Uses a list with journals titles as input
		and uses the scopus package to download a specific
		amount of papers' ID codes, in a specific range of time.
		Specifically, it searches for 2000 papers between 2000 and
		2015, for every journal'''
		
		# start = default_timer()
		id_list = []
		y = 0 # to count number of papers added to the list
		e =0 # to count number of errors appeared
		
		for i in Journ_Search_Res: # for each Journal 
			
			try: # try function to skip any errors occured from the API system or the internet connection 
				journal_name = scopus.search_venue(i, count = 2000, year_range=(2000,2015), show = False) 
				# downloads a list of tuples with the IDs and the titles of a maximum of 2000 papers between
				# 2000 and 2015. For different number of papers or date, just change the number in the command.
				
				journ_test = [(ID , Title) for ID, Title in journal_name.items()] # fix the format of the
				# downloaded data by converting them to a list of tuples
				
				ids = [x[0] for x in journ_test]  # keep only the IDs in a list (withour the titles)
			
				for i in ids: # normalize the IDs string format and store to a new list
					id_list.append(unicodedata.normalize('NFKD', i).encode('ascii','ignore'))
					y = y+1 # count IDs added'
					
			except AttributeError: # If there is an Attribute error due to a 
				# a different format or information of a specific paper, skip it and continue the loop
				print 'An Attribute Error skipped'
				e = e+1 # Count errors encounted
				continue
			except urllib2.HTTPError: # Same for http error due to internet connection problems that
				# may be encountered during the download
				e = e+1 # Count errors encounted
				print 'An Unexpected error skipped:', sys.exc_info()[0] # print also the type of 
				# error to make sure is not a general/code problem
				
				continue # continue with the rest of the loop
				
			
		# dur = default_timer() - start # calculate the time needed 
		# print dur/3600 
		print 'There were',y,'papers IDs added to the list.'
		print 'There were',e,'API errors encountered during download.'
		return id_list # return the list of IDs
		
	
	# Call the function with the Journal list we created
	My_id_list_TOP = get_papers_info(GTop) 
	
	
	# Export the list with the papers IDs as a csv file 
	name_of_fileTOP = "ID_list_Top" # name to be saved
	output_path = "../Data/Extracted" # the directory to be saved
	full_nameTOP = os.path.join(output_path, name_of_fileTOP+".csv") # Join file directory-file-file extension (.csv)
	
	with open(full_nameTOP, "w") as output_file: # export file using the path to the file created
		for item in My_id_list_TOP: # for each ID in our list
			output_file.write("%s\n" % item)
	
	
	#########################################################################
	######### GET PAPERS INFORMATION ########################################
	#########################################################################
	
	## Now we should download the papers to extract the information we want.
	## Instead of downloading the hole pdf of each paper, we will ask for
	## .xml file that have only the information we need.
	
	
	## If we started a new session we first need to upload the ID list
	## that we extracted before. For this we are going to use the first
	## function we created to add the journals list.
	
	
	## The relative path to our list
	idstop_dir = '../Data/Extracted/ID_list_Top.csv'
	
	## Call the open_csv_to_list function on the ID list file and assign to a variable
	ids_top = open_csv_to_list(idstop_dir)
	
		
	## A function to download the .xml paper files:
	
	def get_xml(My_id_list,MyXMLFilesDir):
		''' A function that uses a list of papers IDs, and downloads
		the .xml file of each paper, and saves it in directory, also 
		given as input'''
		
		start = default_timer()
		x = 0 # count the number of xml that where asked from Scopus
		for i in My_id_list:
			pub_info = scopus.retrieve_abstract(i, show = False, save_xml= MyXMLFilesDir) # pyscopus function that saves the xml files
			x=x+1
		dur = default_timer() - start # calculate time needed
		
		print "Time needed:", dur
		print "Papers searched:", x
		return dur # Returns the amount of time needed in case we want to store it 
		
	
	## Due to the limitations for the amount of papers that Scopus permits
	## to download with a specific key, we separate our list into parts of
	## 20000. The total number of papers we could download with our key was
	## 100000. To better control our data and be able to stop and continue
	## our code when the limit is reached, or for any other reason, without wasting 
	## time or skip IDs we are using a smaller amount of papers for each part
	## and save each one to a different folder.
	
	partsTOP = [ids_top[x:x+20000] for x in xrange(0,len(ids_top), 20000)] # regular API keys allow 8000 papers per key
	
	# Call get_xml function in a loop for each part
	
	for i in range(0,len(partsTOP)):
		print "Part_%s started" %i
		MyXMLFilesDir = "../Data/Downloads/XML_Downloads_TOP/Part_%s" %i
		if not os.path.exists(MyXMLFilesDir): # Create a directory for each part to save the files
			os.makedirs(MyXMLFilesDir) # only if the directory does not already exist
		
		get_xml(partsTOP[i], MyXMLFilesDir) # download the xml files
		print "Part_%s finished" %i
	print 'Data downloaded'


# download_my_data()


#########################################################################
######### READ XML FILES ################################################
#########################################################################

# The next step is to read through our downloaded data
# and extract the information we need for our analysis.

def read_xmls(my_xml_directory): # '../Data/Downloads/'
	''' A function that uses a directory and goes through every
	xml file in this directory. It uses regular expressions to keep
	the following information: name and surname of first author, name and surname
	of all authors, papers ID, number of times the paper has been cited till
	the day of download, the date it was publiced, the first author's ID, and 
	the number of references. It is then uses the gender_guesser package
	to guess the gender of all authors of each paper. Finally it exports
	all these information in a list of list (each sublist contains every papers
	information).'''
	
	# create empty lists to store our data
	Fname = [] # list for first names
	MyCSV = [] # general list to store all the data
	
	x= 0 # to count the papers info added
	y= 0 # to count the errors occured
	
	for xmlFile in os.listdir(my_xml_directory): # for every xml name in the input directory
		xmlFile = my_xml_directory +'/'+ xmlFile # create path to the file
		f = open(xmlFile, 'rt') # open the file to read
		textFile = f.read()
		f.close()
		
		## Change files format to make it ready for regex	
		textFile = textFile.replace('\n', ' ').replace('\t', ' ') # replace all newlines and tabs with spaces
		textFile = textFile.decode('ascii', 'ignore') # change format
		textFile = str((textFile)) # convert everything to string
		textFile = re.sub(' +',' ', textFile) # replace all + with spaces
		
		#######################################################
		## Use pf regular expressions (specifically look
		## behind and look infront commands) to extract elements
		
		ScopusID = re.search(r'(?<=SCOPUS_ID:)\d+', textFile) # for the scopus ID
		CitedBy = re.search(r'(?<=<citedby-count> )\d+', textFile) # the number of times cited till download date
		Date = re.search(r'(?<=<prism:coverdate> )\d{4}-\d{2}-\d{2}', textFile) # the date published (format: year-month-day)
		AuthID = re.search(r'(?<= auid=")\d+', textFile) # first authors ID
		Authseq = re.search(r'(?<=seq=")\d+', textFile) # first authors position-should always be 1. Keep just for check 
		RefCount = re.search(r'(?<= refcount=")\d+(?=">)', textFile) # Number of references in paper
		#AuthCountry = re.search(r'(?<=<affiliation-country> ).*?(?=</affiliation-country> )', textFile) # unhush to 
		# keep affiliation country of first author. This information is not stated in all the journals xml, and so
		# excludes a lot of papers from our analysis
		Journal = re.search(r'(?<=<prism:publicationname> ).*?(?=</prism:publicationname> )', textFile) # Journal name
		
		# For first author's names:

		try: # We should account for errors cause some papers don't have the authors first name in their xml files
			
			AuthFName = re.search(r'(?<=<ce:given-name> ).*?(?=</ce:given-name> )', textFile) # find first name
			AuthFName = re.sub('\.',' ',AuthFName.group()) # Some names are stated with initials, so first we replace . with spaces
			spl = AuthFName.split() # If with initials it will be splitted in a list of strings eg. ['Sammraat', 'P']
			if len(spl) > 1: # if list contains more than 1 element
				for i in range(len(spl)):
					if len(spl[i]) == 1:
						spl[i] = 0 # replace the initial with a 0
				spl = [itm for itm in spl if not isinstance(itm,int)] # keep only non numerical elements (so exclude the former initials)
				if len(spl) == 0: # if there where only initials eg. ['S','P'], and list is empty
					spl = "I" # replace by I so its not empty
				AuthFName = ''.join(spl) # put results back together
			
		except AttributeError: # If there is no name in the file, it will give an attribute error when applying the .group function
			y=y+1 # count error 
			print "There was no name in this paper"
			print xmlFile # print xml name for further check
			continue # go to the next file

		# For first authors surnames:		
		AuthSName = re.search(r'(?<=<ce:surname> ).*?(?=</ce:surname> )', textFile)
		
		# For all authors first and last names:
		
		AllAuthF = re.findall(r'(?<=<ce:given-name> ).*?(?=</ce:given-name> )',textFile) # Find all first names
		# create a list to store all the authors names
		AllAuthFlist = [ScopusID.group()]
		AllAuthFnew = []
		for k in AllAuthF: # the same procedure that was used for the first authors name
			k = re.sub('\.',' ',k)
			spl = k.split()
			if len(spl) > 1:
				for i in range(len(spl)):
					if len(spl[i]) == 1:
						spl[i] = 0
				spl = [itm for itm in spl if not isinstance(itm,int)]
				j = ''.join(spl)
				j = re.sub(' ','',j)
				if len(j) == 0:
					j = "I"
				AllAuthFnew.append(j) # add name to the list
							#break				
			else : # if there is only the name (no initials)
				k = re.sub(' ','',k) # take spaces out of the string (because the gender_guesser function doesnt recognize spaces)
				AllAuthFnew.append(k) 
	
		# Find all surnames
		AllAuthS = re.findall(r'(?<=<ce:surname> ).*?(?=</ce:surname> )',textFile)
		
		# ADD ALL NAMES IN LISTS
		
		# First name of all authors
		AllAuthFlist = [ScopusID.group()] # add the papers ID as a first element to separate list from the main data
		for i in AllAuthFnew: # Keep unique names
			if i not in AllAuthFlist: # cause first authors info are stated more than 1 times in the file
				AllAuthFlist.append(i)
				
		AllAuthF = AllAuthFlist

		# Last name of all authors
		AllAuthSlist = [ScopusID.group()] # Same as with first names
		for i in AllAuthS:
			k = i.rstrip()
			if k not in AllAuthSlist:
				AllAuthSlist.append(k)
				
		AllAuthS = AllAuthSlist # in this list there are also the surnames of the references authors
		# In order to keep only authors information:
		number_of_authors = len(AllAuthF)
		AllAuthS = AllAuthS[0:number_of_authors] # Keep only the authors that have a first name in the first name list
		
		######################################
		# ESTIMATE THE GENDERS OF THE AUTHORS
		
		# First authors gender
		Agender = g.get_gender(re.sub(' ','', AuthFName)) # take all spaces out and feed it to the gender_guesser function
		Agender = str(Agender.decode('ascii','ignore')) # change format from unicode to string 
		
		# All authors gender
		AllAuthGen = [ScopusID.group()] # list to store genders
		for k in AllAuthF[1:len(AllAuthF)]: # same as for first authors but for every element of the list
			AlAgen = g.get_gender(k)
			AlAgen =  str((AlAgen.decode('ascii','ignore')))
			AllAuthGen.append(AlAgen)
			
			
		########################################
		# EXTRACT INFORMATION
		
		try:			
			## Combine everything found into a tuple:
			My_paper_list = (ScopusID.group(),Agender,AuthFName, AuthSName.group(), AuthID.group(), Authseq.group(), Journal.group(), Date.group(), CitedBy.group(), RefCount.group(),  AllAuthF, AllAuthS, AllAuthGen)

			MyCSV.append(My_paper_list) # Add each papers tuple with information to the general results list (of tuples)
			x = x+1 # count paper added
			
		except AttributeError: # In case one of the variables is missing in a specific xml file (e.g. some papers without references or number of them)
			y=y+1 # count error
			print "One values was missing- Attribute Error skipped for the file:"
			print xmlFile # name of the file for check 
			# print Agender, AuthFName # for further information
			continue
		
	print "One data list is done!"
	print "There were added" , x, "elements in my results list for this part."
	print "There were", y, "errors or files with missing information."
	return MyCSV

## Call the function for all the folders with our files (all parts)

All_Data = []
for i in range(0,5): # len(partsTOP)) for general apply
	print "Part_%s started" %i
	MyXMLFilesDir = "../Data/Downloads/Part_%s" %i
	part_csv = read_xmls(MyXMLFilesDir) # Take info from all xml files in each folder
	All_Data.extend(part_csv) # Adds all elements of my part list in my general list

	print "Part_%s finished" %i
print "Hoorey! Your Data are ready!"




#########################################################################	
######## EXPORT DATA ####################################################
#########################################################################
  
 ## Finally, we extract out data into .csv files.
 ## These will be used for data analysis and model fitting.  
 
 ## The lists with all authors' names and genders are of different
 ## length that the rest information for each paper. So we will
 ## save them to different csvs, and combine them together based on
 ## their scopus ID(first element in all lists) during data analysis.
	     
	     
## Split our results list of tuples based on the csvs we want to export:

First_Authors = [] # General info for each paper (single elements)
All_Authors_F = [] # List for first names of all authors of each paper
All_Authors_S = [] # List for last names of all authors of each paper
All_Authors_G = [] # List for gender of all authors of each paper
for i in range(len(All_Data)): # split all data to the right list
	First_Authors.append(All_Data[i][0:10])
	All_Authors_F.append(All_Data[i][10])
	All_Authors_S.append(All_Data[i][11])
	All_Authors_G.append(All_Data[i][12])


## Create header for the Main csv:
CSVHead = ( 'ScopusID', 'FAuthor_Gender', 'FAuthor_Name', 'FAuthor_SurnN', 'FAuthor_ID', 'Seq','Journal','Date', 'CitedBy', 'NumofRef')
First_Authors.insert(0,CSVHead) # Insert header as first element of main list

## Store separate lists in a list to export files through a loop:
Full_csv = [First_Authors, All_Authors_F, All_Authors_S , All_Authors_G]
      

## Export:

output_path = "../Data/" # Path to the directory to save the data

for i in range(0, len(Full_csv)):
	name_of_file = "Data_"+str(i) # Name each csv eg.'Data_0' for the main csv
	full_name= os.path.join(output_path,name_of_file+".csv") # Join results directory-file-file extension (.csv)
	with open(full_name, "w") as output_file: # Write files
		writer = csv.writer(output_file, dialect='excel')
		writer.writerows(Full_csv[i])
		


		
##########################################################
### Optional: Check Gender Counts from List ##############
##########################################################

## A section to check gender's counts from a list
## of first names. Check gender guesser  function.


def find_gender(first_name_list):
	m = 0 # male 
	f = 0 # female
	a = 0 # androgynous name 
	mm = 0 # mostly male
	mf = 0 # mostly female
	u = 0 # unknown
	
	for i in first_name_list:
		Agender = g.get_gender(i)
		if Agender == u'male':
			m = m+1
		elif Agender == u'female':
			f = f+1
		elif Agender == u'andy':
			a = a+1
		elif Agender == u'mostly_female':
			mf = mf +1
		elif Agender == u'mostly_male':
			mm = mm+1
		elif Agender == u'unknown':
			u = u +1

	print m, "male names found"
	print f, "female names found"
	print mm, "mostly male names found"
	print mf, "mostly female names found"
	print a, "androgynous names found"
	print u, "unknown names"
	

# find_gender(my_first_names_list) # optional to test gender guesser function


############################################################################
#####______________________ Version 2.0 ____________________________########
############################################################################
