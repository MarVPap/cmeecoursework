#!/bin/bash
#Author: Marina marina.papadopoulou16@imperial.ac.uk
#Script: csvtospace.sh
#Desc: substitutes the commas in the files with spaces and saves the output into a .txt file
#Arguments: 1-> comma delimited file
#Date: Oct 2016

 echo "Creating a space delimited version of $1 "
 
 cat $1 | tr -s "," " " >> $1.txt
 
 echo "Done. May the force be with you."
 
 exit 
