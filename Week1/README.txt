
Weclome to my 1st week as a CMEE master student. Hope you 'll find everything you need in here!

This Week we learned amazing stuff about Unix, Version Control and LaTeX. The directory is structured on 3 main subfolders: Code, Data and Sandbox, and their content is listed below.

----Sandbox: not-at-all-important files used to test the scripts saved in the Code folder.

----Data: 2 folders (fasta and Temperatures) with data copied from the silbiocompmasterepo repository. In the Temperatures folder, there is also the .txt form of the 4 .csv files (output of the csvtospace.sh shell script, see below).

----Code: 14 files

1. boilerplate.sh : our first shell script, just displays a line of text.

2. tabtocsv.sh : shell script that replaces the tabs in the files with commas and creates a new .csv file.

3. variables.sh : shell script for variables assignments (asks for a string and displays it in a text, then asks for 2 numbers and displays their sum).

4. MyExampleScript.sh : happy shell script that says hello to the user.

5. Countlines.sh: shell script that counts the number of lines in the input file.

6. ConcatenateTwoFiles.sh : shell script that merges two imput files into a third one.

7. csvtospace.sh : the shell script of the 2.7 Practical, that converts the input comma separated file into a space separated file and saves it as new .txt file

8. FirstExample.tex : the latex script used as an input to create a .pdf file

9. FirstBiblio.bib : the BibTeX format of the bibliography of the FirstExample.tex

10. FirstExample.pdf : the pdf file created from the previous FirstExample files.

11. CompiLaTeX.sh : the shell script that compiles latex with bibtex and removes other file types (input: a .tex file, output: a .pdf, a .blg and a .bbl files).

12. FirstExample.blg : the bibliographic reference that BibTeX and LaTeX use.

13. FirstExample.bbl: a file with the references formatted according to the bibliography style chosen in the .tex file.

14. UnixPrac1.txt : the file with the shell commands of the practical's 1.13 .


____________________________________________________________________
Created by Marina Papadopoulou marina.papadopoulou16@imperial.ac.uk


