
Welcome to the 2nd week of the CMEE master students! Have fun!

 ___________________________________________
   Basic Biological Computing in Python  
 ___________________________________________
                  

----Data: 5 files and 1 folder with 3 files

1. fata folder: copied from the silbiocompmasterepo repository. Used from the align_seqs_fasta.py code (see Code and Results folder below).
	
2-3. Two .csv files with oaks data (TestOaksData.csv, JustOaksData.csv): copied from the silbiocompmasterepo repository. Used and modified from the test_oaks.py (see Code below).

3. default_align.csv : a .csv file with two DNA sequences that is being used as a default input to the align_seqs.py function, separated by a single comma (see Code below).
	
4-5. Two .fasta files ("small_seq1.fasta" and "small_seq2.fasta") that are being used as a default input to the align_seqs_fasta.py function (see Code below) 



----Results: 5 files

1-3. Three .txt files with name starting with "f_align_", that contain the alignments output from the align_seqs_fasta.py function (includes the best align and the best score of each pairing).

4. aligned_default_align.csv.txt : output file from the default align_seqs.py function (contains the best align and the best score for the 2 sequences of the default_align.csv data file)

5. aligned_small_seq1.fassmall_seq2.fas.txt:  output file from the default align_seqs.fasta.py function. Contains the best align and the best score of the alignment of the 2 default input fasta files ("small_seq1.fasta" and "small_seq2.fasta").



----Code: 20 files

1. align_seqs.py : 

funtion that aligns two DNA sequences such that they are as similar as possible. It starts with the longest string and try to position the shorter string in all possible positions. Finally, it finds the "best align", the align with the highest score, and saves the score's number and this align to a new file. It takes one .csv file as an input with the two sequences to be aligned on top, separated by a single comma. (For the default files being used, please see the Data and Results folder above).

2. align_seqs_fasta.py : 

function that executes the same with align_seqs.py script, but uses any 2 fasta files as an input.(For the default files being used, please see the Data and Results folder above).

3. basic_csv.py :

script that uses the .csv package. It reads a file fron Sandbox folder (testcsv.csv), containing:"Species", "Infraorder","Family","Distriburion","Body mass male (Kg)" and prints a sentence with the names of each one of the species. Finally,it prints each row of the .csv file and creates a new file containing only the species names and the body masses, and saves it in the Sandbox folder (bodymass.csv)..

4. basic_io.py :

 script that tests input, output and storing objects, using test.txt file from Sandbox, and creates testout.txt and testp.p files in the same folder. It also prints the test.txt file (one time row by row as it is and one without the blank lines), along with a dictionary. 

5. boilerplate.py

 our first python function, just displays a line of text. It can be called both from the bash window and the ipython shell. 

6. cfexercises.py

module that first prints a bunch of hellos, based on different functions, 
and includes the fooXX ( foo1() - foo5() ) modules. These functions take arguments from the user and produce different outputs. By default, some evaluations for each one of the fooXX
functions will be printed when cfexercises.py is run, as an example.

7. control_flow.py :

functions exemplifying the use of control statements

8. debugme.py :

a function with a bug to test debugging.

9. dictionary.py:

script that uses a list of tuples with taxa and order names to create a dictionary
that maps order names to sets of taxa.

10. lc1.py: 

script that splits a nested tuple to (3) different lists, based on the 
position of each object in the tuple, with the use of both conventional 
loops and list comprehensions

11. lc2.py: 

script that uses rainfall/month data and creates a list of month,rainfall touples, 
when the rain is greater than 100mm, and a list of just month names when the amount
of rain was less than 50 mm (with the use of both conventional loops and list comprehensions).

12. loops.py : 

script with for and while loops. !!! Infinite loop included !!!

13. oaks.py : 

script that finds those taxa that are oak trees from a list of species. 
It uses both loops and list comprehensions, and finally prints the resulting sets.

14. regexs.py : 

script that uses regex functions (from the module re). They search strings to match to a given pattern and return a match object if a match is found and None if not.

15. scope.py : 

script to test global variables.

16. sysargv.py : 

script to understand sys.argv object.

17. test_control_flow.py : 

script to test unit testing with doctest.

18. test_oaks.py : 

function that finds the oak species. It takes a sting as an input, and if its a species name 
that starts with 'quercus' , returns True. As a default, it uses data from the Test0aksData.csv file (includes various species names), and creates a new 
JustOaksData.csv (see Data folder), with only the names of the species of the genus Quercus.

19. tuple.py : 

script that uses a tuple of tuples of length three, with latin name, common name and mass of birds, and prints these on a separate line for each species.

20. using_name.py : 

script to understand the use of " __name__ == “__main__”  " in the functions scripts.
It prints different outputs depending how it is called/imported by the user.



----Sandbox: not important files used to test the scripts saved in the Code directory.


____________________________________________________________________
Created by Marina Papadopoulou marina.papadopoulou16@imperial.ac.uk


