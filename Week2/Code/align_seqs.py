#!/usr/bin/env python

"""align_seqs('../Data/test.csv') -> sting/path to the file
	This funtion align two DNA sequences such that they are as similar as possible.
	It starts with the longest string and try to position the shorter string in all possible
	positions. Finally, it finds the "best align", the align with the highest score,
	and saves the score's number and this align to a new file.
	"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'

# Imports

import sys 
import csv	#To use csv file as an input
import os.path 	#To assign a path for exporting results

def align_seqs(input_file):   
	
	"""Input: a sting with the relative or absolute path of the file to be alligned 
	(e.g. '/home/Documents/CMEECourseWork/Week2/Data/test.csv'). 
	It should be a .csv file, with the 2 sequences given at the top of the script and separated by a comma.

	Output: creates a new .txt file	with the best alignment and its score,
	which is saved by default in the "../Results" directory (default name of the results is: "aligned_default_align.csv.txt".
	Both the directory and the file's name can be assigned by the user, by unhashing the commands in lines 131 and 134  . """


	#################################################################
	##
	## 1) Open the input files and assign variables to the sequences
	##
	
	with open (input_file, 'r') as f:  # 'r' for reading
		r = csv.reader(f, delimiter=',') #for every object separated by "," creates a row
		for row in r:
			if len(row) == 2: 
				x= row[0]  # assign sequences to variables
				y= row[1]
		print 'Input sequence 1: ' , x #print sequences as imported from the file
		print 'Input sequence 2: ' , y
			
		
	##########################################
	##
	## 2) Assign variables to the lengths of the two sequences
	## l1 is the length of the longest, l2 that of the shortest
	## 
	
	l1 = len(x) #assign variables to the lengths of the sequences
	l2 = len(y)
	print 'The lenght of sequence 1 is : ' , l1  #print lengths od the sequences
	print 'The lenght of sequence 2 is : ' , l2
	
	
	if l1 >= l2 :  
			s1 = x
			s2 = y
	else:
			s1 = y
			s2 = x
			l1, l2 = l2, l1 # Swap the two lengths
	
	print 'The lenght of the longest sequence is: ', l1 #print to test swaps and assignments
	print 'The lenght of the sortest sequence is: ', l2
	#print 'The longest sequence is: ', s1 
	#print 'The sortest sequence is: ', s2 
	print ''
		
	
	def calculate_score(s1, s2, l1, l2, startpoint):
		
		"""3) Function that computes a score by returning the number 
	of matches starting from arbitrary startpoint"""
		
		matched = ""  # Contains string for alignement
		score = 0
				
		for i in range(l2):
			if (i + startpoint) < l1:
					if s1[i + startpoint] == s2[i]: # if its matching the character
						matched = matched + "*"
						score = score + 1  # add a point to the score for every match
					else:
						matched = matched + "-"
		
		# Build some formatted output to test function
		#print "." * startpoint + matched           
		#print "." * startpoint + s2
		#print s1
		#print score 
		#print ""
		
		return score ## 
	
	#print 'Test function: '  #print an example to test function
	#print ''
	#print calculate_score(s1, s2, l1, l2, 0)
		
		        
	########################################################
	##	        
	## 4) Loop to find the best match (highest score)
	## 
	    
	my_best_align = None
	my_best_score = -1
	
	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i)
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z
			result = [my_best_align, my_best_score] # create a list with the results (best align and highest score)
			
	result_string = 'My best align is: ' + ', with score: '.join(str(x) for x in result) # transform list into a string, so it can be saved in a .txt file
	print result_string
	print ''
	
	
	###########################################################################################################
	##
	## 5) Export results as a new file in our preference directory. 
	## The path directory and outputs name can either be assigned by the user or keep the defaults: "../Results" as directory and 
	## "aligned_+input_file_name" as name. If you want to add these manually, please unhash
	## the commands in lines 131 and 134"""
	##
	
	output_path = "../Results"
	#output_path = raw_input("Where do you want to save your results? (please paste the output file's directory):") #Read a string from standard input, unhash if you want to change the outputs directory
	
	name_of_file = "aligned_" + input_file.strip("../Data/")  # assign the name of the new file: uses strip command to erase the directory's characters from the input
	#name_of_file = raw_input("Please give a name to your results file (no file extention needed): ") # unhash if you want to manually assign the outputs name
	
	full_name = os.path.join(output_path, name_of_file+".txt") # Join results directory-file-file extension (.txt)
	       
	output_file = open(full_name, "w") # open the new file to write ("w")
	output_file.write(result_string) # write varable "result_string", which includes a sting of the best align and score
	output_file.close() # closes the new file
	
	print ''
	print "File saved in your directory!"  # Print to test thats over
	
def main(argv):
	print align_seqs("../Data/default_align.csv")
	return 0
	
if (__name__ == "__main__"): 
	status = main(sys.argv)
	sys.exit (status)
	
