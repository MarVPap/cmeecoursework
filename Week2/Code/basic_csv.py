
""" basic_csv.py : script that uses the .csv package.
 It reads a file fron Sandbox folder (testcsv.csv), containing:
"Species","Infraorder","Family","Distriburion","Body mass male (Kg)" and 
prints a sentence with the names of each one of the species. 
Finally,it prints each row of the .csv file and creates a new file 
containing only the species names and the body masses, and saves it 
in the Sandbox folder (bodymass.csv)."""

import csv  #Imports the .csv package

# Read a test.csv file from Sandbox, containing:
#"Species", "Infraorder","Family","Distriburion","Body mass male (Kg)"

f=open('../Sandbox/testcsv.csv','rb') ## open the file to be read

csvread =csv.reader(f)
temp= [] 
for row in csvread: 
	temp.append(tuple(row))
	print row
	print "The species is", row[0]

f.close()

# write a file containing only species name and Body mass
f=open('../Sandbox/testcsv.csv','rb')  
g=open('../Sandbox/bodymass.csv','wb') ##open the new file that we want to write

csvread = csv.reader(f)
csvwrite=csv.writer(g)
for row in csvread:
	print row
	csvwrite.writerow([row[0],row[4]])
	
f.close()
g.close()

