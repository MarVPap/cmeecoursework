#!/usr/bin/bash

"""using_name.py : 
Script to understand the use of  __name__ == __main__ in the functions scripts.
It prints different outputs depending how it is called/imported by the user. 
"""

if __name__=='__main__':
	print 'This program is being run by itself'
else:
	print 'I am being imported form another module'

