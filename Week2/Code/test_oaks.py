#!/usr/bin/env python

""" is_an_oak(" ") --> this function finds the oak species. 
It takes a sting as an input, and if its a species name 
that starts with 'quercus' , returns True. 
As a default, it uses data from the Test0aksData.csv file
(includes various species names), and creates a new 
JustOaksData.csv, with only the names of the species of 
the genus Quercus"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


## Imports

import csv
import sys
import pdb
import doctest

## Is_an_oak function definition

def is_an_oak(name):
	
	""" Returns True if name is starts with 'quercus'
	>>> is_an_oak('quercus')
	True
	
	>>> is_an_oak('Quercuss')
	False
	
	>>> is_an_oak('Fagus sylvatica')
	False"""
	
	## If the input name starts with 'querqus', then the "split" function splits the input after
	## the sting 'quercus', and assigns the second part of the split (not the "quercus")in a "test" variable. 
	## If the test variable is an empty sting or followed by a " ", means that our input is an oak
	## (e.g. "quercus", "Quercus ilex"). If the test variable contains anything else, it means
	## that the input is not an oak ( e.g. "Quercusss"), so the loop gives FALSE.
	
	
	if name.lower().startswith('quercus'):
		test = name.lower().split("quercus",1) [1] 
		if test == "":
			return True
		else:
			if test.startswith(" "):
				return True
			else:
				return False
	else: 
		return False
		
			
print(is_an_oak.__doc__)

def main(argv): 
	f = open('../Data/TestOaksData.csv','rb')
	g = open('../Data/JustOaksData.csv','wb')
	taxa = csv.reader(f)
	csvwrite = csv.writer(g)
	oaks = set()
	for row in taxa:
		print row
		print "The genus is", row[0]
		if is_an_oak(row[0]):
			print (row[0])
			print "FOUND AN OAK!"
			print " "
			csvwrite.writerow([row[0], row[1]])    
    
	return 0
    

if (__name__ == "__main__"):
	status = main(sys.argv)

doctest.testmod()
