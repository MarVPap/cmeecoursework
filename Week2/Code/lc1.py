#!/usr/bin/env python

"""Latin names, common names and mean body masses for 5 bird species. 
 This script splits a  nested tuple to (3) different lists, based on the 
 position of each object in the tuple, with the use of both conventional 
 loops and list comprehensions)"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'

## Given nested tuple with the latin name, the common name and the mean body mass of 5 bird species

birds = ( ('Passerculus sandwichensis','Savannah sparrow',18.7),
          ('Delichon urbica','House martin',19),
          ('Junco phaeonotus','Yellow-eyed junco',19.5),
          ('Junco hyemalis','Dark-eyed junco',19.6),
          ('Tachycineata bicolor','Tree swallow',20.2),
         )
         

##(1) List comprehensions  

latin_name_lc = ([species[0] for species in birds if species[0]]) 
print latin_name_lc
	
	
common_name_lc = ([species[1] for species in birds if species[1]])
print common_name_lc
	
	
body_mass_lc = ([species[2] for species in birds if species[2]])
print body_mass_lc	

	
##(2) Conventional loops

latin_name_loop_list = []   ## Define the 3 empty lists for each caregory that we want to split our data 
common_name_loop_list = [] 
body_mass_loop_list = [] 

for species in birds:       ## Inplicit loop that adds (.append):
	latin_name_loop_list.append(species[0])  ## 1. objects from the first position of the tuples in latins name's loop
	common_name_loop_list.append(species[1]) ## 2. objects from the second position of the tuples in common name's loop
	body_mass_loop_list.append(species[2]) ## 3. objects from the first position of the tuples in mean body mass' loop
print latin_name_loop_list   ## Print new separated lists for latin names, common names, and body masses
print common_name_loop_list
print body_mass_loop_list




##########################################
#### TASK ################################
##########################################

# (1) Write three separate list comprehensions that create three different
# lists containing the latin names, common names and mean body masses for
# each species in birds, respectively. 

# (2) Now do the same using conventional loops (you can shoose to do this 
# before 1 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
