#!/usr/bin/env python

"""align_seqs_fasta(x,y) :
This funtion align two DNA sequences such that they are as similar as possible.
	It starts with the longest string and try to position the shorter string in all possible
	positions. Finally, it finds the "best align", the align with the highest score,
	and saves the score's number and this align to a new file."""


__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


## Imports

import sys 
import os.path 	#To assign a path for exporting results
	
def align_seqs_fasta(x,y): 
	
	"""	
	Input: two .fasta files (names and relative path, if needed). For default the function uses the
	small_seq1.fasta and small_seq2.fasta files from the Data folder, ("../Data/small_seq1.fasta", "..Data/small_seq2.fasta").
	You can manually import your data by unhashing the commands in lines 31 and 32
	
	Output: creates a new .txt file	with the best alignment and its score which is save by default in the "../Results" directory 
	(default name of the results is: ."aligned_small_seq1.fassmall_seq2.fas.txt".
	Both the directory and the file's name can be assigned by the user, by unhashing the commands in lines 155 and 158  """  
	
	# For manually enter the files to be alligned:
	# x = raw_input('Enter your 1st .fasta sequence. File name:')
	# y = raw_input('Enter your 2nd .fasta sequence. File name:')
	
	def keep_seq(fastaf):
		"""opens the input files and assigns only the sequences 
		(without the newlines and the label line) to the variables """
		
		f = open(fastaf,'r')
		line = f.readline()
		meta= '' # empty string to add the meta__ of each file 
		seque='' # empty string to add the sequences to be alligned
		while line:
			line = line.rstrip('\n') # strip newlines from each line
			if '>' in line: # if ">" symbol appears in line
				meta = line # store this line as meta (title__?? of fasta file)
			else:
				seque = seque + line # add each sequence line in the variable seque
				fastaf = seque  # assign the sequence to the input variable of the function
			line = f.readline()
		print ''
		print meta # print label line to test if the files have been imported correctly
		return fastaf # keep the sequence
	
	
	###########################################
	##
	## 1) Assign input sequences to variables
	##
	
	seq1 = keep_seq(x) # call the keep_seq function and assign the 1st input sequence in x
	seq2 = keep_seq(y) # call the keep_seq function and assign the 2nd input sequence in y
	
	##########################################
	##
	## 2) Assign variables to the lengths of the two sequences
	##
	
	l1 = len(seq1) 
	l2 = len(seq2)
	print ''
	print 'The lenght of sequence 1 is : ' , l1  #print lengths of the sequences to test
	print 'The lenght of sequence 2 is : ' , l2
	print ''
	
	
	##############################################################
	##
	## 3) Assign the longest sequence s1, and the shortest to s2.
	## l1 is the length of the longest, l2 that of the shortest
	##
	
	if l1 >= l2 :  
			s1 = seq1
			s2 = seq2
	else:
			s1 = seq2
			s2 = seq1
			l1, l2 = l2, l1 # Swap the two lengths
	
	print 'The lenght of the longest sequence is: ', l1 #print to test swaps and assignments
	print 'The lenght of the sortest sequence is: ', l2 #print to test swaps and assignments 
	#print 'The longest sequence is: ', s1  # use for small sequences to test assignments
	#print 'The sortest sequence is: ', s2 
	print ''
		
	
	def calculate_score(s1, s2, l1, l2, startpoint):
		
		"""Function that computes a score by returning the number 
	of matches starting from arbitrary startpoint"""
		
		matched = ""  # Contains string for alignement
		score = 0
				
		for i in range(l2):
			if (i + startpoint) < l1:
					if s1[i + startpoint] == s2[i]: # if its matching the character
						matched = matched + "*"
						score = score + 1  # add a point to the score for every match
					else:
						matched = matched + "-"
		
		# Build some formatted output to test function
		#print "." * startpoint + matched           
		#print "." * startpoint + s2
		#print s1
		#print score 
		#print ""
		
		return score ## 
	
	#print 'Test function: '  #print an example to test function
	#print ''
	#print calculate_score(s1, s2, l1, l2, 0)
		
		
	########################################################
	##	        
	## 4) Find the best match (highest score)
	##
	    
	my_best_align = None
	my_best_score = -1
	
	for i in range(l1):
		z = calculate_score(s1, s2, l1, l2, i) ## calls the calculate score function
		if z > my_best_score:
			my_best_align = "." * i + s2
			my_best_score = z
			result = [my_best_align, my_best_score] # create a list with the results (best align and highest score)
			
	result_string = 'My best align is: ' + ', with score: '.join(str(x) for x in result) # transform list into a string, so it can be saved in a .txt file
	#print result_string  # use to print result if the allignment is not too long
	print 'Allignment done!'
	
	
	########################################################################################################################################
	##
	## 5) Export results as a new file in our preference directory. 
	## The path directory and outputs name can either be assigned by the user or keep the defaults: "../Results" as directory and 
	##"aligned_+input_file1_name_+input_file2_name" as name. To assign manually please unhash the raw input commands in lines 155 and 158"""
	##
	
	output_path = "../Results"
	#output_path = raw_input("Where do you want to save your results? (please paste the  directory of the file, e.g. ../Results ): ") # asks for a directory
	
	name_of_file = "aligned_" + x.strip("../Data/") + y.strip("../Data/") # assign the name of the new file: uses strip command to erase the directory's characters from the input
	#name_of_file = raw_input("Please give a name to your results file (no file extention needed): ") # assign a name for the new file
	
	full_name = os.path.join(output_path, name_of_file+".txt") #Join results directory-file-file extension (.txt)
	       
	output_file = open(full_name, "w") #open the new file to write ("w")
	output_file.write(result_string) #write varable "result_string", which includes a sting of the best align and score
	output_file.close() #closes the new file
	
	print ''
	print "File saved in your directory!"  #Print to test thats over
	
 
def main(argv):
	print align_seqs_fasta("../Data/small_seq1.fasta", "../Data/small_seq2.fasta")
	return 0
	
if (__name__ == "__main__"): 
	status = main(sys.argv)
	sys.exit (status)
