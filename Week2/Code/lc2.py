#!/usr/bin/env python

"""Average UK Rainfall (mm) for 1910 by month ( by http://www.metoffice.gov.uk/climate/uk/datasets ). 
This script uses these data and creates a list of touples (month,rainfall), 
where the rain is greater than 100mm, and a list of just month names where 
the amount of rain was  less than 50 mm (with the use 
of both conventional loops and list comprehensions)"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


rainfall = (('JAN',111.4),
            ('FEB',126.1),
            ('MAR', 49.9),
            ('APR', 95.3),
            ('MAY', 71.8),
            ('JUN', 70.2),
            ('JUL', 97.1),
            ('AUG',140.2),
            ('SEP', 27.0),
            ('OCT', 89.4),
            ('NOV',128.4),
            ('DEC',142.2),
           )


## (1) List comprehension to create a list of tuples (month,rainfall), where the rain is greater than 100mm
rainy_months = ([rain for rain in rainfall if rain[1] > 100]) 
print rainy_months


## (2) List comprehension to create a list of the months, where the rain is less than 50mm
not_rainy_months = ([rain[0] for rain in rainfall if rain[1] < 50]) 
print not_rainy_months
	
## (3) Conventional loop 

rain_loop = []  #creating empty lists for months with rainfall greater then 100mm
no_rain_loop = []   #creating empty lists for months with rainfall less then 50mm
for rain in rainfall:
	if rain[1] > 100:    
		rain_loop.append(rain) 
	elif rain[1] < 50:
		no_rain_loop.append(rain[0])
print rain_loop
print no_rain_loop
		




######################################
########## TASK ######################
######################################


# (1) Use a list comprehension to create a list of month,rainfall tuples where
# the amount of rain was greater than 100 mm.
 
# (2) Use a list comprehension to create a list of just month names where the
# amount of rain was less than 50 mm. 

# (3) Now do (1) and (2) using conventional loops (you can choose to do 
# this before 1 and 2 !). 

# ANNOTATE WHAT EVERY BLOCK OR IF NECESSARY, LINE IS DOING! 

# ALSO, PLEASE INCLUDE A DOCSTRING AT THE BEGINNING OF THIS FILE THAT 
# SAYS WHAT THE SCRIPT DOES AND WHO THE AUTHOR IS
