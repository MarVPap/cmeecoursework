#!/usr/bin/env python

""" cfexercises.py 
module that first prints a bunch of hellos, based on different functions, 
and includes the fooXX ( foo1() - foo5() ). These functions take arguments from the user
and produce different outputs. By default, some evaluations for each one of the fooXX
functions will be printed when cfexercises.py is run, as an example. """


__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'
		

# imports
import sys #module to interface our program with the operating system


### Hello ####
# How many times will "hello" be printed?

# 1)
for i in range(3, 17):
	print 'hello'
	
# 2)
for j in range(12):
	if j % 3  ==0:
		print 'hello'
		
# 3)
for j in range(15):
	if j % 5 == 3:
		print 'hello'
	elif j % 4 ==3:
		print 'hello'
	
# 4)
z = 0
while z !=15:
	print 'hello'
	z = z + 3
	
# 5)
z=12
while z<100:
	if z == 31:
		for k in range(7) :
			print 'hello'
	elif z == 18:
			print 'hello'
	z=z+1


######################
## fooXX functions ###

def foo1(x):   
	""" foo1    """          
	return x ** 0.5  ##like defining a function, try example foo1(5)=..
	
def foo2(x, y):
	if x>y:
		return x
	return y

def foo3(x, y, z):
	if x>y:
		tmp = y
		y=x
		x=tmp
	if y>z :
		tmp = z
		z = y
		y = tmp
	return [x, y, z]
	
def foo4(x):
	result = 1
	for i in range(1, x+1):
		result = result * i
	return result


def foo5(x):
	if x == 1:
		return 1
	return x * foo5(x-1)


def main(argv):
	# sys.exit("don't want to do this right now!")
	print foo1(4)
	print foo2(3,5)
	print foo2(5,3)
	print foo3(1,2,3)
	print foo3(1,3,2)
	print foo3(3,2,1)
	print foo3(3,1,2)
	print foo4(4)
	print foo4(5)
	print foo5(5)
	return 0
	
if (__name__ == "__main__"): 
	status = main(sys.argv)
	sys.exit (status)
	
	
