
Welcome to the 5th week of the CMEE master students! 

 ____________________________________________________________
   Spatial Analyses & Geographic Information Systems (GIS)  
 ____________________________________________________________
                  

---- Code: 3 files

1. clifford.test.R :
a function imported in the spatial_mod.R script to execute 
the clifford test.

2. GIS_prac.py : 
A python script that executes the GIS Practical 1.
It converts bioclims temperature and rainfall data and CORINE
landcover files into a shared projection and resolution (BNG 2km grid).
Finally it extracts mean climatic values within land cover classes
and creates a .csv file with these information.

3. spatial_mod.R : 
An R script for spatial modelling and especially 
testing and accounting for spatial autocorrelation.



----Results: 1 file and 1 folder

1.zonalstats.csv:
output file of the GIS_prac.py with the mean climatic values for
the bio1 and bio12 climatic variables within each land cover class.

2. A zip folder with GIS data, containing the 2 raster output files of
the GIS_prac (bio1c_UK_BNG and bio12c_UK_BNG) and  3 reprojected files 
from the GIS practical 1 (MODIS_red_reflectance_UTM50N.tif, 
MODIS_NIR_reflectance_UTM50N.tif, MODIS_blue_reflectance_UTM50N.tif).


----Data: 2 folders of GIS data

1. EU: contains 6 .tif files used from the GIS_prac.py script:
bio1c_15.tif, bio1c_16.tif, bio12c_15.tif, bio12c_16.tif, g250_06.tif, G250_06_UK_BNG.tif .
(The bio1c and bio12c raster files have been clipped from the original bio1 and 
bio12 files to the area needed, in order to reduce the file's sizes).

2. SpatMod: contains 4 .tif files used from the spatial_mod.R script.
(avian_richness.tif, elev.tif, mean_aet.tif, mean_temp.tif)


----Sandbox: 1 file, an R script for a graphical user interface for
Stochastic Modelling of Land Cover Change, not compatible with unix os.



____________________________________________________________________
Created by Marina Papadopoulou marina.papadopoulou16@imperial.ac.uk




