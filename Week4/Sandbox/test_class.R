install.packages("lme4")
library(lme4)
require(lme4)

setwd("/home/marina/CMEECourseWork/Week4/Code")

##Clear our environment
rm(list=ls())

## Import data

d<-read.table("../Data/SparrowSize.txt", header = TRUE)
str(d)

names(d)
head(d)

length(d$Tarsus)

#Creat histogram
hist(d$Tarsus)

#Centrality,mean,median and mode in normally distributed data

mean(d$Tarsus) ## NAs values so cannot calculate mean

mean(d$Tarsus, na.rm = TRUE)
median(d$Tarsus, na.rm = TRUE)
mode(d$Tarsus)

par(mfrow = c(2,2))
hist(d$Tarsus, breaks = 3, col = "grey")
hist(d$Tarsus, breaks = 10, col = "grey")
hist(d$Tarsus, breaks = 100, col = "grey")

install.packages("modeest")

require(modeest)

d2<- subset(d, d$Tarsus!="NA") ##take out NAs
length(d$Tarsus)
length(d2$Tarsus)

mlv(d2$Tarsus)

##Range, variance and std

range(d$Tarsus, na.rm = TRUE)
range(d2$Tarsus, na.rm = TRUE)

var(d$Tarsus, na.rm = TRUE)

sum((d2$Tarsus -  mean(d2$Tarsus))^2)/(length(d2$Tarsus) - 1) 	
sqrt(var(d2$Tarsus)) 	

zTarsus  <- (d2$Tarsus - mean(d2$Tarsus))/sd(d2$Tarsus) 	
var(zTarsus)

hist(zTarsus) 	


### HO4
d1<- subset(d,  d$Tarsus!="NA") 	
seTarsus<- sqrt(var(d1$Tarsus)/length(d1$Tarsus)) 	
seTarsus 	

d12001<- subset(d1,  d1$Year==2001) 	

seTarsus2001<- sqrt(var(d12001$Tarsus)/length(d12001$Tarsus)) 	

seTarsus2001

