
Welcome to the 3rd week of the CMEE master students! 

 ___________________________________________
   Basic and Advanced Computing in R  
 ___________________________________________
                  

---- Code: 23 files 

1) apply1.R :
A simple R script to illustrate the *apply family of functions.

2) apply2.R :
A simple R script to illustrate the use of the *apply family of functions in functions.

3) basic_io.R :
A simple R script to illustrate R input-output

4) boilerplate.R:
A simple R script to illustrate R functions.

5) break.R :
A simple R script to illustrate a control flow tool, to break out of a loop.

6) browse.R :
An R script to illustrate the usage of browser() in R for debugging.

7) control.R :
An R script to illustrate control statements.

8) DataWrang.R:
An R script that is wrangling the Pound Hill Dataset (see Data Directory).

9) get_TreeHeight.R: 
A generalization of the TreeHeight.R script.
It takes a csv file name from the command line and outputs the result to a file that includes the input file name in the output file name as InputFileName_treeheights.csv .

10)Latex_Results.tex :
A Latex script with the source code for the pdf document (See Results directory) that interprets the results from the correlation analysis by the TAutoCorr.R script

11) Mapping_extra.R :
A simple R script to illustrate mapping in R.
Creates a world map with all the locations from which the data in the GPDD
dataframe (see Data directory) where collected.

12) next.R :
A simple R script to illustrate a control flow tool, to skip to next iteration of a loop.

13) PP_Lattice.R: 
An R script that draws and saves three lattice graphs of the EcolArchives-E089-51-D1.csv data 
by feeding interaaction type. It also calculates the mean and median log predator mass, prey mass, and predator-prey size ratio, by feeding type, and saves it as a single csv output table called PP_Results.csv (see Results directory)

14) PP_Regress.R: 
An R-script that draws and saves a pdf file of a figure, and writes the
accompanying regression results to a formatted table in csv.

15) PP_Regress_loc.R :
An R script, that operates the same as the PP_Regress.R, but the analysis this time is separated by the dataset’s Location field.

16) run_get_TreeHeight.sh :
A Unix shell script that tests get_TreeHeight.R, and uses the trees.csv file as an example.

17) sample.R :
An R script to illustrate the sampling functions in R.

18) stochrickvect.R :
A modification of the Vectorize2.R (see below) with vectorization.

19) TAutoCorr.R :
An R script to examine if the temperature of one year is significantly correlated with the next years, using data from Key West in Florinda for the 20th century (see Data directory).

20) TreeHeight.R :
This function calculates heights of trees from the angle of elevation and the distance
from the base using the trigonometric formula: height = distance * tan(radians).
It finally calculates the height for all the trees of the trees.csv file (see Data folder), and
creates a .csv file with the results, named TreeHts.csv (see Results folder).

21) try.R :
An R script to illustrate the way to catch an error in R, but instead of 
stopping all the running code, keep going.

22) Vectorize1.R :
An R script to show how the vectorization proccess makes execution time smaller.

23) Vectorize2.R :
An R script that runs the stochastic (with gaussian fluctuations) Ricker Eqn. (from the bitbucket repository).


----Results: 16 files

__ 9 .pdf files with Graphs:
1) Fig_88.pdf
2) Girko.pdf
3) MyBars.pdf
4) Pred_Lattice.pdf
5) Pred_Prey_Overlay.pdf
6) Prey_Lattice.pdf
7) SizeRatio_Lattice.pdf
8) MyFirst-ggplot2-Figure.pdf
9) MyLinReg.pdf

__ 6 .csv results files:
10) MyData.csv
11) PP_Regress_loc_Results.csv
12) PP_Regress_Results.csv
13) PP_Results.csv
14) TreeHts.csv
15) trees_treeheights.csv

__ 1 .pdf Document file:
16)Latex_Results.pdf


----Data: 7 files 

1) EcolArchives-E089-51-D1.csv :
A dataset on Consumer-Resource (e.g., Predator-Prey) body mass ratios taken from the Ecological Archives of the ESA (Barnes et al. 2008, Ecology 89:881).

2-3) Two .RData (GPDDFiltered.RData , KeyWestAnnualMeanTemperature.RData)

4-5) Two .csv files with Pound Hill Data (PoundHillData.csv, PoundHillMetaData.csv)

6-7) Two .csv files used from the scripts of the Code directory



----Sandbox: testing scripts produced during the lectures-practicals


____________________________________________________________________
Created by Marina Papadopoulou marina.papadopoulou16@imperial.ac.uk



