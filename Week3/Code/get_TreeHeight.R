#!/usr/bin/Rscript

##################################################################################
############ PRACTICAL 7.14 #######################################################
##################################################################################

# This function calculates heights of trees
# from the angle of elevation and the distance
# from the base using the trigonometric formula
# height = distance * tan(radians)
#
# Arguments:
# degrees     The angle of elevation
# distance    The distance from base
#
# Output:
# The height of the tree, same units as "distance"

library(tools) 


args <- commandArgs(trailingOnly = TRUE) ## the trailingOnly makes the command to return any arguments

##Function

get_TreeHeight <- function(x)
{
  MyTrees <- read.csv(x, header =TRUE)  #import .csv file with headers
  
  radians <- MyTrees[,3] * pi / 180
  height <- MyTrees[,2] * tan(radians)
  print(paste("Tree height is:", height))
  
  MyTrees[,"Tree.Height.m"] <- height

  output_name = paste("../Results/", basename(file_path_sans_ext(x)),"_treeheights.csv", sep = "")
  
  write.csv(MyTrees, file= output_name)

  return (height)
}









