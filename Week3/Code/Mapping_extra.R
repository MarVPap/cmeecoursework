#!/usr/bin/Rscript

######################################################################
############ PRACTICAL 9.9 ###########################################
###### Extra: Mapping ################################################
######################################################################

##Load Data

load("../Data/GPDDFiltered.RData")

#See the data
head(gpdd) # see only first 50 rows

## Install the packages needed

install.packages("maps")
library(maps)

## Create a map of the world, with grey color background
map("world", bg = "grey")

## Add the points of the locations for the GPDD dataframe, in red
points(gpdd$long, gpdd$lat, col = "red", cex = .6)