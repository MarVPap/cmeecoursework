#!/bin/bash
#Author: Marina marina.papadopoulou16@imperial.ac.uk
#Script: calls the get_TreeHeight.R script and uses as default example the trees.csv file
#Arguments: none
#Date: Nov 2016


script="../Data/trees.csv"

Rscript get_TreeHeight.R $script
