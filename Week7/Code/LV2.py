#!/usr/bin/env python

""" The Lotka-Volterra Model with prey density dependence, simulated using scipy.
	- Input: the function takes arguments for the five LV model parameters
	(r=resource growth rate, a=consumer search rate, z=consumer mortality rate,
	e= consumer production efficency, K=Carrying capasity of resource popultation)
	from the commandline in the exact format: r a z e K
	- Output: a .pdf with the consumer-resource population dynamics plot 
	(named prey_and_predators_2.pdf) from the model	with prey density dependence.
	The chosen parameter values are also displayed	on the plot.
	Finally, the scritp prints the final population values (when 
	with the appropriate prameters both prey and predator persist.)"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'

import sys
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting

#import matplotlip.pyplot as p #Some people might need to do this


def LotVolt(pops, t=0): # t=0: the default value - without it will not going to work
    """ Returns the growth rate of predator and prey populations at any 
    given time step, with prey density dependence model"""
    
    R = pops[0]
    C = pops[1]
    dRdt = r*R*(1 - R/K) - a*R*C 
    dCdt = -z*C + e*a*R*C
    
    return sc.array([dRdt, dCdt])

# Define parameters:
r = float(sys.argv[1]) #1. # Resource growth rate
a = float(sys.argv[2]) #0.1 # Consumer search rate (determines consumption rate) 
z = float(sys.argv[3]) #1.5 # Consumer mortality rate
e = float(sys.argv[4])#0.75 # Consumer production efficiency
K = float(sys.argv[5]) #30 #Carrying capasity of the resource population

# Now define time -- integrate from 0 to 25, using 1000 points:
t = sc.linspace(0, 25,  1000)

x0 = 10
y0 = 5 
z0 = sc.array([x0, y0]) # initial conditions: 10 prey and 5 predators per unit area

#the output that scipy gives, integrate an odes
pops, infodict = integrate.odeint(LotVolt, z0, t, full_output=True)

#by default outputs a dictionary, this tells u if everything run correctly
infodict['message']     # >>> 'Integration successful.'

prey, predators = pops.T # Transpose to vertical 
#Creating a output plots file
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics with prey density dependence \n r=%s, a=%s, z=%s, e=%s, K=%s'%(r, a, z, e, K))
#p.show()
f1.savefig('../Results/prey_and_predators_2.pdf') #Save figure

print 'The final prey size is:', round(prey[-1],2)
print 'The final predator size is:', round(predators[-1],2)

	
def main(argv):
	print 'LV2 done!'
	return 0
	
if (__name__ == "__main__"): 
	status = main(sys.argv)
	sys.exit (status)
	
	
