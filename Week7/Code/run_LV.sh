#!/bin/bash
#Author: Marina marina.papadopoulou16@imperial.ac.uk
#Script: calls the run_LV_R.R script and then profiling the
# 4 LV.py files with some default values in the ipython platform
#Date: Nov 2016

## Run R script
Rscript run_LV_R.R

# Profiling in ipython
echo "%run -p LV1.py" | ipython
echo "%run -p LV2.py 1 0.1 1.5 0.75 35" | ipython
echo "%run -p LV3.py 1 0.1 1.5 0.75 60" | ipython
echo "%run -p LV4.py 1 0.1 1.5 0.75 60" | ipython
