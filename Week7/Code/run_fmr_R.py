#!/usr/bin/env python

""" Runs the fmr.R file using the subrocess module and prints on screen
	the R output and notifies that run was succesful """

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'

import subprocess

subprocess.Popen("/usr/lib/R/bin/Rscript --verbose fmr.R", shell=True).wait()

print " "
print "Finished in Python!"
