#!/usr/bin/env python

""" A discrete Lotka-Volterra Model with prey density dependence, 
	and random fluctuations drawn from a gaussian distribution for both
	prey and predators population, simulated using scipy.
	The time step is assigned to be 1 time unit.
	- Input: the function takes arguments for the five LV model parameters
	(r=resource growth rate, a=consumer search rate, z=consumer mortality rate,
	e= consumer production efficency, K=Carrying capasity of resource popultation)
	from the commandline in the exact format: r a z e K
	- Output: a .pdf with the consumer-resource population dynamics plot 
	(named prey_and_predators_4.pdf)from the model with prey density dependence. 
	The chosen parameter values are also displayed on the plot.
"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'

import sys
import scipy as sc 
import scipy.integrate as integrate
import pylab as p #Contains matplotlib for plotting
from scipy.stats import norm
import matplotlib.pyplot as p 


def LotVoltD4(pops, t=0): # t=0: the default value - without it will not going to work
	""" Returns the growth rate of predator and prey populations at any 
	given time step, with prey density dependence model"""
	R = pops[0]
	C = pops[1]
	R2 = R*(1 + (r+epsilon)*(1 - R/K) - a*C)
	C2 = C*(1 - z +epsilon2 + e*a*R)
		    
	return sc.array([R2 - R, C2 - C])

# Define parameters:
r = float(sys.argv[1]) #1. # Resource growth rate
a = float(sys.argv[2]) #0.1 # Consumer search rate (determines consumption rate) 
z = float(sys.argv[3]) #1.5 # Consumer mortality rate
e = float(sys.argv[4])#0.75 # Consumer production efficiency
K = float(sys.argv[5]) #30 #Carrying capasity

#Add random fluctuation drawn from a gaussian distribution
# 0 and 1 stand for extracting values from a normal distribution
# with mean 0 and stdev of 1, to control the effect of the
# fluctations to our population sizes
epsilon = float(norm.rvs(0, 1, size=1)) 
epsilon2 = float(norm.rvs(0, 1, size=1))

# Now define time -- integrate from 0 to 25, using 1000 points:
t = sc.linspace(0, 25, 25)

x0 = 10
y0 = 5
z0 = sc.array([x0, y0]) # initial conditions: 10 prey and 5 predators per unit area

#the output that scipy gives, integrate an odes
pops, infodict = integrate.odeint(LotVoltD4, z0, t, full_output=True)

#by default outputs a dictionary, this tells u if everything run correctly
infodict['message']     # >>> 'Integration successful.'

prey, predators = pops.T # Transpose to vertical 
#Creating a output plots file
f1 = p.figure() #Open empty figure object
p.plot(t, prey, 'g-', label='Resource density') # Plot
p.plot(t, predators  , 'b-', label='Consumer density')
p.grid()
p.legend(loc='best')
p.xlabel('Time')
p.ylabel('Population')
p.title('Consumer-Resource population dynamics with prey density dependence \n r=%s, a=%s, z=%s, e=%s, K=%s'%(r, a, z, e, K))
#p.show() #unhush to display graph
f1.savefig('../Results/prey_and_predators_4.pdf') #Save figure


def main(argv):
	print 'LV4 done!'
	return 0
	
if (__name__ == "__main__"): 
	status = main(sys.argv)
	sys.exit (status)
	
	
