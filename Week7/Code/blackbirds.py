#!/usr/bin/env python

""" This script opens the blackbirds.txt file from the Data directory 
abd uses a regular expression that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly"""

__author__ = 'Marina Papadopoulou (marina.papadopoulou16@imperial.ac.uk)'
__version__ = '0.0.1'


import re

# Read the file
f = open('../Data/blackbirds.txt', 'r')
text = f.read()
f.close()

# remove \t\n and put a space in:
text = text.replace('\t',' ')
text = text.replace('\n',' ')

# note that there are "strange characters" (these are accents and
# non-ascii symbols) because we don't care for them, first transform
# to ASCII:
text = text.decode('ascii', 'ignore')

# Now write a regular expression my_reg that captures # the Kingdom, 
# Phylum and Species name for each species and prints it out neatly:

text = str((text)) #transform file to script


# the regular expression that uses a look behind expression, matches the words
# after the matches "Kingdom", "Phylum" and "Species" (two words separated by a comma 
# for the species names), and saves them in the my_reg variable.

my_reg = re.findall(r'(?<=Kingdom )\w+|(?<=Phylum )\w+|(?<=Species )\w+\s\w+', text)

# To print the results nicely, for every set of kingdom-phylum-species, prins
# a separate sentence with the results.
for i in (0,3,6,9):
	print "The species",my_reg[i+2],"is in the", my_reg[i+1] ,"Phylum of the Kingdom", my_reg[i],"."
	
	


