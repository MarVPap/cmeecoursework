#!/usr/bin/env python

""" Use the subrocess module to open the TestR.R 
 script and save the output and errors in 2 new 
 results files.  """
 
import subprocess
subprocess.Popen("/usr/lib/R/bin/Rscript --verbose TestR.R > \
../Results/TestR.Rout 2> ../Results/TestR_errorFile.Rout", \
shell=True).wait()
