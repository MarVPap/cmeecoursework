#!/usr/bin/env python

""" Uses the subprocess.os module to extract files and 
	directories information from the users home directory.
	Specifically, it creates 3 lists with matches 
	files/directories starting with C, C or c and only
	directories starting with C or c"""

# Use the subprocess.os module to get a list of files and  
# Hint: look in subprocess.os and/or subprocess.os.path and/or 
# subprocess.os.walk for helpful functions

import subprocess

#################################
#~Get a list of files and 
#~directories in your home/ that start with an uppercase 'C'

# Type your code here:

# Get the user's home directory.
home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithC = []
num_list = 0

# Use a for loop to walk through the home directory.
for dire, subdir, files in subprocess.os.walk(home):
	for n in subdir:
		if n.startswith("C"):
			FilesDirsStartingWithC.append(n)
			num_list = num_list + 1
	for f in files:
		if f.startswith("C"):
			FilesDirsStartingWithC.append(f)
			num_list = num_list + 1
print num_list, "matched files and directories added to the FilesDirsStartingWithC list"

# print FilesDirsStartingWithC
	
	
#################################
# Get files and directories in your home/ that start with either an 
# upper or lower case 'C'

# Type your code here:


# keep user's home directory as home from above
# home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
FilesDirsStartingWithCorc = []
num_list_2 = 0

# Use a for loop to walk through the home directory.
for dire, subdir, files in subprocess.os.walk(home):
	for n in subdir:
		if n.lower().startswith("c"):
			FilesDirsStartingWithCorc.append(n)
			num_list_2 = num_list_2 + 1
	for f in files:
		if f.lower().startswith("c"):
			FilesDirsStartingWithCorc.append(f)
			num_list_2 = num_list_2 + 1
print num_list_2, "matched files and directories added to the FilesDirsStartingWithCorc list"

# print FilesDirsStartingWithCorc
	

#################################
# Get only directories in your home/ that start with either an upper or 
#~lower case 'C' 

# Type your code here:

# keep user's home directory as home from above
# home = subprocess.os.path.expanduser("~")

# Create a list to store the results.
DirsStartingWithCorc = []
num_list_3 = 0

# Use a for loop to walk through the home directory.
for dire, subdir, files in subprocess.os.walk(home):
	for n in subdir:
		if n.lower().startswith("c"):
			DirsStartingWithCorc.append(n)
			num_list_3 = num_list_3 + 1
			
print num_list_3, "matched directories added to the DirsStartingWithCorc list"

