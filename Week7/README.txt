
Welcome to the 7th week of the CMEE master students!

 _____________________________________________
   Advanced Biological Computing in Python   
 _____________________________________________
                  

----Data: 11 files

1. Biotraits.db: a database file used for the SQLite excercise 

2. blackbirds.txt : a text file with the full taxonomy of blackbirds speceis,
used from the blackbirds.py code file.

3-6. 4 csv files (Consumer.csv, TCP.csv, TraitInfo.csv, Resource.csv) that are splited tables from Biotraits dataset.

7. NagyEtAl1999.csv: the input data of the fmr.R script with metabolic rate and
body mass information

8-9. 2 .csv files (QMEE_Net_Mat_edges.csv and QMEE_Net_Mat_nodes.csv) with
the nodes and the links of the network exercises

10.test.db: a database file for the SQLite ecercise in python, created
from the SQLite.py script.

11. Test.sqlite: a database file for the SQLite ecercise in R, created
from the RSQLite.R script.




----Results: 11 files

1-2. errorFile.Rout and outputFile.Rout: subprocess output files when we run
subprocess.Popen in python for a non existing file.

3.fmr_plot.pdf : a plot with the log(field metabolic rate) against log(body mass) for the Nagy et al 1999 dataset, output of the fmr.R script.

4-7. prey_and_predators.pdf (1-4): the plots of the LV1-4.py scripts with 
the consumer-resource population dynamics of Lotka-Voltera models.

8-9. QMEENet.svg and QMEENetPython.svg: the network plots of the Nets.R and Nets.py 
respectively.

10-11. TestR.Rout and TestR_errorFile.Rout: results of the subprocess module from 
the TestR.py script.



----Code: 13 files

1. blackbirds.py:

A python script that opens the blackbirds.txt file from the Data directory 
and uses a regular expression that captures # the Kingdom, # Phylum 
and Species name for each species. 
- Output: it prints the results in a sentence for each species.


2. LV1.py:

A python script with the typical Lotka-Volterra Model simulated using scipy.
- Output: a .pdf with the consumer-resource population dynamics plot from the model
(named prey_and_predators_1.pdf). 


3. LV2.py:

A python script with the Lotka-Volterra Model with prey density dependence, simulated using scipy.
- Input: the function takes arguments for the five LV model parameters
(r=resource growth rate, a=consumer search rate, z=consumer mortality rate,
e= consumer production efficency, K=Carrying capasity of resource popultation)
from thecommandline in the exact format: r a z e K
- Output: a .pdf with the consumer-resource population dynamics plot 
(named prey_and_predators_2.pdf) from the model with prey density dependence. 
The chosen parameter values are also displayed on the plot.
Finally, the scritp prints the final population values (when 
with the appropriate prameters both prey and predator persist).


4. LV3.py:

A python script with a discrete Lotka-Volterra Model with prey density dependence, simulated using scipy. The time step is assigned to be 1 time unit.
- Input: the function takes arguments for the five LV model parameters
(r=resource growth rate, a=consumer search rate, z=consumer mortality rate,
e= consumer production efficency, K=Carrying capasity of resource popultation)
from thecommandline in the exact format: r a z e K
- Output: a .pdf with the consumer-resource population dynamics plot (named
prey_and_predators_3.pdf) from the model with prey density dependence. The chosen parameter values are also displayed on the plot."""


5. LV4.py: 

A python script with a discrete Lotka-Volterra Model with prey density dependence, 
and random fluctuations drawn from a gaussian distribution for both
prey and predators population, simulated using scipy.
The time step is assigned to be 1 time unit.
- Input: the function takes arguments for the five LV model parameters
(r=resource growth rate, a=consumer search rate, z=consumer mortality rate,
e= consumer production efficency, K=Carrying capasity of resource popultation)
from the commandline in the exact format: r a z e K
- Output: a .pdf with the consumer-resource population dynamics plot 
(named prey_and_predators_4.pdf)from the model with prey density dependence. 
The chosen parameter values are also displayed on the plot.


6. profileme.py:

A python script used to test the %run -p command for profiling.


7. RSQLite.R:

An R script that access, update and manage SQLite databases.
Creates a test database in the data directory.


8. run_LV.sh:

A bash scritp that calls the run_LV_R.R script and then profiling the
4 LV.py files with some default values in the ipython platform. 


9. run_LV_R.R:

An R script that runs the LV python scripts, using default variables.


10. SQLite.py:

A python script that access, update and manage SQLite databases.
Creates a test database in the data directory.


11. TestR.py: 

A python script that uses the subrocess module to open the TestR.R 
script (see below) and save the output.
Output: 2 files with the R output and the procedure/errors during running.


12. TestR.R:

An R script used as an example file to test running R with the
subprocess module.
Output: prints a greeting


13. timeitme.py:

A python script for quick profiling in python with the time and timeit modules. 


14. DrawFW.Py: 

A python script that plots a snapshot of a food web graph/network.
Input: Adjacency list of who eats whom (consumer name/id in 1st 
column, resource name/id in 2nd column), and list of species
names/ids and properties such as biomass (node abundance), or average
body mass.


15. fmr.R:

An R script that plots log(field metabolic rate) against log(body mass) for the Nagy et al 1999 dataset to a .pdf file (named fmr.pdf).


16. run_fmr_R.py:

A python script that runs the fmr.R file using the subrocess module and 
prints on screen the R outputs and notifies that the python run was succesful.


17. using_os.py:

A python script that uses the subprocess.os module to extract files and 
directories information from the users home directory. Specifically, it
creates 3 lists with matches files/directories starting with C, C or c and only
directories starting with C or c.


18. Nets.R : 

An R script that constructs and plots a snapshot of a collaboration network 
between Universities and partnerns.
- Input: the two QMEE datafiles with the edges and the nodes of our network.
- Output: save the plot as a .svg file.

19. Nets.py:

A python script that executes the same as the Nets.R script above.



----Sandbox: empty folder


____________________________________________________________________
Created by Marina Papadopoulou marina.papadopoulou16@imperial.ac.uk


