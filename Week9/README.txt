 ___________________________________________
   Week 9: HPC and Fractals  
 ___________________________________________
                  

----Data: an HPC_Results folder with the 100 .rda files from our cluster run

----Results: 

1. The HPC zip files with all our cluster results: the .rda files, the error and output files, and the 2 code files we used

2-19. Plots and a csv abundance octaves file from our R code.

----Code: 2 files

1. mp1916.R : our R code, with all the aswers to the HPC exercises

2. run.sh : the shell script used to run our code to the HPC cluser

----Sandbox: empty folder


____________________________________________________________________
Created by Marina Papadopoulou marina.papadopoulou16@imperial.ac.uk


